package com.gitlab.rurouniwallace.notes.test.models;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A notes app user
 */
public class User {

	/**
	 * The user's unique identifier
	 */
	@JsonProperty
	private UUID uuid;

	/**
	 * The user's email address
	 */
	@JsonProperty
	private String email;

	/**
	 * The user's password
	 */
	private String password;

	/**
	 * The user's phone number
	 */
	@JsonProperty
	private String phone;

	/**
	 * The user's given name
	 */
	@JsonProperty
	private String givenName;

	/**
	 * The user's surname
	 */
	@JsonProperty
	private String surname;

	/**
	 * Is the user's email address verified?
	 */
	private Boolean emailVerified;

	/**
	 * User's two factor auth preference. Default is zero, which means 2FA is turned
	 * off
	 */
	private Integer twoFactorAuthPreference = 0;

	/**
	 * User's OTP secret
	 */
	private String otpSecret;

	/**
	 * User's OTP algorithm
	 */
	private String otpAlgorithm;

	/**
	 * User's OTP step-up duration
	 */
	private Integer otpStep;

	/**
	 * Number of digits in OTP code
	 */
	private Integer otpDigits;

	/**
	 * Construct a new instance
	 */
	public User() {
		// empty constructor to be used by Jackson serializer
	}

	public User(final UUID uuid, final int twoFactorAuthPreference) {
		this.uuid = uuid;
		this.twoFactorAuthPreference = twoFactorAuthPreference;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param email user email address
	 * @param password user password
	 * @param phone user phone number
	 * @param givenName user's given name
	 * @param surname user's surname
	 */
	public User(final String email, final String password, final String phone, final String givenName,
			final String surname) {
		this.email = email;
		this.password = password;
		this.phone = phone;
		this.givenName = givenName;
		this.surname = surname;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param uuid the user's unique identifier
	 * @param email user email address
	 * @param password user password
	 * @param phone user phone number
	 * @param givenName user's given name
	 * @param surname user's surname
	 */
	public User(final UUID uuid, final String email, final String password, final String phone, final String givenName,
			final String surname) {
		this.uuid = uuid;
		this.email = email;
		this.password = password;
		this.phone = phone;
		this.givenName = givenName;
		this.surname = surname;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param uuid the user's unique identifier
	 * @param email user email address
	 * @param password user password
	 * @param phone user phone number
	 * @param givenName user's given name
	 * @param surname user's surname
	 * @param emailVerified is user email address verified?
	 */
	public User(final UUID uuid, final String email, final String password, final String phone, final String givenName,
			final String surname, final boolean emailVerified) {
		this.uuid = uuid;
		this.email = email;
		this.password = password;
		this.phone = phone;
		this.givenName = givenName;
		this.surname = surname;
		this.emailVerified = emailVerified;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param uuid the user's unique identifier
	 * @param email user email address
	 * @param password user password
	 * @param phone user phone number
	 * @param givenName user's given name
	 * @param surname user's surname
	 * @param emailVerified is user email address verified?
	 */
	public User(final String email, final String password, final String phone, final String givenName,
			final String surname, final boolean emailVerified, final Integer twoFactorAuthPreference,
			final String otpSecret, final String otpAlgorithm, final Integer otpStep, final Integer otpDigits) {
		this.email = email;
		this.password = password;
		this.phone = phone;
		this.givenName = givenName;
		this.surname = surname;
		this.emailVerified = emailVerified;
		this.twoFactorAuthPreference = twoFactorAuthPreference;
		this.otpSecret = otpSecret;
		this.otpAlgorithm = otpAlgorithm;
		this.otpStep = otpStep;
		this.otpDigits = otpDigits;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param uuid the user's unique identifier
	 * @param email user email address
	 */
	public User(final UUID uuid, final String email) {
		this(uuid, email, null, null, null, null);
	}

	/**
	 * Clone from another instance
	 * 
	 * @param other the other instance
	 */
	public User(final User other) {
		this.uuid = other.uuid;
		this.email = other.email;
		this.password = other.password;
		this.phone = other.phone;
		this.givenName = other.givenName;
		this.surname = other.surname;
		this.emailVerified = other.emailVerified;
		this.twoFactorAuthPreference = other.twoFactorAuthPreference;
		this.otpSecret = other.otpSecret;
		this.otpAlgorithm = other.otpAlgorithm;
		this.otpStep = other.otpStep;
		this.otpDigits = other.otpDigits;
	}

	/**
	 * @return the uuid
	 */
	public UUID getUuid() {
		return uuid;
	}

	/**
	 * @param uuid the uuid to set
	 */
	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the givenName
	 */
	public String getGivenName() {
		return givenName;
	}

	/**
	 * @param givenName the givenName to set
	 */
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * @return the emailVerified
	 */
	public Boolean isEmailVerified() {
		return emailVerified;
	}

	/**
	 * @param emailVerified the emailVerified to set
	 */
	public void setEmailVerified(Boolean emailVerified) {
		this.emailVerified = emailVerified;
	}

	/**
	 * @return the otpSecret
	 */
	public String getOtpSecret() {
		return otpSecret;
	}

	/**
	 * @param otpSecret the otpSecret to set
	 */
	public void setOtpSecret(String otpSecret) {
		this.otpSecret = otpSecret;
	}

	/**
	 * @return the twoFactorAuthPreference
	 */
	public Integer getTwoFactorAuthPreference() {
		return twoFactorAuthPreference;
	}

	/**
	 * @param twoFactorAuthPreference the twoFactorAuthPreference to set
	 */
	public void setTwoFactorAuthPreference(Integer twoFactorAuthPreference) {
		this.twoFactorAuthPreference = twoFactorAuthPreference;
	}

	/**
	 * @return the otpAlgorithm
	 */
	public String getOtpAlgorithm() {
		return otpAlgorithm;
	}

	/**
	 * @param otpAlgorithm the otpAlgorithm to set
	 */
	public void setOtpAlgorithm(String otpAlgorithm) {
		this.otpAlgorithm = otpAlgorithm;
	}

	/**
	 * @return the otpStep
	 */
	public Integer getOtpStep() {
		return otpStep;
	}

	/**
	 * @param otpStep the otpStep to set
	 */
	public void setOtpStep(Integer otpStep) {
		this.otpStep = otpStep;
	}

	/**
	 * @return the otpDigits
	 */
	public Integer getOtpDigits() {
		return otpDigits;
	}

	/**
	 * @param otpDigits the otpDigits to set
	 */
	public void setOtpDigits(Integer otpDigits) {
		this.otpDigits = otpDigits;
	}
}
