package com.gitlab.rurouniwallace.notes.test;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.rurouniwallace.notes.test.models.User;

import dev.samstevens.totp.secret.DefaultSecretGenerator;
import dev.samstevens.totp.secret.SecretGenerator;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class IntegrationTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(IntegrationTest.class);

	private static User user;

	private static ObjectMapper jsonMapper;

	private static String userId;

	private static List<String> folderIds;

	private static String generateRandomEmail() {
		return RandomStringUtils.randomAlphabetic(16).toLowerCase() + "@example.com";
	}

	private static String generateRandomPassword() {
		return RandomStringUtils.randomAlphabetic(16);
	}

	@BeforeAll
	public static void setUpBeforeClass() {

		jsonMapper = new ObjectMapper();

		final SecretGenerator otpSecretGenerator = new DefaultSecretGenerator(64);
		final String otpSecret = otpSecretGenerator.generate();

		user = new User();

		user.setEmail(generateRandomEmail());
		user.setPassword(generateRandomPassword());
		user.setPhone("(716)534-9698");
		user.setGivenName("Glenn");
		user.setSurname("Wallace");
		user.setTwoFactorAuthPreference(0);

		folderIds = new ArrayList<>();

		RestAssured.baseURI = "http://localhost:8080";
	}

	@Test
	@Order(1)
	public void createUser() throws JsonProcessingException {
		// Specify the base URL to the RESTful web service

		final RequestSpecification httpRequest = RestAssured.given();

		final String requestBody = jsonMapper.writeValueAsString(user);

		httpRequest.body(requestBody);
		httpRequest.contentType(ContentType.JSON);

		final String path = "/users";

		LOGGER.info("Submitting POST " + path + " with request body " + requestBody);

		final Response response = httpRequest.request(Method.POST, path);

		// Now let us print the body of the message to see what response
		// we have recieved from the server
		final String responseBody = response.getBody().asString();
		LOGGER.info("Response Body is =>  " + responseBody);

		response.then().statusCode(HttpStatus.SC_CREATED);

		final JsonPath jsonPath = response.jsonPath();
		assertEquals("SUCCESS", jsonPath.getString("status"));
		assertNotNull(jsonPath.getString("user.uuid"));

		userId = jsonPath.getString("user.uuid");
	}

	@Test
	@Order(2)
	public void lookupUser() throws JsonProcessingException {
		final String uriPath = "/users/" + userId;

		final User expectedUser = new User();

		given().get("/users/" + userId).then().body(containsString("test"));
	}

	@Test
	@Order(3)
	public void authWithWrongPasswordAfterCreatingUser() throws JsonProcessingException {
		LOGGER.info("User ID " + userId);

		assumeTrue(userId != null);

		final RequestSpecification httpRequest = RestAssured.given();

		final Map<String, String> authenticationRequest = new HashMap<>();
		authenticationRequest.put("email", user.getEmail());
		authenticationRequest.put("password", "invalidpassword");

		final String requestBody = jsonMapper.writeValueAsString(authenticationRequest);

		httpRequest.body(requestBody);
		httpRequest.contentType(ContentType.JSON);

		final String path = "/auth";

		LOGGER.info("Submitting POST " + path + " with request body " + requestBody);

		final Response response = httpRequest.request(Method.POST, path);

		response.then().statusCode(HttpStatus.SC_FORBIDDEN);

		// Now let us print the body of the message to see what response
		// we have received from the server
		final String responseBody = response.getBody().asString();
		LOGGER.info("Response Body is =>  " + responseBody);

		final JsonPath jsonPath = response.jsonPath();
		assertEquals("DENY", jsonPath.getString("status"));
	}

	@Test
	@Order(4)
	public void authWithCorrectPassword() throws JsonProcessingException {
		LOGGER.info("User ID " + userId);

		assumeTrue(userId != null);

		final RequestSpecification httpRequest = RestAssured.given();

		final Map<String, String> authenticationRequest = new HashMap<>();
		authenticationRequest.put("email", user.getEmail());
		authenticationRequest.put("password", user.getPassword());

		final String requestBody = jsonMapper.writeValueAsString(authenticationRequest);

		httpRequest.body(requestBody);
		httpRequest.contentType(ContentType.JSON);

		final String path = "/auth";

		LOGGER.info("Submitting POST " + path + " with request body " + requestBody);

		final Response response = httpRequest.request(Method.POST, path);

		response.then().statusCode(HttpStatus.SC_OK);

		// Now let us print the body of the message to see what response
		// we have received from the server
		final String responseBody = response.getBody().asString();
		LOGGER.info("Response Body is =>   " + responseBody);

		final JsonPath jsonPath = response.jsonPath();
		assertEquals("ALLOW", jsonPath.getString("status"));

		// all user details must be returned
		assertEquals(user.getEmail(), jsonPath.getString("user.email"));
		assertEquals(user.getGivenName(), jsonPath.getString("user.givenName"));
		assertEquals(user.getSurname(), jsonPath.getString("user.surname"));
		assertEquals(user.getPhone(), jsonPath.getString("user.phone"));
		assertEquals(user.getTwoFactorAuthPreference(), jsonPath.getInt("user.twoFactorAuthPreference"));
	}

	@Test
	@Order(5)
	public void createFolder() {

	}

}
