package com.gitlab.rurouniwallace.notes.models;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A user's note
 */
public class Note {

	/**
	 * Unique identifier
	 */
	@JsonProperty
	private UUID uuid;

	/**
	 * Folder this note is part of, if any
	 */
	@JsonProperty
	private UUID folder;

	/**
	 * User that owns this note
	 */
	private UUID user;

	/**
	 * Note title
	 */
	@JsonProperty
	private String title;

	/**
	 * Note body
	 */
	@JsonProperty
	private String body;

	/**
	 * Date/time the note was created
	 */
	@JsonProperty
	private Date created;

	/**
	 * Date/time the note was last updated
	 */
	@JsonProperty
	private Date lastUpdated;

	/**
	 * Construct a new instance
	 */
	public Note() {
		// empty constructor
	}

	/**
	 * Construct a new instance
	 * 
	 * @param uuid the unique identifier
	 * @param folder folder that this note is in
	 * @param user user that owns this note
	 * @param title note title
	 * @param body note body
	 */
	public Note(final UUID uuid, final UUID folder, final UUID user, final String title, final String body) {
		this.uuid = uuid;
		this.folder = folder;
		this.user = user;
		this.title = title;
		this.body = body;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param uuid the unique identifier
	 * @param folder folder that this note is in
	 * @param user user that owns this note
	 * @param title note title
	 * @param body note body
	 * @param created create date
	 * @param lastUpdated date last updated
	 */
	public Note(final UUID uuid, final UUID folder, final UUID user, final String title, final String body,
			final Date created, final Date lastUpdated) {
		this.uuid = uuid;
		this.folder = folder;
		this.user = user;
		this.title = title;
		this.body = body;
		this.created = created;
		this.lastUpdated = lastUpdated;
	}

	/**
	 * Clone from another instance
	 * 
	 * @param other instance to clone from
	 */
	public Note(final Note other) {
		this.uuid = other.uuid;
		this.folder = other.folder;
		this.user = other.user;
		this.title = other.title;
		this.body = other.body;
		this.created = other.created;
		this.lastUpdated = other.lastUpdated;
	}

	/**
	 * @return the id
	 */
	public UUID getUuid() {
		return uuid;
	}

	/**
	 * @param id the id to set
	 */
	public void setUuid(UUID id) {
		this.uuid = id;
	}

	/**
	 * @return the folder
	 */
	public UUID getFolder() {
		return folder;
	}

	/**
	 * @param folder the folder to set
	 */
	public void setFolder(UUID folder) {
		this.folder = folder;
	}

	/**
	 * @return the user
	 */
	public UUID getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(UUID user) {
		this.user = user;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}

	/**
	 * @param body the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

	/**
	 * @return the lastUpdated
	 */
	public Date getLastUpdated() {
		return lastUpdated;
	}

	/**
	 * @param lastUpdated the lastUpdated to set
	 */
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public void update(final Note updatePayload) {
		if (updatePayload.uuid != null) {
			this.uuid = updatePayload.uuid;
		}

		if (updatePayload.body != null) {
			this.body = updatePayload.body;
		}

		if (updatePayload.folder != null) {
			this.folder = updatePayload.folder;
		}

		if (updatePayload.user != null) {
			this.user = updatePayload.user;
		}

		if (updatePayload.title != null) {
			this.title = updatePayload.title;
		}

		if (updatePayload.body != null) {
			this.body = updatePayload.body;
		}

		if (updatePayload.created != null) {
			this.created = updatePayload.created;
		}

		if (updatePayload.lastUpdated != null) {
			this.lastUpdated = updatePayload.lastUpdated;
		}
	}

	/**
	 * Check against another instance for equality
	 * 
	 * @return true if equal, false otherwise
	 */
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Note)) {
			return false;
		}

		return Objects.deepEquals(((Note) other).uuid, this.uuid)
				&& Objects.deepEquals(((Note) other).folder, this.folder)
				&& Objects.deepEquals(((Note) other).user, this.user)
				&& Objects.deepEquals(((Note) other).title, this.title)
				&& Objects.deepEquals(((Note) other).body, this.body)
				&& Objects.deepEquals(((Note) other).created, this.created)
				&& Objects.deepEquals(((Note) other).lastUpdated, this.lastUpdated);
	}

	/**
	 * Write as a string
	 * 
	 * @return instance as a string
	 */
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		final String newLine = System.getProperty("line.separator");

		result.append("Notes {" + newLine);
		result.append("\tuuid: " + uuid + newLine);
		result.append("\tfolder: " + folder + newLine);
		result.append("\tuser: " + user + newLine);
		result.append("\ttitle: " + title + newLine);
		result.append("\tbody: " + body + newLine);
		result.append("\tcreated: " + created + newLine);
		result.append("\tlastUpdated: " + lastUpdated + newLine);
		result.append("}");

		return result.toString();
	}

	/**
	 * Returns a hash code of this instance
	 * 
	 * @return hash code of this instance
	 */
	@Override
	public int hashCode() {
		return uuid.hashCode();
	}

}
