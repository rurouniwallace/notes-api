package com.gitlab.rurouniwallace.notes.responses;

import java.util.List;

import com.gitlab.rurouniwallace.notes.models.Folder;

/**
 * Response to an API operation that contains aa list of folders
 */
public class FoldersListResponse extends StandardResponse {

	/**
	 * List of folders from the API operation
	 */
	private List<Folder> folders;

	/**
	 * Construct a new instance
	 */
	public FoldersListResponse() {
		// empty constructor
	}

	/**
	 * Construct a new instance
	 * 
	 * @param message the response message
	 * @param status the response status
	 */
	public FoldersListResponse(final String message, final StatusCode status) {
		this.message = message;
		this.status = status;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param status the response status
	 */
	public FoldersListResponse(final StatusCode status) {
		this.status = status;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param status the response status
	 * @param httpStatus HTTP status
	 */
	public FoldersListResponse(final StatusCode status, final int httpStatus) {
		this.status = status;
		this.httpStatus = httpStatus;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param httpStatus HTTP status
	 * @param message the response message
	 * @param status the response status
	 */
	public FoldersListResponse(final String message, final StatusCode status, final int httpStatus) {
		this.httpStatus = httpStatus;
		this.message = message;
		this.status = status;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param note the note for the response
	 * @param status the response status
	 */
	public FoldersListResponse(final List<Folder> folders, final StatusCode status) {
		this.folders = folders;
		this.status = status;
	}

	/**
	 * @return the folders
	 */
	public List<Folder> getFolders() {
		return folders;
	}

	/**
	 * @param folders the folders to set
	 */
	public void setFolders(List<Folder> folders) {
		this.folders = folders;
	}
}
