package com.gitlab.rurouniwallace.notes.config;

/**
 * Thread pool bulkhead configurations
 */
public class ThreadPoolBulkheadConfiguration {

	/**
	 * max thread pool size
	 */
	private Integer maxThreadPoolSize;

	/**
	 * core thread pool size
	 */
	private Integer coreThreadPoolSize;

	/**
	 * capacity of the queue
	 */
	private Integer queueCapacity;

	/**
	 * When the number of threads is greater than the core, this is the maximum time
	 * that excess idle threads will wait for new tasks before terminating
	 */
	private Integer keepAliveDuration;

	/**
	 * @return the maxThreadPoolSize
	 */
	public Integer getMaxThreadPoolSize() {
		return maxThreadPoolSize;
	}

	/**
	 * @param maxThreadPoolSize the maxThreadPoolSize to set
	 */
	public void setMaxThreadPoolSize(Integer maxThreadPoolSize) {
		this.maxThreadPoolSize = maxThreadPoolSize;
	}

	/**
	 * @return the coreThreadPoolSize
	 */
	public Integer getCoreThreadPoolSize() {
		return coreThreadPoolSize;
	}

	/**
	 * @param coreThreadPoolSize the coreThreadPoolSize to set
	 */
	public void setCoreThreadPoolSize(Integer coreThreadPoolSize) {
		this.coreThreadPoolSize = coreThreadPoolSize;
	}

	/**
	 * @return the queueCapacity
	 */
	public Integer getQueueCapacity() {
		return queueCapacity;
	}

	/**
	 * @param queueCapacity the queueCapacity to set
	 */
	public void setQueueCapacity(Integer queueCapacity) {
		this.queueCapacity = queueCapacity;
	}

	/**
	 * @return the keepAliveDuration
	 */
	public Integer getKeepAliveDuration() {
		return keepAliveDuration;
	}

	/**
	 * @param keepAliveDuration the keepAliveDuration to set
	 */
	public void setKeepAliveDuration(Integer keepAliveDuration) {
		this.keepAliveDuration = keepAliveDuration;
	}
}
