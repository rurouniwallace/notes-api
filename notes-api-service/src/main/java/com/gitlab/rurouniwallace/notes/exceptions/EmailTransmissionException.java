package com.gitlab.rurouniwallace.notes.exceptions;

/**
 * Error sending email
 */
@SuppressWarnings("serial")
public class EmailTransmissionException extends Exception {

	/**
	 * Construct a new instance
	 */
	public EmailTransmissionException() {
		super();
	}

	/**
	 * Construct a new instance
	 */
	public EmailTransmissionException(final String message) {
		super(message);
	}

	/**
	 * Construct a new instance
	 */
	public EmailTransmissionException(final String message, final Exception cause) {
		super(message, cause);
	}
}
