package com.gitlab.rurouniwallace.notes.exceptions;

/**
 * Attempted to create an entity that already exists
 */
@SuppressWarnings("serial")
public class EntityAlreadyExistsException extends DataAccessException {

	/**
	 * Construct a new instance
	 */
	public EntityAlreadyExistsException() {
		super();
	}

	/**
	 * Construct a new instance
	 */
	public EntityAlreadyExistsException(final String message) {
		super(message);
	}

	/**
	 * Construct a new instance
	 */
	public EntityAlreadyExistsException(final String message, final Exception cause) {
		super(message, cause);
	}
}
