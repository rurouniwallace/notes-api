package com.gitlab.rurouniwallace.notes.config;

import java.util.List;

public class CircuitBreakerConfiguration {

	/**
	 * Number of failures before the circuit is opened
	 */
	private Integer failureRateThreshold;

	/**
	 * Number of slow calls before the circuit is opened
	 */
	private Integer slowCallRateThreshold;

	/**
	 * Duration threshold after which calls are considered "slow" for the purpose of
	 * circuit breaking
	 */
	private Integer slowCallDurationThreshold;

	/**
	 * Permitted number of calls in half-open state
	 */
	private Integer permittedNumberOfCallsInHalfOpenState;

	/**
	 * maximum wait duration which controls the longest amount of time a
	 * CircuitBreaker could stay in Half Open state, before it switches to open
	 */
	private Integer maxWaitDurationInHalfOpenState;

	/**
	 * type of the sliding window which is used to record the outcome of calls when
	 * the CircuitBreaker is closed
	 */
	private String slidingWindowType;

	/**
	 * Sliding window size
	 */
	private Integer slidingWindowSize;

	/**
	 * Number of calls before calculating error rate and slow calls rate
	 */
	private Integer minimumNumberOfCalls;

	/**
	 * The time that the CircuitBreaker should wait before transitioning from open
	 * to half-open
	 */
	private Integer waitDurationInOpenState;

	/**
	 * If true, circuit breaker transition directly from open to half open
	 * automatically
	 */
	private Boolean automaticTransitionFromOpenToHalfOpenEnabled;

	/**
	 * List of exceptions to record as errors
	 */
	private List<String> recordExceptions;

	/**
	 * List of exceptions to ignore as errors
	 */
	private List<String> ignoreExceptions;

	/**
	 * @return the failureRateThreshold
	 */
	public Integer getFailureRateThreshold() {
		return failureRateThreshold;
	}

	/**
	 * @param failureRateThreshold the failureRateThreshold to set
	 */
	public void setFailureRateThreshold(Integer failureRateThreshold) {
		this.failureRateThreshold = failureRateThreshold;
	}

	/**
	 * @return the slowCallRateThreshold
	 */
	public Integer getSlowCallRateThreshold() {
		return slowCallRateThreshold;
	}

	/**
	 * @param slowCallRateThreshold the slowCallRateThreshold to set
	 */
	public void setSlowCallRateThreshold(Integer slowCallRateThreshold) {
		this.slowCallRateThreshold = slowCallRateThreshold;
	}

	/**
	 * @return the slowCallDurationThreshold
	 */
	public Integer getSlowCallDurationThreshold() {
		return slowCallDurationThreshold;
	}

	/**
	 * @param slowCallDurationThreshold the slowCallDurationThreshold to set
	 */
	public void setSlowCallDurationThreshold(Integer slowCallDurationThreshold) {
		this.slowCallDurationThreshold = slowCallDurationThreshold;
	}

	/**
	 * @return the permittedNumberOfCallsInHalfOpenState
	 */
	public Integer getPermittedNumberOfCallsInHalfOpenState() {
		return permittedNumberOfCallsInHalfOpenState;
	}

	/**
	 * @param permittedNumberOfCallsInHalfOpenState the
	 * permittedNumberOfCallsInHalfOpenState to set
	 */
	public void setPermittedNumberOfCallsInHalfOpenState(Integer permittedNumberOfCallsInHalfOpenState) {
		this.permittedNumberOfCallsInHalfOpenState = permittedNumberOfCallsInHalfOpenState;
	}

	/**
	 * @return the slidingWindowType
	 */
	public String getSlidingWindowType() {
		return slidingWindowType;
	}

	/**
	 * @param slidingWindowType the slidingWindowType to set
	 */
	public void setSlidingWindowType(String slidingWindowType) {
		this.slidingWindowType = slidingWindowType;
	}

	/**
	 * @return the slidingWindowSize
	 */
	public Integer getSlidingWindowSize() {
		return slidingWindowSize;
	}

	/**
	 * @param slidingWindowSize the slidingWindowSize to set
	 */
	public void setSlidingWindowSize(Integer slidingWindowSize) {
		this.slidingWindowSize = slidingWindowSize;
	}

	/**
	 * @return the minimumNumberOfCalls
	 */
	public Integer getMinimumNumberOfCalls() {
		return minimumNumberOfCalls;
	}

	/**
	 * @param minimumNumberOfCalls the minimumNumberOfCalls to set
	 */
	public void setMinimumNumberOfCalls(Integer minimumNumberOfCalls) {
		this.minimumNumberOfCalls = minimumNumberOfCalls;
	}

	/**
	 * @return the waitDurationInOpenState
	 */
	public Integer getWaitDurationInOpenState() {
		return waitDurationInOpenState;
	}

	/**
	 * @param waitDurationInOpenState the waitDurationInOpenState to set
	 */
	public void setWaitDurationInOpenState(Integer waitDurationInOpenState) {
		this.waitDurationInOpenState = waitDurationInOpenState;
	}

	/**
	 * @return the automaticTransitionFromOpenToHalfOpenEnabled
	 */
	public Boolean getAutomaticTransitionFromOpenToHalfOpenEnabled() {
		return automaticTransitionFromOpenToHalfOpenEnabled;
	}

	/**
	 * @param automaticTransitionFromOpenToHalfOpenEnabled the
	 * automaticTransitionFromOpenToHalfOpenEnabled to set
	 */
	public void setAutomaticTransitionFromOpenToHalfOpenEnabled(Boolean automaticTransitionFromOpenToHalfOpenEnabled) {
		this.automaticTransitionFromOpenToHalfOpenEnabled = automaticTransitionFromOpenToHalfOpenEnabled;
	}

	/**
	 * @return the recordExceptions
	 */
	public List<String> getRecordExceptions() {
		return recordExceptions;
	}

	/**
	 * @param recordExceptions the recordExceptions to set
	 */
	public void setRecordExceptions(List<String> recordExceptions) {
		this.recordExceptions = recordExceptions;
	}

	/**
	 * @return the ignoreExceptions
	 */
	public List<String> getIgnoreExceptions() {
		return ignoreExceptions;
	}

	/**
	 * @param ignoreExceptions the ignoreExceptions to set
	 */
	public void setIgnoreExceptions(List<String> ignoreExceptions) {
		this.ignoreExceptions = ignoreExceptions;
	}

	/**
	 * @return the maxWaitDurationInHalfOpenState
	 */
	public Integer getMaxWaitDurationInHalfOpenState() {
		return maxWaitDurationInHalfOpenState;
	}

	/**
	 * @param maxWaitDurationInHalfOpenState the maxWaitDurationInHalfOpenState to
	 * set
	 */
	public void setMaxWaitDurationInHalfOpenState(Integer maxWaitDurationInHalfOpenState) {
		this.maxWaitDurationInHalfOpenState = maxWaitDurationInHalfOpenState;
	}
}
