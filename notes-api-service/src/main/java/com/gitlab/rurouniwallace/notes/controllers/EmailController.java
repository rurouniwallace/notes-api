package com.gitlab.rurouniwallace.notes.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import javax.ws.rs.core.UriBuilder;

import com.gitlab.rurouniwallace.notes.config.EmailVerificationConfiguration;
import com.gitlab.rurouniwallace.notes.email.ISendsEmails;
import com.gitlab.rurouniwallace.notes.resilience.ResilienceRegistry;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class EmailController extends BaseController {

	private ISendsEmails emailer;

	private EmailVerificationConfiguration emailVerificationConfig;

	public EmailController(final ISendsEmails emailer, final EmailVerificationConfiguration emailVerificationConfig,
			final ResilienceRegistry resilienceRegistry) {
		super(resilienceRegistry);
		this.emailer = emailer;
		this.emailVerificationConfig = emailVerificationConfig;
	}

	public CompletableFuture<Void> sendVerificationEmail(final String emailAddress, final UUID userId) {
		final String resilienceKey = emailer.getResilienceRegistryKey();

		return runAsync(resilienceKey, () -> {
			final Map<String, Object> claims = buildEmailVerificationTokenClaims(emailAddress, userId, new Date());
			final String emailVerificationToken = Jwts.builder()
					.signWith(SignatureAlgorithm.forName(emailVerificationConfig.getAlg()),
							emailVerificationConfig.getKey())
					.addClaims(claims).compact();

			LOGGER.debug("Created email verification token " + emailVerificationToken);

			emailer.sendVerificationEmail(emailAddress,
					UriBuilder.fromUri(emailVerificationConfig.getUrl()).queryParam("email", emailAddress)
							.queryParam("userId", userId.toString()).queryParam("token", emailVerificationToken).build()
							.toString());
			return null;
		});
	}

	/**
	 * Build an email verification token. This will be sent in the user's
	 * verification email and can then be passed along to a dedicated endpoint that
	 * will validate it, and set the user's verification flag to "verified"
	 * 
	 * @return email verification token claims
	 */
	private Map<String, Object> buildEmailVerificationTokenClaims(final String emailAddress, final UUID userId,
			final Date authInstant) {
		final Map<String, Object> claims = new HashMap<>();

		final Date expiration = new Date(authInstant.getTime() + emailVerificationConfig.getTtl() * 1000);

		claims.put("sub", userId);
		claims.put("iat", authInstant.getTime() / 1000);
		claims.put("nbf", authInstant.getTime() / 1000);
		claims.put("exp", expiration.getTime() / 1000);
		claims.put("jti", UUID.randomUUID().toString());
		claims.put("type", EMAIL_VERIFICATION_TOKEN_TYPE);
		claims.put("email", emailAddress);

		return claims;
	}
}
