package com.gitlab.rurouniwallace.notes.config;

import java.time.Duration;
import java.util.List;

import io.github.resilience4j.bulkhead.ThreadPoolBulkheadConfig;
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig.SlidingWindowType;
import io.github.resilience4j.ratelimiter.RateLimiterConfig;
import io.github.resilience4j.retry.RetryConfig;
import io.github.resilience4j.timelimiter.TimeLimiterConfig;

public class ResilienceFactory {

	/**
	 * Circuit breaker config
	 */
	private CircuitBreakerConfiguration circuitBreaker;

	/**
	 * Thread bulkhead config
	 */
	private ThreadPoolBulkheadConfiguration threadPoolBulkhead;

	/**
	 * Rate limiter config
	 */
	private RateLimiterConfiguration rateLimiter;

	/**
	 * Retry configuration
	 */
	private RetryConfiguration retry;

	/**
	 * Time limiter configuration
	 */
	private TimeLimiterConfiguration timeLimiter;

	/**
	 * @return the circuitBreaker
	 */
	public CircuitBreakerConfiguration getCircuitBreaker() {
		return circuitBreaker;
	}

	/**
	 * @param circuitBreaker the circuitBreaker to set
	 */
	public void setCircuitBreaker(CircuitBreakerConfiguration circuitBreaker) {
		this.circuitBreaker = circuitBreaker;
	}

	/**
	 * @return the threadPoolBulkhead
	 */
	public ThreadPoolBulkheadConfiguration getThreadPoolBulkhead() {
		return threadPoolBulkhead;
	}

	/**
	 * @param threadPoolBulkhead the threadPoolBulkhead to set
	 */
	public void setThreadPoolBulkhead(ThreadPoolBulkheadConfiguration threadPoolBulkhead) {
		this.threadPoolBulkhead = threadPoolBulkhead;
	}

	/**
	 * @return the rateLimiter
	 */
	public RateLimiterConfiguration getRateLimiter() {
		return rateLimiter;
	}

	/**
	 * @param rateLimiter the rateLimiter to set
	 */
	public void setRateLimiter(RateLimiterConfiguration rateLimiter) {
		this.rateLimiter = rateLimiter;
	}

	/**
	 * @return the timeLimiter
	 */
	public TimeLimiterConfiguration getTimeLimiter() {
		return timeLimiter;
	}

	/**
	 * @param timeLimiter the timeLimiter to set
	 */
	public void setTimeLimiter(TimeLimiterConfiguration timeLimiter) {
		this.timeLimiter = timeLimiter;
	}

	/**
	 * Build circuit breaker configuration
	 * 
	 * @return circuit breaker configuration
	 */
	public CircuitBreakerConfig buildCircuitBreakerConfig() {

		if (circuitBreaker == null) {
			return CircuitBreakerConfig.ofDefaults();
		}

		final CircuitBreakerConfig.Builder builder = CircuitBreakerConfig.custom();

		if (circuitBreaker.getFailureRateThreshold() != null) {
			builder.failureRateThreshold(circuitBreaker.getFailureRateThreshold());
		}

		if (circuitBreaker.getSlowCallRateThreshold() != null) {
			builder.slowCallRateThreshold(circuitBreaker.getSlowCallRateThreshold());
		}

		if (circuitBreaker.getSlowCallDurationThreshold() != null) {
			builder.slowCallDurationThreshold(Duration.ofMillis(circuitBreaker.getSlowCallDurationThreshold()));
		}

		if (circuitBreaker.getPermittedNumberOfCallsInHalfOpenState() != null) {
			builder.permittedNumberOfCallsInHalfOpenState(circuitBreaker.getPermittedNumberOfCallsInHalfOpenState());
		}

		if (circuitBreaker.getMaxWaitDurationInHalfOpenState() != null) {
			builder.maxWaitDurationInHalfOpenState(
					Duration.ofMillis(circuitBreaker.getMaxWaitDurationInHalfOpenState()));
		}

		if (circuitBreaker.getSlidingWindowType() != null) {
			builder.slidingWindowType(SlidingWindowType.valueOf(circuitBreaker.getSlidingWindowType()));
		}

		if (circuitBreaker.getSlidingWindowSize() != null) {
			builder.slidingWindowSize(circuitBreaker.getSlidingWindowSize());
		}

		if (circuitBreaker.getMinimumNumberOfCalls() != null) {
			builder.minimumNumberOfCalls(circuitBreaker.getMinimumNumberOfCalls());
		}

		if (circuitBreaker.getWaitDurationInOpenState() != null) {
			builder.waitDurationInOpenState(Duration.ofMillis(circuitBreaker.getWaitDurationInOpenState()));
		}

		if (circuitBreaker.getAutomaticTransitionFromOpenToHalfOpenEnabled() != null) {
			builder.automaticTransitionFromOpenToHalfOpenEnabled(
					circuitBreaker.getAutomaticTransitionFromOpenToHalfOpenEnabled());
		}

		final List<String> exceptionsToRecord = circuitBreaker.getRecordExceptions();
		if (circuitBreaker.getRecordExceptions() != null) {
			builder.recordException(throwable -> exceptionsToRecord.contains(throwable.getClass().getName()));
		}

		final List<String> exceptionsToIgnore = circuitBreaker.getIgnoreExceptions();
		if (circuitBreaker.getIgnoreExceptions() != null) {
			builder.ignoreException(throwable -> exceptionsToIgnore.contains(throwable.getClass().getName()));
		}

		return builder.build();
	}

	/**
	 * Build threadpool bulkhead configuration
	 * 
	 * @return threadpool bulkhead configuration
	 */
	public ThreadPoolBulkheadConfig buildThreadPoolBulkheadConfiguration() {
		if (threadPoolBulkhead == null) {
			return ThreadPoolBulkheadConfig.ofDefaults();
		}

		final ThreadPoolBulkheadConfig.Builder builder = ThreadPoolBulkheadConfig.custom();

		if (threadPoolBulkhead.getMaxThreadPoolSize() != null) {
			builder.maxThreadPoolSize(threadPoolBulkhead.getMaxThreadPoolSize());
		}

		if (threadPoolBulkhead.getCoreThreadPoolSize() != null) {
			builder.coreThreadPoolSize(threadPoolBulkhead.getCoreThreadPoolSize());
		}

		if (threadPoolBulkhead.getQueueCapacity() != null) {
			builder.queueCapacity(threadPoolBulkhead.getQueueCapacity());
		}

		if (threadPoolBulkhead.getKeepAliveDuration() != null) {
			builder.keepAliveDuration(Duration.ofMillis(threadPoolBulkhead.getKeepAliveDuration()));
		}

		return builder.build();
	}

	/**
	 * Build rate limiter configuration
	 * 
	 * @return rate limiter configuration
	 */
	public RateLimiterConfig buildRateLimiterConfiguration() {
		if (rateLimiter == null) {
			return RateLimiterConfig.ofDefaults();
		}

		final RateLimiterConfig.Builder builder = RateLimiterConfig.custom();

		if (rateLimiter.getTimeoutDuration() != null) {
			builder.timeoutDuration(Duration.ofSeconds(rateLimiter.getTimeoutDuration()));
		}

		if (rateLimiter.getLimitRefreshPeriod() != null) {
			builder.limitRefreshPeriod(Duration.ofNanos(rateLimiter.getLimitRefreshPeriod()));
		}

		if (rateLimiter.getLimitForPeriod() != null) {
			builder.limitForPeriod(rateLimiter.getLimitForPeriod());
		}

		return builder.build();
	}

	public RetryConfig buildRetryConfiguration() {
		if (retry == null) {
			return RetryConfig.ofDefaults();
		}

		final RetryConfig.Builder builder = RetryConfig.custom();

		if (retry.getMaxAttempts() != null) {
			builder.maxAttempts(retry.getMaxAttempts());
		}

		if (retry.getWaitDuration() != null) {
			builder.waitDuration(Duration.ofMillis(retry.getWaitDuration()));
		}

		return builder.build();
	}

	public TimeLimiterConfig buildTimeLimiterConfiguration() {
		if (timeLimiter == null) {
			return TimeLimiterConfig.ofDefaults();
		}

		final TimeLimiterConfig.Builder builder = TimeLimiterConfig.custom();

		if (timeLimiter.getTimeoutDuration() != null) {
			builder.timeoutDuration(Duration.ofMillis(timeLimiter.getTimeoutDuration()));
		}

		if (timeLimiter.getCancelRunningFuture() != null) {
			builder.cancelRunningFuture(timeLimiter.getCancelRunningFuture());
		}

		return builder.build();
	}
}
