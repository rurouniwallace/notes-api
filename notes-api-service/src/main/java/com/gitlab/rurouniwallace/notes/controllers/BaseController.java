package com.gitlab.rurouniwallace.notes.controllers;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Consumer;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.Response;

import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gitlab.rurouniwallace.notes.config.SessionConfiguration;
import com.gitlab.rurouniwallace.notes.resilience.ResilienceRegistry;
import com.gitlab.rurouniwallace.notes.responses.HttpResponse;
import com.gitlab.rurouniwallace.notes.responses.StandardResponse;
import com.gitlab.rurouniwallace.notes.responses.StatusCode;
import com.gitlab.rurouniwallace.notes.responses.UserResponse;

import io.github.resilience4j.bulkhead.ThreadPoolBulkhead;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.decorators.Decorators;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.timelimiter.TimeLimiter;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.vavr.CheckedFunction0;
import io.vavr.CheckedFunction1;

public abstract class BaseController {

	/**
	 * Event logger
	 */
	protected static final Logger LOGGER = LoggerFactory.getLogger(BaseController.class);

	/**
	 * Authenticated status code that will appear in an auth token
	 */
	protected static final String AUTH_STATUS_AUTHENTICATED = "AUTHENTICATED";

	/**
	 * Status code indicating two-factor auth is required
	 */
	protected static final String AUTH_STATUS_TFA_REQUIRED = "TFA_REQUIRED";

	/**
	 * Token type indicating that a token is to be used for email verification
	 */
	protected static final String EMAIL_VERIFICATION_TOKEN_TYPE = "EMAIL_VERIFICATION";

	/**
	 * Resilience4j registry
	 */
	protected final ResilienceRegistry resilienceRegistry;

	/**
	 * Construct a new instance
	 * 
	 * @param resilienceRegistry resilience registry
	 */
	public BaseController(final ResilienceRegistry resilienceRegistry) {
		this.resilienceRegistry = resilienceRegistry;
	}

	/**
	 * Build an error HTTP response
	 * 
	 * @param httpStatus HTTP status
	 * @param statusCode response status code
	 * @param message error message
	 * @return the HTTP response
	 */
	protected Response buildErrorResponse(final int httpStatus, final StatusCode statusCode, final String message) {
		final UserResponse userResponse = new UserResponse(message, statusCode);

		return Response.status(httpStatus).entity(userResponse).build();
	}

	protected Claims checkAuth(final Cookie sessionCookie, final SessionConfiguration sessionConfig, final UUID userId,
			final AsyncResponse response) {
		return checkAuth(sessionCookie, sessionConfig, userId, response, Arrays.asList(AUTH_STATUS_AUTHENTICATED));
	}

	/**
	 * Check a user's authentication
	 * 
	 * @param sessionCookie the session cookie
	 * @param sessionConfig session configuration
	 * @param userId the ID of the user to authenticate
	 * @param response suspended respose. The response will be resumed if
	 * authentication fails
	 * @param allowedStatuses authentication statuses that are considered valid
	 * authentication
	 * @return
	 */
	protected Claims checkAuth(final Cookie sessionCookie, final SessionConfiguration sessionConfig, final UUID userId,
			final AsyncResponse response, final List<String> allowedStatuses) {
		final StandardResponse denyResponse = new StandardResponse(
				"Client not authorized to perform the requested action", StatusCode.FORBIDDEN);

		if (sessionCookie == null) {
			response.resume(Response.status(HttpStatus.FORBIDDEN_403).entity(denyResponse).build());
		}

		Claims claims = null;
		try {
			claims = Jwts.parser().setSigningKey(sessionConfig.getKey()).parseClaimsJws(sessionCookie.getValue())
					.getBody();
		} catch (final JwtException e) {
			LOGGER.info("JWT failed validation", e);
			response.resume(Response.status(HttpStatus.FORBIDDEN_403).entity(denyResponse).build());
		}

		LOGGER.info("Auth claims: " + claims);

		if (!allowedStatuses.contains(claims.get("status"))) {
			response.resume(Response.status(HttpStatus.FORBIDDEN_403).entity(denyResponse).build());
		}

		if (!claims.getSubject().equals(userId.toString())) {
			response.resume(Response.status(HttpStatus.FORBIDDEN_403).entity(denyResponse).build());
		}

		return claims;
	}

	/**
	 * Build a completable future instance from a consumer.
	 * 
	 * @param <T> type of object being consumed
	 * @param resilienceKey resilience key to use for resilience4j settings
	 * @param consumer the consumer to decorate with resilience settings
	 * @param object the object passed to the consumer
	 * @param errorHandler handles errors
	 * @return
	 */
	protected <T, R> CompletableFuture<R> buildCompletableFuture(final String resilienceKey,
			final CheckedFunction1<T, R> consumer, final T object, final Consumer<Throwable> errorHandler) {
		final CircuitBreaker circuitBreaker = getCircuitBreaker(resilienceKey);

		final ThreadPoolBulkhead threadPoolBulkhead = getThreadPoolBulkhead(resilienceKey);

		final TimeLimiter timeLimiter = getTimeLimiter(resilienceKey);
		ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

		final CheckedFunction1<T, R> consumerWithCircuitBreaker = Decorators.ofCheckedFunction(consumer)
				.withCircuitBreaker(circuitBreaker).decorate();

		// removing time limiter for now. It was causing the JVM to run out of threads,
		// for some reason
		return Decorators.ofSupplier(() -> {
			try {
				return consumerWithCircuitBreaker.apply(object);
			} catch (final Throwable e) {
				errorHandler.accept(e);
				return null;
			}
		}).withThreadPoolBulkhead(threadPoolBulkhead).get().toCompletableFuture();
	}

	/**
	 * Create a completable future lambda decorated with resilience settings. This
	 * is useful for when completable futures need to be chained together, with the
	 * result of one being brokered to the next.
	 * 
	 * @param <T> type of the object emitted by the completable future
	 * @param resilienceKey resilience key to use for resilience4j settings
	 * @param supplier object supplier, the result of it will be emitted by the
	 * completable future
	 * @param errorHandler handle errors thrown by the supplier
	 * @return
	 */
	protected <T> CompletableFuture<T> buildCompletableFuture(final String resilienceKey,
			final CheckedFunction0<T> supplier, final Consumer<Throwable> errorHandler) {
		final CircuitBreaker circuitBreaker = getCircuitBreaker(resilienceKey);

		final ThreadPoolBulkhead threadPoolBulkhead = getThreadPoolBulkhead(resilienceKey);

		final TimeLimiter timeLimiter = getTimeLimiter(resilienceKey);
		ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

		final CheckedFunction0<T> supplierWithCircuitBreaker = Decorators.ofCheckedSupplier(supplier)
				.withCircuitBreaker(circuitBreaker).decorate();

		// removing time limiter for now. It was causing the JVM to run out of threads,
		// for some reason
		return Decorators.ofSupplier(() -> {
			try {
				return supplierWithCircuitBreaker.apply();
			} catch (final Throwable e) {
				errorHandler.accept(e);
				return null;
			}
		}).withThreadPoolBulkhead(threadPoolBulkhead).get().toCompletableFuture();
	}

	/**
	 * Execute a function asynchronously, with a configured resilience4j circuit
	 * breaker and a threadpool bulkhead
	 * 
	 * @param resilienceKey key to resilience configs to use
	 * @param response asynchronous response
	 * @param supplier lambda function to execute
	 * @return
	 */
	protected CompletableFuture<Void> respondAsync(final String resilienceKey, final AsyncResponse response,
			final CheckedFunction0<? extends HttpResponse> supplier) {

		final CircuitBreaker circuitBreaker = getCircuitBreaker(resilienceKey);

		final ThreadPoolBulkhead threadPoolBulkhead = getThreadPoolBulkhead(resilienceKey);

		final TimeLimiter timeLimiter = getTimeLimiter(resilienceKey);
		ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

		final CheckedFunction0<? extends HttpResponse> supplierWithCircuitBreaker = Decorators
				.ofCheckedSupplier(supplier).withCircuitBreaker(circuitBreaker).decorate();

		// removing time limiter for now. It was causing the JVM to run out of threads,
		// for some reason
		final CompletableFuture<? extends HttpResponse> async = Decorators.ofSupplier(() -> {
			try {
				return supplierWithCircuitBreaker.apply();
			} catch (final Throwable e) {
				return handleException(e);
			}
		}).withThreadPoolBulkhead(threadPoolBulkhead).get().toCompletableFuture();

		return async.thenAccept(result -> {
			LOGGER.info(String.format("Returning value: %s", result.getHttpStatus()));
			response.resume(Response.status(result.getHttpStatus()).entity(result).build());
		}).exceptionally(e -> {
			final StandardResponse errorResponse = handleException(e);
			response.resume(Response.status(errorResponse.getHttpStatus()).entity(errorResponse).build());

			// without this the compiler complains
			return null;
		});
	}

	/**
	 * Run a simple function with no arguments and no returns, asynchronously.
	 * Optionally, retry settings can be specified.
	 * 
	 * @param resilienceKey resilience registry config key to use
	 * @param runnable the lambda function to run
	 * @return the worker thread running asynchronously
	 */
	/*
	 * protected CompletableFuture<Void> runAsync(final String resilienceKey, final
	 * CheckedFunction0<Void> runnable) { return runAsync(resilienceKey,
	 * Optional.empty(), runnable); }
	 */

	protected <Type> CompletableFuture<Void> runAsync(final String resilienceKey,
			final CheckedFunction0<Type> runnable) {
		return runAsync(resilienceKey, Optional.empty(), runnable);
	}

	/**
	 * Run a simple function with no arguments and no returns, asynchronously.
	 * Optionally, retry settings can be specified.
	 * 
	 * @param resilienceKey resilience registry config key to use
	 * @param retry retry settings to use during execution
	 * @param runnable the lambda function to run
	 * @return the worker thread running asynchronously
	 */
	protected <Type> CompletableFuture<Void> runAsync(final String resilienceKey, final Optional<Retry> retry,
			final CheckedFunction0<Type> runnable) {
		final CircuitBreaker circuitBreaker = getCircuitBreaker(resilienceKey);

		final ThreadPoolBulkhead threadPoolBulkhead = getThreadPoolBulkhead(resilienceKey);

		final TimeLimiter timeLimiter = getTimeLimiter(resilienceKey);
		ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

		final CheckedFunction0<Type> runnableWithCircuitBreaker = Decorators.ofCheckedSupplier(runnable)
				.withCircuitBreaker(circuitBreaker).decorate();

		final CheckedFunction0<Type> runnableWithRetry = (retry.isPresent())
				? Decorators.ofCheckedSupplier(runnableWithCircuitBreaker).withRetry(retry.get()).decorate()
				: runnableWithCircuitBreaker;

		// removing time limiter for now. It was causing the JVM to run out of threads,
		// for some reason
		final CompletableFuture<Type> async = Decorators.ofCallable(() -> {
			try {
				return runnableWithRetry.apply();
			} catch (final Throwable e) {
				LOGGER.error("Failed to execute asynchronous call with resilience key " + resilienceKey, e);
				return null;
			}
		}).withThreadPoolBulkhead(threadPoolBulkhead).get().toCompletableFuture();

		return async.thenRunAsync(() -> {
			LOGGER.debug("Asynchronous execution with resilience key " + resilienceKey + " completed");
		}).exceptionally(e -> {
			LOGGER.error("Asynchronous execution with resilience key " + resilienceKey + " failed", e);

			// compiler requires this
			return null;
		});

	}

	protected StandardResponse handleException(final Throwable e) {
		LOGGER.error("Dependency failed", e);
		return new StandardResponse(StatusCode.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR_500);
	}

	protected CircuitBreaker getCircuitBreaker(final String resilienceKey) {
		final CircuitBreaker circuitBreaker;
		if (resilienceRegistry.getCircuitBreakers().containsKey(resilienceKey)) {
			circuitBreaker = resilienceRegistry.getCircuitBreakers().get(resilienceKey);
		} else {
			LOGGER.warn("Circuit breaker settings '" + resilienceKey + "' not configured. Using defaults instead.");
			circuitBreaker = CircuitBreaker.ofDefaults(resilienceKey);
		}
		return circuitBreaker;
	}

	protected ThreadPoolBulkhead getThreadPoolBulkhead(final String resilienceKey) {
		final ThreadPoolBulkhead threadPoolBulkhead;
		if (resilienceRegistry.getThreadPoolBulkheads().containsKey(resilienceKey)) {
			threadPoolBulkhead = resilienceRegistry.getThreadPoolBulkheads().get(resilienceKey);
		} else {
			LOGGER.warn(
					"Thread pool bulkhead settings '" + resilienceKey + "' not configured. Using defaults instead.");
			threadPoolBulkhead = ThreadPoolBulkhead.ofDefaults(resilienceKey);
		}
		return threadPoolBulkhead;
	}

	protected TimeLimiter getTimeLimiter(final String resilienceKey) {
		final TimeLimiter timeLimiter;
		if (resilienceRegistry.getTimeLimiters().containsKey(resilienceKey)) {
			timeLimiter = resilienceRegistry.getTimeLimiters().get(resilienceKey);
		} else {
			LOGGER.warn("Time limiter settings '" + resilienceKey + "' not configured. Using defaults instead.");
			timeLimiter = TimeLimiter.ofDefaults();
		}

		return timeLimiter;
	}
}
