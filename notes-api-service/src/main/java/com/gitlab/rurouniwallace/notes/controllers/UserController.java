package com.gitlab.rurouniwallace.notes.controllers;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gitlab.rurouniwallace.notes.dao.IAccessesUsers;
import com.gitlab.rurouniwallace.notes.exceptions.EntityAlreadyExistsException;
import com.gitlab.rurouniwallace.notes.exceptions.EntityNotFoundException;
import com.gitlab.rurouniwallace.notes.models.User;
import com.gitlab.rurouniwallace.notes.passwords.verification.IChecksPasswordCompromised;
import com.gitlab.rurouniwallace.notes.resilience.ResilienceRegistry;
import com.gitlab.rurouniwallace.notes.responses.StandardResponse;
import com.gitlab.rurouniwallace.notes.responses.StatusCode;
import com.gitlab.rurouniwallace.notes.responses.UserResponse;

import io.vavr.CheckedFunction0;
import io.vavr.CheckedFunction1;

/**
 * User resource logic controller
 */
public class UserController extends BaseController {

	/**
	 * Event logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

	/**
	 * User data access interface
	 */
	private final IAccessesUsers userDao;

	/**
	 * Password checker
	 */
	private final IChecksPasswordCompromised passwordChecker;

	/**
	 * Emailer
	 */
	private final EmailController emailController;

	/**
	 * Construct a new instance
	 * 
	 * @param userDao the user data access interface
	 */
	public UserController(final IAccessesUsers userDao, final IChecksPasswordCompromised passwordChecker,
			final EmailController emailController, final ResilienceRegistry resilienceRegistry) {
		super(resilienceRegistry);
		this.userDao = userDao;
		this.passwordChecker = passwordChecker;
		this.emailController = emailController;
	}

	/**
	 * Create a new user
	 * 
	 * @param user the user to create
	 * @param response the response
	 */
	public void createUser(final User user, final AsyncResponse response) {
		if (user.getUuid() != null) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"UUID may not be asserted when creating a user"));
			return;
		}

		if (user.getEmail() == null || user.getEmail().isEmpty()) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"Email address required"));
			return;
		}

		if (user.getPassword() == null || user.getPassword().isEmpty()) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"Password required"));
			return;
		}

		if (user.isEmailVerified() != null) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"Email verification may not be asserted. Use email verification endpoint"));
			return;
		}

		user.setEmailVerified(false);

		final String passwordCheckerResilienceKey = passwordChecker.getResilienceRegistryKey();

		final CheckedFunction0<Integer> compromisedPasswordCheckResultSupplier = () -> {
			return passwordChecker.isCompromised(user.getPassword());
		};

		final CompletableFuture<Integer> futureCompromisedPasswordResult = buildCompletableFuture(
				passwordCheckerResilienceKey, compromisedPasswordCheckResultSupplier, (exception) -> {
					final StandardResponse responseBody = handleException(exception);
					response.resume(responseBody);
				});

		final CheckedFunction1<Integer, UserResponse> createUserThread = (numResults) -> {
			User userFromDao;

			if (numResults > 0) {
				return new UserResponse("Password has been compromised on " + numResults + " other sites",
						StatusCode.COMPROMISED_PASSWORD, HttpStatus.UNPROCESSABLE_ENTITY_422);
			}

			if (user.getTwoFactorAuthPreference() != null && user.getTwoFactorAuthPreference() != 0) {
				if (StringUtils.isBlank(user.getOtpSecret())) {
					return new UserResponse("OTP secret must be set", StatusCode.INVALID_ARGUMENTS,
							HttpStatus.UNPROCESSABLE_ENTITY_422);
				}

				if (StringUtils.isBlank(user.getOtpAlgorithm())) {
					return new UserResponse("OTP algorithm must be set", StatusCode.INVALID_ARGUMENTS,
							HttpStatus.UNPROCESSABLE_ENTITY_422);
				}

				if (!userDao.isOtpAlgorithmValid(user.getOtpAlgorithm())) {
					return new UserResponse("OTP algorithm invalid", StatusCode.INVALID_ARGUMENTS,
							HttpStatus.UNPROCESSABLE_ENTITY_422);
				}

				if (user.getOtpStep() == null) {
					return new UserResponse("OTP step must be set", StatusCode.INVALID_ARGUMENTS,
							HttpStatus.UNPROCESSABLE_ENTITY_422);
				}

				if (user.getOtpDigits() == null) {
					return new UserResponse("OTP number of digits must be set", StatusCode.INVALID_ARGUMENTS,
							HttpStatus.UNPROCESSABLE_ENTITY_422);
				}

				if (user.getOtpDigits() <= 0) {
					return new UserResponse("OTP number of digits must be greater than 0", StatusCode.INVALID_ARGUMENTS,
							HttpStatus.UNPROCESSABLE_ENTITY_422);
				}
			}

			try {
				userFromDao = userDao.registerUser(user);
			} catch (final EntityAlreadyExistsException e) {
				return new UserResponse("A user with the specified email address already exists",
						StatusCode.ENTITY_ALREADY_EXISTS, HttpStatus.UNPROCESSABLE_ENTITY_422);
			}

			emailController.sendVerificationEmail(user.getEmail(), userFromDao.getUuid());

			return new UserResponse(userFromDao, StatusCode.SUCCESS, HttpStatus.CREATED_201);
		};

		futureCompromisedPasswordResult.thenAcceptAsync(numResults -> {
			final String userResilienceKey = userDao.getResilienceRegistryKey();

			final CompletableFuture<UserResponse> userCreation = buildCompletableFuture(userResilienceKey,
					createUserThread, numResults, (throwable) -> {
						final StandardResponse errorResponse = handleException(throwable);

						response.resume(Response.status(errorResponse.getHttpStatus()).entity(errorResponse).build());
					});

			userCreation.thenAcceptAsync((result) -> {
				LOGGER.debug("User creation complete");
				response.resume(Response.status(result.getHttpStatus()).entity(result).build());
			});

			LOGGER.debug("User creation thread started");
		});
	}

	public void lookupUser(final UUID userId, final AsyncResponse response) {
		if (userId == null) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"UUID may not be null"));
			return;
		}

		final String resilienceKey = userDao.getResilienceRegistryKey();

		respondAsync(resilienceKey, response, () -> {
			final User user;
			try {
				user = userDao.lookupUser(userId);
			} catch (final EntityNotFoundException e) {
				return new UserResponse("User with ID " + userId + " not found", StatusCode.ENTITY_NOT_FOUND,
						HttpStatus.NOT_FOUND_404);
			}

			return new UserResponse(user, StatusCode.SUCCESS);
		});
	}

	public void updateUser(final UUID userId, final User user, final AsyncResponse response) {
		if (userId == null) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"UUID may not be null"));
			return;
		}

		if (user.getUuid() != null) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"UUID may not be asserted when updating a user"));
			return;
		}

		if (user.getPassword() != null) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"Password may not be asserted when updating a user. Use password update endpoint"));
			return;
		}

		if (user.isEmailVerified() != null) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"Email verification may not be asserted. Use email verification endpoint"));
			return;
		}

		final String resilienceKey = userDao.getResilienceRegistryKey();

		respondAsync(resilienceKey, response, () -> {
			final User updatedUser;
			try {
				updatedUser = userDao.updateUser(userId, user);
			} catch (final EntityNotFoundException e) {
				return new UserResponse("User with ID " + userId + " not found", StatusCode.ENTITY_NOT_FOUND,
						HttpStatus.NOT_FOUND_404);
			}
			return new UserResponse(updatedUser, StatusCode.SUCCESS);
		});
	}
}
