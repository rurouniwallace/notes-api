package com.gitlab.rurouniwallace.notes.resilience;

public interface IEvaluatesRetryOnResult {

	public boolean checkRetry(final Object result);
}
