package com.gitlab.rurouniwallace.notes.email.sparkpost;

public class TransmissionRecipient {

	private String address;

	public TransmissionRecipient(final String address) {
		this.address = address;
	}

	/**
	 * @return the recipient
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param recipient the recipient to set
	 */
	public void setAddress(String recipient) {
		this.address = recipient;
	}
}
