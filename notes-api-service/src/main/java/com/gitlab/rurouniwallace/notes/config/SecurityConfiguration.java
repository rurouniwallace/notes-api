package com.gitlab.rurouniwallace.notes.config;

/**
 * Security configuration data
 */
public class SecurityConfiguration {

	/**
	 * Bcrypt hash cost factor. The higher the cost, the slower and more secure the
	 * algorithm
	 */
	private Integer hashCost;

	/**
	 * AES encryption initialization vector
	 */
	private String initializationVector;

	/**
	 * @return the initializationVector
	 */
	public String getInitializationVector() {
		return initializationVector;
	}

	/**
	 * @param initializationVector the initializationVector to set
	 */
	public void setInitializationVector(String initializationVector) {
		this.initializationVector = initializationVector;
	}

	/**
	 * Construct a new instance
	 */
	public SecurityConfiguration() {
		// empty constructor
	}

	/**
	 * Construct a new instance
	 * 
	 * @param hashCost Bcrypt hash cost factor
	 */
	public SecurityConfiguration(final Integer hashCost) {
		this.hashCost = hashCost;
	}

	/**
	 * @return the hashCost
	 */
	public Integer getHashCost() {
		return hashCost;
	}

	/**
	 * @param hashCost the hashCost to set
	 */
	public void setHashCost(final Integer hashCost) {
		this.hashCost = hashCost;
	}
}
