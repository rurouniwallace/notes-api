package com.gitlab.rurouniwallace.notes.dao.encryptedsqldao;

import java.util.Map;

import com.gitlab.rurouniwallace.notes.models.Folder;
import com.gitlab.rurouniwallace.notes.models.User;

public class UserData {

	private User user;

	private Map<String, Folder> folders;

	public UserData() {

	}

	public UserData(final User user) {
		this.user = user;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the folders
	 */
	public Map<String, Folder> getFolders() {
		return folders;
	}

	/**
	 * @param folders the folders to set
	 */
	public void setFolders(Map<String, Folder> folders) {
		this.folders = folders;
	}
}
