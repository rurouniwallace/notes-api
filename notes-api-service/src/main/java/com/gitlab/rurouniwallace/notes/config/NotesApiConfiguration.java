package com.gitlab.rurouniwallace.notes.config;

import java.util.HashMap;
import java.util.Map;

import com.gitlab.rurouniwallace.notes.resilience.ResilienceRegistry;

import io.dropwizard.Configuration;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import io.github.resilience4j.bulkhead.ThreadPoolBulkhead;
import io.github.resilience4j.bulkhead.ThreadPoolBulkheadRegistry;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.github.resilience4j.ratelimiter.RateLimiter;
import io.github.resilience4j.ratelimiter.RateLimiterRegistry;
import io.github.resilience4j.timelimiter.TimeLimiter;
import io.github.resilience4j.timelimiter.TimeLimiterRegistry;

/**
 * Application-level configurations
 */
public class NotesApiConfiguration extends Configuration {

	/**
	 * Swagger settings
	 */
	private SwaggerBundleConfiguration swagger;

	/**
	 * Security settings
	 */
	private SecurityConfiguration security;

	/**
	 * SQL configurations
	 */
	private SqlFactory sql;

	/**
	 * Auth session configuration
	 */
	private SessionConfiguration session;

	/**
	 * Sparkpost configuration
	 */
	private SparkpostConfiguration sparkpost;

	/**
	 * Email verification token configurations
	 */
	private EmailVerificationConfiguration emailVerification;

	/**
	 * Twilio configuration
	 */
	private TwilioConfiguration twilio;

	/**
	 * Nexmo configuration
	 */
	private NexmoConfiguration nexmo;

	/**
	 * HaveIBeenPwned API configuration
	 */
	private HIBPConfiguration hibp;

	/**
	 * @return the nexmo
	 */
	public NexmoConfiguration getNexmo() {
		return nexmo;
	}

	/**
	 * @param nexmo the nexmo to set
	 */
	public void setNexmo(NexmoConfiguration nexmo) {
		this.nexmo = nexmo;
	}

	/**
	 * Resilience configuration
	 */
	private Map<String, ResilienceFactory> resilience;

	/**
	 * @return the swagger
	 */
	public SwaggerBundleConfiguration getSwagger() {
		return swagger;
	}

	/**
	 * @param swagger the swagger to set
	 */
	public void setSwagger(SwaggerBundleConfiguration swagger) {
		this.swagger = swagger;
	}

	/**
	 * @return the security
	 */
	public SecurityConfiguration getSecurity() {
		return security;
	}

	/**
	 * @param security the security to set
	 */
	public void setSecurity(SecurityConfiguration security) {
		this.security = security;
	}

	/**
	 * @return the sql
	 */
	public SqlFactory getSql() {
		return sql;
	}

	/**
	 * @param sql the sql to set
	 */
	public void setSql(SqlFactory sql) {
		this.sql = sql;
	}

	/**
	 * @return the session
	 */
	public SessionConfiguration getSession() {
		return session;
	}

	/**
	 * @param session the session to set
	 */
	public void setSession(SessionConfiguration session) {
		this.session = session;
	}

	/**
	 * @return the resilience
	 */
	public Map<String, ResilienceFactory> getResilience() {
		return resilience;
	}

	/**
	 * @param resilience the resilience to set
	 */
	public void setResilience(Map<String, ResilienceFactory> resilience) {
		this.resilience = resilience;
	}

	/**
	 * @return the sparkpost
	 */
	public SparkpostConfiguration getSparkpost() {
		return sparkpost;
	}

	/**
	 * @param sparkpost the sparkpost to set
	 */
	public void setSparkpost(SparkpostConfiguration sparkpost) {
		this.sparkpost = sparkpost;
	}

	/**
	 * Build in-memory registry for resilience4j settings
	 * 
	 * @return resilience4j settings registry
	 */
	public ResilienceRegistry buildResilienceRegistry() {

		// instead of fighting with resilience4j's registries, we're using our own
		// "registry" using hash maps
		final Map<String, CircuitBreaker> circuitBreakers = new HashMap<>();
		final Map<String, ThreadPoolBulkhead> threadPoolBulkheads = new HashMap<>();
		final Map<String, RateLimiter> rateLimiters = new HashMap<>();
		final Map<String, TimeLimiter> timeLimiters = new HashMap<>();

		for (final String key : resilience.keySet()) {
			circuitBreakers.put(key,
					CircuitBreakerRegistry.of(resilience.get(key).buildCircuitBreakerConfig()).circuitBreaker(key));
			threadPoolBulkheads.put(key, ThreadPoolBulkheadRegistry
					.of(resilience.get(key).buildThreadPoolBulkheadConfiguration()).bulkhead(key));
			rateLimiters.put(key,
					RateLimiterRegistry.of(resilience.get(key).buildRateLimiterConfiguration()).rateLimiter(key));
			timeLimiters.put(key,
					TimeLimiterRegistry.of(resilience.get(key).buildTimeLimiterConfiguration()).timeLimiter(key));

		}

		return new ResilienceRegistry(circuitBreakers, threadPoolBulkheads, rateLimiters, timeLimiters);
	}

	/**
	 * @return the emailVerificationToken
	 */
	public EmailVerificationConfiguration getEmailVerification() {
		return emailVerification;
	}

	/**
	 * @param emailVerification the emailVerificationToken to set
	 */
	public void setEmailVerification(EmailVerificationConfiguration emailVerification) {
		this.emailVerification = emailVerification;
	}

	/**
	 * @return the twilio
	 */
	public TwilioConfiguration getTwilio() {
		return twilio;
	}

	/**
	 * @param twilio the twilio to set
	 */
	public void setTwilio(TwilioConfiguration twilio) {
		this.twilio = twilio;
	}

	/**
	 * @return the hibp
	 */
	public HIBPConfiguration getHibp() {
		return hibp;
	}

	/**
	 * @param hibp the hibp to set
	 */
	public void setHibp(HIBPConfiguration hibp) {
		this.hibp = hibp;
	}
}
