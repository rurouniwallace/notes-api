package com.gitlab.rurouniwallace.notes.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gitlab.rurouniwallace.notes.exceptions.DataAccessException;
import com.gitlab.rurouniwallace.notes.exceptions.EntityNotFoundException;
import com.gitlab.rurouniwallace.notes.models.Note;

/**
 * Data access layer for accessing data in a Lucene index
 */
public class LuceneDao implements IIndexesEntities {

	/**
	 * Event logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(LuceneDao.class);

	/**
	 * Resilience configuration key
	 */
	private static final String RESILIENCE_KEY = "lucene";

	/**
	 * Index directory
	 */
	private final Directory directory;

	/**
	 * Text analyzer
	 */
	private final Analyzer analyzer;

	/**
	 * Index writer
	 */
	private final IndexWriter writer;

	/**
	 * Construct a new instance
	 * 
	 * @param directory index directory
	 * @param analyzer token/text analyzer
	 */
	public LuceneDao(final Directory directory, final Analyzer analyzer, final IndexWriter writer) {
		this.directory = directory;
		this.analyzer = analyzer;
		this.writer = writer;
	}

	/**
	 * Insert a note into the index
	 * 
	 * @param userId ID of the user whose note we're indexing
	 * @param note the note we're inserting into the index
	 * @throws DataAccessException if an error occurs indexing the note
	 */
	@Override
	public void indexNote(final UUID userId, final Note note) throws DataAccessException {
		try {
			final Document document = writeNoteToDocument(new Document(), note);

			writer.addDocument(document);
			writer.commit();
		} catch (final IOException e) {
			throw new DataAccessException("Failed to index note", e);
		}
	}

	/**
	 * Update an already indexed note
	 * 
	 * @param userId the ID of the user whose note is being updated
	 * @param noteId the ID of the note being updated
	 * @param updatePayload the update to make to the note
	 * @throws DataAccessException if an error occurs updating the index
	 */
	@Override
	public void updateIndexedNote(final UUID userId, final UUID noteId, final Note updatePayload)
			throws DataAccessException {
		try {
			final IndexReader reader = DirectoryReader.open(directory);
			final IndexSearcher searcher = new IndexSearcher(reader);

			final ScoreDoc[] hits = lookupNote(userId, noteId, searcher);

			if (hits.length == 0) {
				throw new EntityNotFoundException("Unable to re-index note " + noteId + ". Not found in index");
			}

			if (hits.length > 1) {
				throw new DataAccessException("Multiple notes with ID " + noteId + " found in index");
			}

			final int docId = hits[0].doc;

			final Document documentToUpdate = searcher.doc(docId);

			final Note updatedNote = buildNoteFromDocument(documentToUpdate);
			updatedNote.update(updatePayload);

			final Document updatedDocument = writeNoteToDocument(new Document(), updatedNote);

			writer.updateDocument(new Term("uuid", noteId.toString()), updatedDocument);
			writer.commit();

		} catch (final IOException e) {
			throw new DataAccessException("Failed to update note in index", e);
		}
	}

	/**
	 * Delete a note from the index
	 * 
	 * @param userId ID of the user whose note is being deleted
	 * @param noteId ID of the note being deleted
	 * @throws DataAccessException if an error occurs deleting the note
	 */
	@Override
	public void deleteIndexedNote(final UUID userId, final UUID noteId) throws DataAccessException {

		try {
			final IndexReader reader = DirectoryReader.open(directory);
			final IndexSearcher searcher = new IndexSearcher(reader);

			final ScoreDoc[] hits = lookupNote(userId, noteId, searcher);

			if (hits.length == 0) {
				throw new EntityNotFoundException("Unabled to re-index note " + noteId + ". Not found in index");
			}

			if (hits.length > 1) {
				throw new DataAccessException("Multiple notes with ID " + noteId + " found in index");
			}

			writer.deleteDocuments(new Term("uuid", noteId.toString()));
			writer.commit();
		} catch (final IOException e) {
			throw new DataAccessException("Failed to delete note from index", e);
		}
	}

	/**
	 * Search the index for notes matching a set of keywords
	 * 
	 * @param userId the ID of the user whose notes are being searched
	 * @param terms set of search terms
	 * @param numResults number of results to search for
	 * @throws DataAccessException if an error occurs searching the index
	 */
	@Override
	public List<Note> searchNotes(final UUID userId, final Set<String> terms, final int numResults)
			throws DataAccessException {
		try {
			if (directory.listAll().length == 0) {
				return new ArrayList<Note>();
			}

			final IndexReader reader = DirectoryReader.open(directory);
			final IndexSearcher searcher = new IndexSearcher(reader);

			final BooleanQuery.Builder fullQueryBuilder = new BooleanQuery.Builder();

			final TermQuery userIdQuery = new TermQuery(new Term("userId", userId.toString()));
			fullQueryBuilder.add(userIdQuery, BooleanClause.Occur.MUST);
			for (final String term : terms) {

				// the term can appear in either the title or the body
				final TermQuery titleQuery = new TermQuery(new Term("title", term.toLowerCase()));
				final TermQuery bodyQuery = new TermQuery(new Term("body", term.toLowerCase()));

				final BooleanQuery.Builder searchTermQueryBuilder = new BooleanQuery.Builder();
				searchTermQueryBuilder.add(bodyQuery, BooleanClause.Occur.SHOULD);
				searchTermQueryBuilder.add(titleQuery, BooleanClause.Occur.SHOULD);

				fullQueryBuilder.add(searchTermQueryBuilder.build(), BooleanClause.Occur.MUST);
			}

			LOGGER.info("Querying Lucene: " + fullQueryBuilder.build().toString());

			final TopDocs docs = searcher.search(fullQueryBuilder.build(), numResults);

			return getNotesFromTopDocs(searcher, docs);

		} catch (IOException e) {
			throw new DataAccessException("Failed to search for note in index", e);
		}
	}

	/**
	 * Look up a note in the index by user ID and note ID
	 * 
	 * @param userId the ID of the user whose note is being queried
	 * @param noteId ID of the note being queried
	 * @param searcher the searcher to use in the search
	 * @return top scoring results
	 * @throws IOException if an error occurs
	 */
	private ScoreDoc[] lookupNote(final UUID userId, final UUID noteId, final IndexSearcher searcher)
			throws IOException {
		final BooleanQuery.Builder queryBuilder = new BooleanQuery.Builder();
		queryBuilder.add(new TermQuery(new Term("userId", userId.toString())), BooleanClause.Occur.MUST);
		queryBuilder.add(new TermQuery(new Term("uuid", noteId.toString())), BooleanClause.Occur.MUST);

		LOGGER.info("Querying Lucene: " + queryBuilder.build());

		final TopDocs docs = searcher.search(queryBuilder.build(), 1);

		return docs.scoreDocs;
	}

	/**
	 * Read a score doc into a list of notes
	 * 
	 * @param searcher the index searcher
	 * @param docs docs to read
	 * @return list of notes
	 * @throws IOException if an error occurs
	 */
	private List<Note> getNotesFromTopDocs(final IndexSearcher searcher, final TopDocs docs) throws IOException {
		final ScoreDoc[] hits = docs.scoreDocs;

		final List<Note> notes = new ArrayList<>();

		for (int i = 0; i < hits.length; i++) {
			final int docId = hits[i].doc;

			final Document document = searcher.doc(docId);

			final Note note = buildNoteFromDocument(document);

			notes.add(note);
		}

		return notes;
	}

	/**
	 * Write a note to a document
	 * 
	 * @param document the base document to write to
	 * @param note the note to write to the document
	 * @return the document with the note's data written to it
	 */
	private Document writeNoteToDocument(final Document document, final Note note) {
		if (note.getTitle() != null) {
			document.add(new TextField("title", note.getTitle(), Field.Store.YES));
		}

		if (note.getBody() != null) {
			document.add(new TextField("body", note.getBody(), Field.Store.YES));
		}

		if (note.getUuid() != null) {
			document.add(new StringField("uuid", note.getUuid().toString(), Field.Store.YES));
		}

		if (note.getUser() != null) {
			document.add(new StringField("userId", note.getUser().toString(), Field.Store.YES));
		}

		if (note.getFolder() != null) {
			document.add(new StringField("folderId", note.getFolder().toString(), Field.Store.YES));
		}

		if (note.getCreated() != null) {
			document.add(new StoredField("created", note.getCreated().getTime()));
		}

		if (note.getLastUpdated() != null) {
			document.add(new StoredField("lastUpdated", note.getLastUpdated().getTime()));
		}

		return document;
	}

	/**
	 * Build an instance of note from a document
	 * 
	 * @param document the document
	 * @return the note from the document
	 */
	private Note buildNoteFromDocument(final Document document) {
		final Note note = new Note();

		if (document.get("uuid") != null) {
			note.setUuid(UUID.fromString(document.get("uuid")));
		}

		if (document.get("folderId") != null) {
			note.setFolder(UUID.fromString(document.get("folderId")));
		}

		if (document.get("userId") != null) {
			note.setUser(UUID.fromString(document.get("userId")));
		}

		note.setTitle(document.get("title"));
		note.setBody(document.get("body"));

		if (document.get("lastUpdated") != null) {
			note.setLastUpdated(new Date(Long.parseLong(document.get("lastUpdated"))));
		}

		if (document.get("created") != null) {
			note.setCreated(new Date(Long.parseLong(document.get("created"))));
		}

		return note;
	}

	/**
	 * Open a new index writer
	 * 
	 * @return index writer
	 * @throws DataAccessException if closing the writer fails
	 */
	private IndexWriter openWriter() throws DataAccessException {
		final IndexWriterConfig config = new IndexWriterConfig(analyzer);
		try {
			return new IndexWriter(directory, config);
		} catch (final IOException e) {
			throw new DataAccessException("Failed to open writer", e);
		}
	}

	/**
	 * Close an index writer
	 * 
	 * @param writer the index writer to close
	 * @throws DataAccessException if closing the writer fails
	 */
	private void closeWriter(final IndexWriter writer) throws DataAccessException {
		try {
			writer.close();
		} catch (final IOException e) {
			throw new DataAccessException("Failed to close index writer", e);
		}
	}

	/**
	 * Get resilience config key
	 * 
	 * @return resilience config key
	 */
	@Override
	public String getResilienceRegistryKey() {
		return RESILIENCE_KEY;
	}

}
