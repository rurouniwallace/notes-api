package com.gitlab.rurouniwallace.notes.exceptions;

@SuppressWarnings("serial")
public class UnexpectedStateException extends Exception {

	/**
	 * Construct a new instance
	 */
	public UnexpectedStateException() {
		super();
	}

	/**
	 * Construct a new instance
	 * 
	 * @param message error message
	 */
	public UnexpectedStateException(final String message) {
		super(message);
	}

	/**
	 * Construct a new instance
	 * 
	 * @param message error message
	 * @param cause exception that caused this one
	 */
	public UnexpectedStateException(final String message, final Exception cause) {
		super(message, cause);
	}
}
