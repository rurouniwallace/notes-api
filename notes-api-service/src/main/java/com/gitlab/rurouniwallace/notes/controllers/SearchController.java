package com.gitlab.rurouniwallace.notes.controllers;

import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import javax.ws.rs.container.AsyncResponse;

import com.gitlab.rurouniwallace.notes.dao.IIndexesEntities;
import com.gitlab.rurouniwallace.notes.exceptions.DataAccessException;
import com.gitlab.rurouniwallace.notes.exceptions.EntityNotFoundException;
import com.gitlab.rurouniwallace.notes.models.Note;
import com.gitlab.rurouniwallace.notes.resilience.ResilienceRegistry;
import com.gitlab.rurouniwallace.notes.responses.NotesListResponse;
import com.gitlab.rurouniwallace.notes.responses.StatusCode;

import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryConfig;
import io.github.resilience4j.retry.RetryRegistry;

public class SearchController extends BaseController {

	private final IIndexesEntities dao;

	public SearchController(final IIndexesEntities dao, final ResilienceRegistry resilienceRegistry) {
		super(resilienceRegistry);
		this.dao = dao;
	}

	public CompletableFuture<Void> indexNote(final UUID userId, final Note note) {
		return runAsync(dao.getResilienceRegistryKey(), () -> {
			try {
				dao.indexNote(userId, note);
				LOGGER.info("Successfully indexed note " + note.getUuid());
			} catch (DataAccessException e) {
				LOGGER.error("Failed to insert note into search index", e);
			}

			// here for compiler
			return null;
		});
	}

	public CompletableFuture<Void> indexNote(final UUID userId, final Note note, final int numTries) {
		// Updating could fail if update is called before the worker thread that
		// inserted it hasn't finished yet. In those cases, an EntityNotFoundException
		// will be thrown by the DAO. This is rare and has only been observed during
		// load tests, but worth accounting for in order to ensure maximum resiliency
		final RetryConfig retryConfig = RetryConfig.custom().maxAttempts(numTries).waitDuration(Duration.ofMillis(300))
				.retryExceptions(EntityNotFoundException.class).build();

		final Retry retry = RetryRegistry.of(retryConfig).retry(dao.getResilienceRegistryKey());

		return runAsync(dao.getResilienceRegistryKey(), Optional.of(retry), () -> {
			try {
				dao.indexNote(userId, note);
				LOGGER.info("Successfully indexed note " + note.getUuid());
			} catch (DataAccessException e) {
				LOGGER.error("Failed to insert note into search index", e);
			}

			// here for compiler
			return null;
		});
	}

	public CompletableFuture<Void> updateIndexedNote(final UUID userId, final UUID noteId, final Note updatePayload) {
		return updateIndexedNote(userId, noteId, updatePayload, 3);
	}

	public CompletableFuture<Void> updateIndexedNote(final UUID userId, final UUID noteId, final Note updatePayload,
			final int numTries) {

		// Updating could fail if update is called before the worker thread that
		// inserted it hasn't finished yet. In those cases, an EntityNotFoundException
		// will be thrown by the DAO. This is rare and has only been observed during
		// load tests, but worth accounting for in order to ensure maximum resiliency
		final RetryConfig retryConfig = RetryConfig.custom().maxAttempts(numTries).waitDuration(Duration.ofMillis(300))
				.retryExceptions(DataAccessException.class).build();

		final Retry retry = RetryRegistry.of(retryConfig).retry(dao.getResilienceRegistryKey());

		return runAsync(dao.getResilienceRegistryKey(), Optional.of(retry), () -> {
			dao.updateIndexedNote(userId, noteId, updatePayload);
			LOGGER.info("Successfully re-indexed note " + noteId);

			// here for compiler
			return null;
		});
	}

	public CompletableFuture<Void> deleteIndexedNote(final UUID userId, final UUID noteId) {
		return runAsync(dao.getResilienceRegistryKey(), () -> {
			try {
				dao.deleteIndexedNote(userId, noteId);
				LOGGER.info("Successfully deleted note from search index " + noteId);
			} catch (DataAccessException e) {
				LOGGER.error("Failed to delete note from search index", e);
			}

			// here for compiler
			return null;
		});
	}

	public CompletableFuture<Void> searchNotes(final UUID userId, final Set<String> terms, final int numResults,
			final AsyncResponse response) {
		return respondAsync(dao.getResilienceRegistryKey(), response, () -> {
			final List<Note> results = dao.searchNotes(userId, terms, numResults);

			return new NotesListResponse(results, StatusCode.SUCCESS);
		});
	}
}
