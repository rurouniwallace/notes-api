package com.gitlab.rurouniwallace.notes.exceptions;

/**
 * A requested entity was not found
 */
@SuppressWarnings("serial")
public class EntityNotFoundException extends DataAccessException {

	/**
	 * Construct a new instance
	 */
	public EntityNotFoundException() {
		super();
	}

	/**
	 * Construct a new instance
	 */
	public EntityNotFoundException(final String message) {
		super(message);
	}

	/**
	 * Construct a new instance
	 */
	public EntityNotFoundException(final String message, final Exception cause) {
		super(message, cause);
	}
}
