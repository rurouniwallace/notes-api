package com.gitlab.rurouniwallace.notes.dao;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import com.gitlab.rurouniwallace.notes.exceptions.DataAccessException;
import com.gitlab.rurouniwallace.notes.models.Note;
import com.gitlab.rurouniwallace.notes.resilience.IResilient;

/**
 * An interface for writing entities to a search index
 */
public interface IIndexesEntities extends IResilient {

	/**
	 * Insert a note into the search index
	 * 
	 * @param userId ID of the user whose notes are being indexed
	 * @param note the note to index
	 * @throws DataAccessException if an error occurred indexing the note
	 */
	public void indexNote(final UUID userId, final Note note) throws DataAccessException;

	/**
	 * Update a note in the search index
	 * 
	 * @param userId ID of the user whose note is being updated
	 * @param noteId the ID of the note to index
	 * @param updatePayload the update to make to the indexed entry
	 * @throws DataAccessException if an error occurred updating the indexed entry
	 */
	public void updateIndexedNote(final UUID userId, final UUID noteId, final Note updatePayload)
			throws DataAccessException;

	/**
	 * Delete a note from the search index
	 * 
	 * @param userId ID of the user whose note is being deleted
	 * @param noteId ID of the note being deleted
	 * @throws DataAccessException if an error occurred deleting the indexed entry
	 */
	public void deleteIndexedNote(final UUID userId, final UUID noteId) throws DataAccessException;

	/**
	 * Search the index for notes based on a set of keywords
	 * 
	 * @param userId iD of the user whose notes are being searched
	 * @param terms search terms to query by
	 * @param numResults number of results to return
	 * @return matching notes found
	 * @throws DataAccessException if an error occurred searching the index
	 */
	public List<Note> searchNotes(final UUID userId, final Set<String> terms, final int numResults)
			throws DataAccessException;
}
