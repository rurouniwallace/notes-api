package com.gitlab.rurouniwallace.notes.dao.encryptedsqldao;

import java.security.GeneralSecurityException;
import java.security.Security;
import java.security.spec.KeySpec;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.sql.DataSource;

import org.apache.commons.dbutils.DbUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.rurouniwallace.notes.config.SecurityConfiguration;
import com.gitlab.rurouniwallace.notes.dao.IHandlesSqlErrors;
import com.gitlab.rurouniwallace.notes.dao.SqlDao;
import com.gitlab.rurouniwallace.notes.exceptions.AuthenticationException;
import com.gitlab.rurouniwallace.notes.exceptions.DataAccessException;
import com.gitlab.rurouniwallace.notes.exceptions.EntityAlreadyExistsException;
import com.gitlab.rurouniwallace.notes.models.User;

public class EncryptedSqlDao extends SqlDao {

	private static final String RESILIENCE_KEY = "sql";

	/**
	 * Encryption algorithm to use for encrypting user data in database
	 */
	protected static final String ENCRYPTION_ALGORITHM = "AES";

	protected static final String SECRET_KEY_ALGORITHM = "PBKDF2WithHmacSHA256";

	private static final String INSERT_USER_CREDENTIALS_STATEMENT = "INSERT INTO UserCredentials(uuid, email, password) VALUES (?, ?, ?)";

	private static final String INSERT_ENCRYPTED_USER_DATA_STATEMENT = "INSERT INTO EncryptedData(userId, salt, data) VALUES (?, ?, ?)";

	/**
	 * Query for retrieving user credentials from the database by email
	 */
	private static final String LOOKUP_USER_CREDENTIALS_BY_EMAIL_STATEMENT = "SELECT * FROM UserCredentials WHERE email = ?";

	/**
	 * Query for retrieving encrypted user data from the database by user ID
	 */
	private static final String LOOKUP_USER_ENCRYPTED_DATA_BY_ID_STATEMENT = "SELECT * FROM EncryptedData WHERE userId = ?";

	/**
	 * Construct a new instance
	 * 
	 * @param datasource datasource for querying user data
	 * @param securityConfiguration security configs
	 */
	public EncryptedSqlDao(final DataSource datasource, final SecurityConfiguration securityConfiguration) {
		super(datasource, securityConfiguration);
	}

	/**
	 * Authenticate a user with their email and password
	 * 
	 * @param email user email address
	 * @param password user password
	 * @return the user data
	 * @throws DataAccessException accessing data failed
	 * @throws AuthenticationException authentication failed
	 */
	@Override
	public User authenticateUser(final String email, final String password)
			throws DataAccessException, AuthenticationException {
		return runPasswordCheckQuery(email, password, LOOKUP_USER_CREDENTIALS_BY_EMAIL_STATEMENT);
	}

	/**
	 * Submit a user to the database to be registered
	 * 
	 * @param user the user to register
	 * @return the registered user payload
	 * @throws DataAccessException if adding the user to the database fails
	 */
	@Override
	public User registerUser(final User user) throws DataAccessException {

		final Connection connection = buildConnection();

		user.setEmailVerified(false);

		try {
			final UserData userData = new UserData(user);
			userData.setFolders(new HashMap<>());

			final ObjectMapper jsonEncoder = new ObjectMapper();
			final String jsonEncodedUserData = jsonEncoder.writeValueAsString(userData);
			final String base64EncodedData = Base64.getEncoder().encodeToString(jsonEncodedUserData.getBytes());

			final String encryptedData = aesEncryptString(base64EncodedData, user.getEncryptionKey(),
					user.getEncryptionSalt());

			runUpdate(connection, INSERT_USER_CREDENTIALS_STATEMENT,
					Arrays.asList(user.getUuid(), user.getEmail(), user.getPassword()), new IHandlesSqlErrors() {

						/**
						 * Check for the SQL state to see if a uniqueness constraint was violated
						 * 
						 * @param e the exception to handle
						 */
						@Override
						public void handleError(final SQLException e) throws DataAccessException {
							if (e.getSQLState().equals("23505")) {
								DbUtils.closeQuietly(connection);
								throw new EntityAlreadyExistsException(
										"A user with the provided user ID already exists", e);
							}

						}
					});

			runUpdate(connection, INSERT_ENCRYPTED_USER_DATA_STATEMENT,
					Arrays.asList(user.getUuid(), user.getEncryptionSalt(), encryptedData), new IHandlesSqlErrors() {

						/**
						 * Check for the SQL state to see if a uniqueness constraint was violated
						 * 
						 * @param e the exception to handle
						 */
						@Override
						public void handleError(final SQLException e) throws DataAccessException {
							if (e.getSQLState().equals("23505")) {
								DbUtils.closeQuietly(connection);
								throw new EntityAlreadyExistsException(
										"A user with the provided user ID already exists", e);
							}

						}
					});
		} catch (JsonProcessingException e) {
			throw new DataAccessException("Failed to process JSON while encrypting user data", e);
		} catch (GeneralSecurityException e) {
			throw new DataAccessException("Failed to encrypt user data", e);
		} finally {
			DbUtils.closeQuietly(connection);
		}

		final User createdUser = new User(user);

		// for security purposes, don't return the user's password and otp secret
		createdUser.setPassword(null);
		createdUser.setOtpSecret(null);

		LoggerFactory.getLogger(EncryptedSqlDao.class).info("Encrypted data inserted");

		return createdUser;
	}

	private String aesEncryptString(final String dataToEncrypt, final String encryptionKey, final String salt)
			throws GeneralSecurityException {
		byte[] dataInBytes = dataToEncrypt.getBytes();

		Security.addProvider(new BouncyCastleProvider());
		final Cipher encryptionCipher = Cipher.getInstance("AES/GCM/NoPadding", "BC");

		final SecretKeySpec keySpec = buildSecretKeyFromPassword(encryptionKey, salt);

		final IvParameterSpec iv = new IvParameterSpec(securityConfiguration.getInitializationVector().getBytes());

		encryptionCipher.init(Cipher.ENCRYPT_MODE, keySpec, iv);
		final byte[] encryptedBytes = encryptionCipher.doFinal(dataInBytes);
		return Base64.getEncoder().encodeToString(encryptedBytes);

	}

	private SecretKeySpec buildSecretKeyFromPassword(final String password, final String salt)
			throws GeneralSecurityException {
		final KeySpec spec = new PBEKeySpec(password.toCharArray(), salt.getBytes(), 65536, 256); // AES-256
		final SecretKeyFactory f = SecretKeyFactory.getInstance(SECRET_KEY_ALGORITHM);
		byte[] key = f.generateSecret(spec).getEncoded();
		return new SecretKeySpec(key, ENCRYPTION_ALGORITHM);
	}
}
