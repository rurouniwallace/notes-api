package com.gitlab.rurouniwallace.notes.controllers;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.Cookie;

import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gitlab.rurouniwallace.notes.config.SessionConfiguration;
import com.gitlab.rurouniwallace.notes.dao.IAccessesNotes;
import com.gitlab.rurouniwallace.notes.exceptions.EntityNotFoundException;
import com.gitlab.rurouniwallace.notes.models.Folder;
import com.gitlab.rurouniwallace.notes.resilience.ResilienceRegistry;
import com.gitlab.rurouniwallace.notes.responses.FolderResponse;
import com.gitlab.rurouniwallace.notes.responses.FoldersListResponse;
import com.gitlab.rurouniwallace.notes.responses.StatusCode;

public class FolderController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(FolderController.class);

	private final IAccessesNotes dao;

	private final SessionConfiguration sessionConfig;

	public FolderController(final IAccessesNotes dao, final SessionConfiguration sessionConfig,
			final ResilienceRegistry resilienceRegistry) {
		super(resilienceRegistry);
		this.dao = dao;
		this.sessionConfig = sessionConfig;
	}

	public CompletableFuture<Void> createFolder(final UUID userId, final Folder folder, final Cookie sessionCookie,
			final AsyncResponse response) {

		// checkAuth(sessionCookie, sessionConfig, userId, response);

		if (folder.getUuid() != null) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"UUID may not appear in request body"));
		}

		if (folder.getUser() != null) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"User ID may not appear in request body"));
		}

		final String resilienceKey = dao.getResilienceRegistryKey();

		return respondAsync(resilienceKey, response, () -> {
			Folder createdFolder;

			try {
				createdFolder = dao.createFolder(userId, folder);
			} catch (final EntityNotFoundException e) {
				return new FolderResponse("User with ID " + userId + " not found", StatusCode.ENTITY_NOT_FOUND,
						HttpStatus.NOT_FOUND_404);
			}

			return new FolderResponse(createdFolder, StatusCode.CREATED, HttpStatus.CREATED_201);
		});
	}

	public CompletableFuture<Void> updateFolder(final UUID userId, final UUID folderId, final Folder updatePayload,
			final Cookie sessionCookie, final AsyncResponse response) {
		// checkAuth(sessionCookie, sessionConfig, userId, response);

		if (updatePayload.getUuid() != null) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"UUID may not be updated"));
		}

		if (updatePayload.getUser() != null) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"User ID may not be updated"));
		}

		final String resilienceKey = dao.getResilienceRegistryKey();

		return respondAsync(resilienceKey, response, () -> {
			Folder folder;
			try {
				folder = dao.updateFolder(userId, folderId, updatePayload);
			} catch (final EntityNotFoundException e) {
				return new FolderResponse(StatusCode.ENTITY_NOT_FOUND, HttpStatus.NOT_FOUND_404);
			}

			return new FolderResponse(folder, StatusCode.SUCCESS);
		});
	}

	public CompletableFuture<Void> getFoldersForUser(final UUID userId, final Cookie sessionCookie,
			final AsyncResponse response) {
		// checkAuth(sessionCookie, sessionConfig, userId, response);

		final String resilienceKey = dao.getResilienceRegistryKey();

		return respondAsync(resilienceKey, response, () -> {
			List<Folder> folders;
			try {
				folders = dao.getFoldersForUser(userId);
			} catch (final EntityNotFoundException e) {
				return new FoldersListResponse("User with ID " + userId + " not found", StatusCode.ENTITY_NOT_FOUND,
						HttpStatus.NOT_FOUND_404);
			}

			return new FoldersListResponse(folders, StatusCode.SUCCESS);
		});
	}
}
