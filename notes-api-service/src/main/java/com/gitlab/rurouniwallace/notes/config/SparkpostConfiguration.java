package com.gitlab.rurouniwallace.notes.config;

/**
 * Sparkpost API configurations
 */
public class SparkpostConfiguration {

	/**
	 * Authorization API key
	 */
	private String apikey;

	/**
	 * Sparkpost hostname
	 */
	private String hostname;

	/**
	 * Email verification Sparkpost template
	 */
	private String emailVerificationTemplate;

	public SparkpostConfiguration() {
		// empty constructor
	}

	/**
	 * Construct a new instance
	 * 
	 * @param apiKey authorization API key
	 * @param hostname sparkpost hostname
	 * @param emailVerificationTemplate email verification Sparkpost template
	 */
	public SparkpostConfiguration(final String apikey, final String hostname, final String emailVerificationTemplate) {
		this.apikey = apikey;
		this.hostname = hostname;
		this.emailVerificationTemplate = emailVerificationTemplate;
	}

	/**
	 * @return the apiKey
	 */
	public String getApikey() {
		return apikey;
	}

	/**
	 * @param apiKey the apiKey to set
	 */
	public void setApikey(String apiKey) {
		this.apikey = apiKey;
	}

	/**
	 * @return the emailVerificationTemplate
	 */
	public String getEmailVerificationTemplate() {
		return emailVerificationTemplate;
	}

	/**
	 * @param emailVerificationTemplate the emailVerificationTemplate to set
	 */
	public void setEmailVerificationTemplate(String emailVerificationTemplate) {
		this.emailVerificationTemplate = emailVerificationTemplate;
	}

	/**
	 * @return the hostname
	 */
	public String getHostname() {
		return hostname;
	}

	/**
	 * @param hostname the hostname to set
	 */
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
}
