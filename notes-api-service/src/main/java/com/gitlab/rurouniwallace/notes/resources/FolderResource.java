package com.gitlab.rurouniwallace.notes.resources;

import java.util.UUID;

import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;

import com.gitlab.rurouniwallace.notes.controllers.FolderController;
import com.gitlab.rurouniwallace.notes.models.Folder;
import com.gitlab.rurouniwallace.notes.responses.FolderResponse;
import com.gitlab.rurouniwallace.notes.responses.FoldersListResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Folder REST resource
 */
@Api("/folders")
@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
public class FolderResource {

	/**
	 * Logic controller
	 */
	private final FolderController controller;

	/**
	 * Construct a new instance
	 * 
	 * @param controller logic controller
	 */
	public FolderResource(final FolderController controller) {
		this.controller = controller;
	}

	/**
	 * Create a new folder
	 * 
	 * @param userId the user identifier
	 * @param folder folder to create
	 * @param response the HTTP response
	 */
	@POST
	@Path("/{userId}/folders")
	@ApiOperation(value = "Create a folder for a user", response = FolderResponse.class)
	public void createFolder(@CookieParam("session") final Cookie sessionCookie, @PathParam("userId") final UUID userId,
			final Folder folder, @Suspended final AsyncResponse response) {
		controller.createFolder(userId, folder, sessionCookie, response);
	}

	/**
	 * Get all folders for a user
	 * 
	 * @param userId the user identifier
	 * @param response the HTTP response
	 */
	@GET
	@Path("/{userId}/folders")
	@ApiOperation(value = "Get all folders for a user", response = FoldersListResponse.class)
	public void getFoldersForUser(@CookieParam("session") final Cookie sessionCookie,
			@PathParam("userId") final UUID userId, @Suspended final AsyncResponse response) {
		controller.getFoldersForUser(userId, sessionCookie, response);
	}

	/**
	 * Update an existing folder
	 * 
	 * @param userId the user identifier
	 * @param folderId the ID of the folder to update
	 * @param updatePayload the updates to make to the folder
	 * @param response the HTTP response
	 */
	@PUT
	@Path("/{userId}/folders/{folderId}")
	public void updateFolder(@CookieParam("session") final Cookie sessionCookie, @PathParam("userId") final UUID userId,
			@PathParam("folderId") final UUID folderId, final Folder updatePayload,
			@Suspended final AsyncResponse response) {
		controller.updateFolder(userId, folderId, updatePayload, sessionCookie, response);
	}
}
