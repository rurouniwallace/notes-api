package com.gitlab.rurouniwallace.notes.email.sparkpost;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TransmissionContent {

	@JsonProperty("template_id")
	private String templateId;

	/**
	 * @return the templateId
	 */
	public String getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateId the templateId to set
	 */
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}
}
