package com.gitlab.rurouniwallace.notes.resources;

import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;

import com.gitlab.rurouniwallace.notes.controllers.AuthController;
import com.gitlab.rurouniwallace.notes.requests.AuthenticationRequest;
import com.gitlab.rurouniwallace.notes.requests.EmailVerificationRequest;
import com.gitlab.rurouniwallace.notes.requests.VerifyOtpRequest;
import com.gitlab.rurouniwallace.notes.responses.StandardResponse;
import com.gitlab.rurouniwallace.notes.responses.UserResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api("/auth")
@Path("/auth")
@Produces(MediaType.APPLICATION_JSON)
public class AuthResource {

	/**
	 * Logic controller
	 */
	private final AuthController controller;

	/**
	 * Construct a new instance
	 * 
	 * @param controller logic controller
	 */
	public AuthResource(final AuthController controller) {
		this.controller = controller;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Authenticate a user", response = UserResponse.class)
	public void authenticateUser(final AuthenticationRequest authnRequest, @Suspended final AsyncResponse response) {
		controller.authenticateUser(authnRequest, response);
	}

	@POST
	@Path("/verify/email")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Verify email address", response = StandardResponse.class)
	public void emailVerification(final EmailVerificationRequest request, @Suspended final AsyncResponse response) {
		controller.verifyEmail(request, response);
	}

	@POST
	@Path("/{userId}/tfa/sms/send")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Send a two-factor auth verification email", response = StandardResponse.class)
	public void emailVerification(@CookieParam("session") final Cookie sessionCookie,
			@PathParam("userId") final UUID userId, @Suspended final AsyncResponse response) {
		controller.sendSmsOtpVerification(userId, sessionCookie, response);
	}

	@POST
	@Path("/verify/otp")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Verify OTP code", response = UserResponse.class)
	public void verifyOtp(@CookieParam("session") final Cookie sessionCookie, final VerifyOtpRequest request,
			@Suspended final AsyncResponse response) {
		controller.verifyOtp(request, sessionCookie, response);
	}
}
