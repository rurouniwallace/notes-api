package com.gitlab.rurouniwallace.notes;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import javax.sql.DataSource;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.apache.commons.dbutils.DbUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.eclipse.jetty.servlets.CrossOriginFilter;

import com.gitlab.rurouniwallace.notes.config.HIBPConfiguration;
import com.gitlab.rurouniwallace.notes.config.NotesApiConfiguration;
import com.gitlab.rurouniwallace.notes.config.SqlFactory;
import com.gitlab.rurouniwallace.notes.config.YamlFileConfigurationSourceProvider;
import com.gitlab.rurouniwallace.notes.controllers.AuthController;
import com.gitlab.rurouniwallace.notes.controllers.EmailController;
import com.gitlab.rurouniwallace.notes.controllers.FolderController;
import com.gitlab.rurouniwallace.notes.controllers.HealthController;
import com.gitlab.rurouniwallace.notes.controllers.NoteController;
import com.gitlab.rurouniwallace.notes.controllers.SearchController;
import com.gitlab.rurouniwallace.notes.controllers.UserController;
import com.gitlab.rurouniwallace.notes.dao.IAccessesNotes;
import com.gitlab.rurouniwallace.notes.dao.IAccessesUsers;
import com.gitlab.rurouniwallace.notes.dao.IIndexesEntities;
import com.gitlab.rurouniwallace.notes.dao.LuceneDao;
import com.gitlab.rurouniwallace.notes.dao.SqlDao;
import com.gitlab.rurouniwallace.notes.email.ISendsEmails;
import com.gitlab.rurouniwallace.notes.email.SparkpostEmailer;
import com.gitlab.rurouniwallace.notes.exceptions.DataAccessException;
import com.gitlab.rurouniwallace.notes.passwords.verification.HIBPConnector;
import com.gitlab.rurouniwallace.notes.passwords.verification.IChecksPasswordCompromised;
import com.gitlab.rurouniwallace.notes.resilience.ResilienceRegistry;
import com.gitlab.rurouniwallace.notes.resources.AuthResource;
import com.gitlab.rurouniwallace.notes.resources.FolderResource;
import com.gitlab.rurouniwallace.notes.resources.HealthResource;
import com.gitlab.rurouniwallace.notes.resources.NoteResource;
import com.gitlab.rurouniwallace.notes.resources.UserResource;
import com.gitlab.rurouniwallace.notes.sms.ISendsSms;
import com.gitlab.rurouniwallace.notes.sms.NexmoMessenger;

import io.dropwizard.Application;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;

/**
 * 
 *
 */
public class NotesApiApplication extends Application<NotesApiConfiguration> {

	public static void main(final String[] args) throws Exception {
		new NotesApiApplication().run(args);
	}

	@Override
	public String getName() {
		return "NotesApi";
	}

	/**
	 * Initialize the service bootstrap
	 * 
	 * @param bootstrap service bootstrapping
	 */
	@Override
	public void initialize(final Bootstrap<NotesApiConfiguration> bootstrap) {
		bootstrap.setConfigurationSourceProvider(new SubstitutingSourceProvider(
				new YamlFileConfigurationSourceProvider(), new EnvironmentVariableSubstitutor(false)));

		bootstrap.addBundle(new SwaggerBundle<NotesApiConfiguration>() {

			@Override
			protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(
					final NotesApiConfiguration configuration) {
				return configuration.getSwagger();
			}
		});
	}

	/**
	 * Run the application
	 * 
	 * @param configuration application configurations
	 * @param environment application environment
	 * @throws DataAccessException
	 * @throws IOException
	 */
	@SuppressWarnings("deprecation")
	@Override
	public void run(final NotesApiConfiguration configuration, final Environment environment)
			throws SQLException, LiquibaseException, DataAccessException, IOException {

		// TODO Adding this to trigger a deployment. Remove at a later time.

		// Enable CORS headers
		final FilterRegistration.Dynamic cors = environment.servlets().addFilter("CORS", CrossOriginFilter.class);

		// Configure CORS parameters
		cors.setInitParameter("allowedOrigins", "*");
		cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin");
		cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");

		// Add URL mapping
		cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");

		final SqlFactory sqlFactory = configuration.getSql();

		final DataSource sqlDataSource = sqlFactory.buildDataSource();

		// since right now we're using an in-memory database, we'll use Liquibase to
		// bootstrap the database at runtime
		initLiquibase(sqlDataSource);

		final SqlDao sqlDao = new SqlDao(sqlDataSource, configuration.getSecurity());

		final ResilienceRegistry resilienceRegistry = configuration.buildResilienceRegistry();

		final HealthController healthController = new HealthController(environment, resilienceRegistry);

		final HealthResource healthResource = new HealthResource(healthController);

		final ISendsSms smsMessenger = new NexmoMessenger(configuration.getNexmo());

		final IAccessesUsers userDao = sqlDao;
		final AuthController authController = new AuthController(userDao, smsMessenger, configuration.getSession(),
				configuration.getEmailVerification(), resilienceRegistry);
		final AuthResource authResource = new AuthResource(authController);

		final Client httpClient = ClientBuilder.newClient();

		final ISendsEmails emailer = new SparkpostEmailer(configuration.getSparkpost(), httpClient);
		final EmailController emailController = new EmailController(emailer, configuration.getEmailVerification(),
				resilienceRegistry);

		final HIBPConfiguration hibpConfig = configuration.getHibp();

		final IChecksPasswordCompromised passwordChecker = new HIBPConnector(hibpConfig, httpClient);

		final UserController userController = new UserController(userDao, passwordChecker, emailController,
				resilienceRegistry);
		final UserResource userResource = new UserResource(userController);

		final Directory searchIndex = new RAMDirectory();
		final Analyzer searchAnalyzer = new StandardAnalyzer();

		final IndexWriterConfig config = new IndexWriterConfig(searchAnalyzer);
		final IndexWriter searchIndexWriter = new IndexWriter(searchIndex, config);

		final IIndexesEntities searchDao = new LuceneDao(searchIndex, searchAnalyzer, searchIndexWriter);
		final SearchController searchController = new SearchController(searchDao, resilienceRegistry);

		final IAccessesNotes noteDao = sqlDao;
		final NoteController noteController = new NoteController(noteDao, searchController, configuration.getSession(),
				resilienceRegistry);
		final NoteResource noteResource = new NoteResource(noteController);

		final FolderController folderController = new FolderController(noteDao, configuration.getSession(),
				resilienceRegistry);
		final FolderResource folderResource = new FolderResource(folderController);

		environment.jersey().register(userResource);
		environment.jersey().register(noteResource);
		environment.jersey().register(folderResource);
		environment.jersey().register(authResource);
		environment.jersey().register(healthResource);
	}

	/**
	 * Bootstrap the database via the Liquibase changelog
	 * 
	 * @param sqlDataSource SQL connection datasource
	 * @throws SQLException database error occurred
	 * @throws LiquibaseException liquibase error occurred
	 */
	private void initLiquibase(final DataSource sqlDataSource) throws SQLException, LiquibaseException {
		final Connection connection = sqlDataSource.getConnection();

		final Database database = DatabaseFactory.getInstance()
				.findCorrectDatabaseImplementation(new JdbcConnection(connection));

		final Liquibase liquibase = new Liquibase("liquibase/changelog.xml", new ClassLoaderResourceAccessor(),
				database);
		liquibase.update(new Contexts(), new LabelExpression());
		liquibase.close();

		DbUtils.closeQuietly(connection);
	}
}
