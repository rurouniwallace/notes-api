package com.gitlab.rurouniwallace.notes.config;

import java.util.List;

public class RetryConfiguration {

	private Integer maxAttempts;

	private Integer waitDuration;

	private Integer intervalFunction;

	private String intervalBiFunction;

	private String retryOnResultHandler;

	private String retryOnExceptionHandler;

	private List<String> retryExceptions;

	private List<String> ignoreExceptions;

	private Boolean failAfterMaxRetries;

	/**
	 * @return the maxAttempts
	 */
	public Integer getMaxAttempts() {
		return maxAttempts;
	}

	/**
	 * @param maxAttempts the maxAttempts to set
	 */
	public void setMaxAttempts(Integer maxAttempts) {
		this.maxAttempts = maxAttempts;
	}

	/**
	 * @return the waitDuration
	 */
	public Integer getWaitDuration() {
		return waitDuration;
	}

	/**
	 * @param waitDuration the waitDuration to set
	 */
	public void setWaitDuration(Integer waitDuration) {
		this.waitDuration = waitDuration;
	}

	/**
	 * @return the intervalFunction
	 */
	public Integer getIntervalFunction() {
		return intervalFunction;
	}

	/**
	 * @param intervalFunction the intervalFunction to set
	 */
	public void setIntervalFunction(Integer intervalFunction) {
		this.intervalFunction = intervalFunction;
	}

	/**
	 * @return the retryOnResultHandler
	 */
	public String getRetryOnResultHandler() {
		return retryOnResultHandler;
	}

	/**
	 * @param retryOnResultHandler the retryOnResultHandler to set
	 */
	public void setRetryOnResultHandler(String retryOnResultHandler) {
		this.retryOnResultHandler = retryOnResultHandler;
	}

	/**
	 * @return the retryOnExceptionHandler
	 */
	public String getRetryOnExceptionHandler() {
		return retryOnExceptionHandler;
	}

	/**
	 * @param retryOnExceptionHandler the retryOnExceptionHandler to set
	 */
	public void setRetryOnExceptionHandler(String retryOnExceptionHandler) {
		this.retryOnExceptionHandler = retryOnExceptionHandler;
	}

	/**
	 * @return the retryExceptions
	 */
	public List<String> getRetryExceptions() {
		return retryExceptions;
	}

	/**
	 * @param retryExceptions the retryExceptions to set
	 */
	public void setRetryExceptions(List<String> retryExceptions) {
		this.retryExceptions = retryExceptions;
	}

	/**
	 * @return the ignoreExceptions
	 */
	public List<String> getIgnoreExceptions() {
		return ignoreExceptions;
	}

	/**
	 * @param ignoreExceptions the ignoreExceptions to set
	 */
	public void setIgnoreExceptions(List<String> ignoreExceptions) {
		this.ignoreExceptions = ignoreExceptions;
	}

	/**
	 * @return the failAfterMaxRetries
	 */
	public Boolean getFailAfterMaxRetries() {
		return failAfterMaxRetries;
	}

	/**
	 * @param failAfterMaxRetries the failAfterMaxRetries to set
	 */
	public void setFailAfterMaxRetries(Boolean failAfterMaxRetries) {
		this.failAfterMaxRetries = failAfterMaxRetries;
	}

	/**
	 * @return the intervalBiFunction
	 */
	public String getIntervalBiFunction() {
		return intervalBiFunction;
	}

	/**
	 * @param intervalBiFunction the intervalBiFunction to set
	 */
	public void setIntervalBiFunction(String intervalBiFunction) {
		this.intervalBiFunction = intervalBiFunction;
	}
}
