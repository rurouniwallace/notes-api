package com.gitlab.rurouniwallace.notes.email;

import com.gitlab.rurouniwallace.notes.exceptions.EmailTransmissionException;
import com.gitlab.rurouniwallace.notes.resilience.IResilient;

/**
 * Interface for an emailer.
 */
public interface ISendsEmails extends IResilient {

	/**
	 * Sends a verification email.
	 * 
	 * @param verificationUrl
	 * @throws EmailTransmissionException
	 */
	public void sendVerificationEmail(final String emailAddress, final String verificationUrl)
			throws EmailTransmissionException;
}
