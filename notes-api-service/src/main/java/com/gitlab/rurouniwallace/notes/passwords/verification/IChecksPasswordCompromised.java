package com.gitlab.rurouniwallace.notes.passwords.verification;

import com.gitlab.rurouniwallace.notes.resilience.IResilient;

public interface IChecksPasswordCompromised extends IResilient {

	/**
	 * Checks to determine if a given password has been previously leaked or
	 * compromised.
	 * 
	 * @param password the password to check
	 * @return number of sites this password has been compromised on
	 * @throws PasswordVerificationException connecting to password verification
	 * service failed
	 */
	public int isCompromised(final String password) throws PasswordVerificationException;
}
