package com.gitlab.rurouniwallace.notes.config;

public class HIBPConfiguration {

	private String baseUrl;

	private String apikey;

	public HIBPConfiguration() {
	}

	public HIBPConfiguration(final String baseUrl, final String apikey) {
		this.baseUrl = baseUrl;
		this.apikey = apikey;
	}

	/**
	 * @return the baseUrl
	 */
	public String getBaseUrl() {
		return baseUrl;
	}

	/**
	 * @param baseUrl the baseUrl to set
	 */
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	/**
	 * @return the apikey
	 */
	public String getApikey() {
		return apikey;
	}

	/**
	 * @param apikey the apikey to set
	 */
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
}
