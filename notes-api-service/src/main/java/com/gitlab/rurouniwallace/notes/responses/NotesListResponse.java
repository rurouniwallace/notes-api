package com.gitlab.rurouniwallace.notes.responses;

import java.util.List;

import com.gitlab.rurouniwallace.notes.models.Note;

/**
 * List of notes
 */
public class NotesListResponse extends StandardResponse {

	/**
	 * List of notes from the API operation
	 */
	private List<Note> notes;

	/**
	 * Construct a new instance
	 */
	public NotesListResponse() {
		// empty constructor
	}

	/**
	 * Construct a new instance
	 * 
	 * @param message the response message
	 * @param status the response status
	 */
	public NotesListResponse(final String message, final StatusCode status) {
		this.message = message;
		this.status = status;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param status the response status
	 */
	public NotesListResponse(final StatusCode status) {
		this.status = status;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param status the response status
	 * @param httpStatus HTTP status
	 */
	public NotesListResponse(final StatusCode status, final int httpStatus) {
		this.status = status;
		this.httpStatus = httpStatus;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param httpStatus HTTP status
	 * @param message the response message
	 * @param status the response status
	 */
	public NotesListResponse(final String message, final StatusCode status, final int httpStatus) {
		this.httpStatus = httpStatus;
		this.message = message;
		this.status = status;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param note the note for the response
	 * @param status the response status
	 */
	public NotesListResponse(final List<Note> notes, final StatusCode status) {
		this.notes = notes;
		this.status = status;
	}

	/**
	 * @return the notes
	 */
	public List<Note> getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(List<Note> notes) {
		this.notes = notes;
	}
}
