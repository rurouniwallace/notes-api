package com.gitlab.rurouniwallace.notes.resilience;

/**
 * Interface defining a module that implements Resilience4j settings
 */
public interface IResilient {

	/**
	 * Get the resilience registry key for this module
	 * 
	 * @return resilience registry key
	 */
	public String getResilienceRegistryKey();
}
