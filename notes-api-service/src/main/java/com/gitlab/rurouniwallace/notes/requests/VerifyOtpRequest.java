package com.gitlab.rurouniwallace.notes.requests;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VerifyOtpRequest {

	private UUID userId;

	/**
	 * User email address
	 */
	@JsonProperty
	private String code;

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the userId
	 */
	public UUID getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(UUID userId) {
		this.userId = userId;
	}
}
