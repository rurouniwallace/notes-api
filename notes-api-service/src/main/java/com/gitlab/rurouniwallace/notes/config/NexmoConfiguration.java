package com.gitlab.rurouniwallace.notes.config;

public class NexmoConfiguration {

	private String apiKey;

	private String apiSecret;

	private String hostname;

	private String sourcePhoneNumber;

	/**
	 * @return the apiKey
	 */
	public String getApiKey() {
		return apiKey;
	}

	/**
	 * @param apiKey the apiKey to set
	 */
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	/**
	 * @return the apiSecret
	 */
	public String getApiSecret() {
		return apiSecret;
	}

	/**
	 * @param apiSecret the apiSecret to set
	 */
	public void setApiSecret(String apiSecret) {
		this.apiSecret = apiSecret;
	}

	/**
	 * @return the hostname
	 */
	public String getHostname() {
		return hostname;
	}

	/**
	 * @param hostname the hostname to set
	 */
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	/**
	 * @return the sourcePhoneNumber
	 */
	public String getSourcePhoneNumber() {
		return sourcePhoneNumber;
	}

	/**
	 * @param sourcePhoneNumber the sourcePhoneNumber to set
	 */
	public void setSourcePhoneNumber(String sourcePhoneNumber) {
		this.sourcePhoneNumber = sourcePhoneNumber;
	}
}
