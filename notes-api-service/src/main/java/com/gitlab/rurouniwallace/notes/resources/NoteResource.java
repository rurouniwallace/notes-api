package com.gitlab.rurouniwallace.notes.resources;

import java.util.Optional;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;

import com.gitlab.rurouniwallace.notes.controllers.NoteController;
import com.gitlab.rurouniwallace.notes.models.Note;
import com.gitlab.rurouniwallace.notes.responses.NoteResponse;
import com.gitlab.rurouniwallace.notes.responses.NotesListResponse;
import com.gitlab.rurouniwallace.notes.responses.StandardResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Note REST resource
 */
@Api("/notes")
@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
public class NoteResource {

	/**
	 * Logic controller
	 */
	private final NoteController controller;

	/**
	 * Construct a new instance
	 * 
	 * @param controller logic controller
	 */
	public NoteResource(final NoteController controller) {
		this.controller = controller;
	}

	/**
	 * Get all notes for a user
	 * 
	 * @param userId the user identifier
	 * @param folderId the folder to filter by
	 * @param response the HTTP response
	 */
	@GET
	@Path("/{userId}/notes")
	@ApiOperation(value = "Get all notes for user", response = NotesListResponse.class)
	public void getNotesForUser(@CookieParam("session") final Cookie sessionCookie,
			@PathParam("userId") final UUID userId, @QueryParam("folder") final UUID folderId,
			@Suspended final AsyncResponse response) {
		controller.getNotesForUser(userId, Optional.ofNullable(folderId), sessionCookie, response);
	}

	/**
	 * Delete a note
	 * 
	 * @param userId the user identifier
	 * @param noteId the ID of the note to delete
	 * @param response the HTTP response
	 */
	@DELETE
	@Path("/{userId}/notes/{noteId}")
	@ApiOperation(value = "Delete a note", response = StandardResponse.class)
	public void deleteNote(@CookieParam("session") final Cookie sessionCookie, @PathParam("userId") final UUID userId,
			@PathParam("noteId") final UUID noteId, @Suspended final AsyncResponse response) {
		controller.deleteNote(userId, noteId, sessionCookie, response);
	}

	/**
	 * Update a note
	 * 
	 * @param userId the user identifier
	 * @param noteId the ID of the note to update
	 * @param updatePayload the updates to make to the note
	 * @param response the HTTP response
	 */
	@PUT
	@Path("/{userId}/notes/{noteId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Update a note", response = NoteResponse.class)
	public void updateNote(@CookieParam("session") final Cookie sessionCookie, @PathParam("userId") final UUID userId,
			@PathParam("noteId") final UUID noteId, final Note updatePayload, @Suspended final AsyncResponse response) {
		controller.updateNote(userId, noteId, updatePayload, sessionCookie, response);
	}

	/**
	 * Create a new note
	 * 
	 * @param userId the user identifier
	 * @param noteToCreate the note to create
	 * @param response the HTTP response
	 */
	@POST
	@Path("/{userId}/notes")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Create a note", response = NoteResponse.class)
	public void createNote(@CookieParam("session") final Cookie sessionCookie, @PathParam("userId") final UUID userId,
			final Note noteToCreate, @Suspended final AsyncResponse response) {
		controller.createNote(userId, noteToCreate, sessionCookie, response);
	}

	/**
	 * Search notes
	 * 
	 * @param sessionCookie the session cookie
	 * @param userId user identifier
	 * @param query the search query
	 * @param response the HTTP response
	 */
	@GET
	@Path("/{userId}/notes/search")
	@ApiOperation(value = "Search for a note or notes based on a set of search terms ", response = NotesListResponse.class)
	public void searchNotes(@CookieParam("session") final Cookie sessionCookie, @PathParam("userId") final UUID userId,
			@QueryParam("query") final String query, @QueryParam("numResults") final Integer numResults,
			@Suspended final AsyncResponse response) {
		controller.searchNotes(userId, query, numResults, sessionCookie, response);
	}
}
