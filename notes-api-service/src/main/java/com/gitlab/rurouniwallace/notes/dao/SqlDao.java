package com.gitlab.rurouniwallace.notes.dao;

import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.sql.DataSource;

import org.apache.commons.dbutils.DbUtils;
import org.bouncycastle.crypto.generators.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gitlab.rurouniwallace.notes.config.SecurityConfiguration;
import com.gitlab.rurouniwallace.notes.exceptions.AuthenticationDeniedException;
import com.gitlab.rurouniwallace.notes.exceptions.AuthenticationException;
import com.gitlab.rurouniwallace.notes.exceptions.DataAccessException;
import com.gitlab.rurouniwallace.notes.exceptions.EntityAlreadyExistsException;
import com.gitlab.rurouniwallace.notes.exceptions.EntityNotFoundException;
import com.gitlab.rurouniwallace.notes.models.Folder;
import com.gitlab.rurouniwallace.notes.models.Note;
import com.gitlab.rurouniwallace.notes.models.User;

import dev.samstevens.totp.code.CodeGenerator;
import dev.samstevens.totp.code.DefaultCodeGenerator;
import dev.samstevens.totp.code.DefaultCodeVerifier;
import dev.samstevens.totp.code.HashingAlgorithm;
import dev.samstevens.totp.time.SystemTimeProvider;
import dev.samstevens.totp.time.TimeProvider;

/**
 * Data access layer to communicate with an SQL database
 */
public class SqlDao implements IAccessesUsers, IAccessesNotes {

	private static final String RESILIENCE_KEY = "sql";

	/**
	 * Query for retrieving users from the database by email
	 */
	private static final String LOOKUP_USER_BY_EMAIL_STATEMENT = "SELECT * FROM Users WHERE email = ?";

	/**
	 * Query for retrieving a user from the database by ID
	 */
	private static final String LOOKUP_USER_BY_ID_STATEMENT = "SELECT * FROM Users WHERE uuid = ?";

	/**
	 * Query for updating a user
	 */
	private static final String UPDATE_USER_STATEMENT = "UPDATE Users SET email = COALESCE(?, email), password = COALESCE(?, password), phone = COALESCE(?, phone), givenName = COALESCE(?, givenName), surname = COALESCE(?, surname), emailVerified = COALESCE(?, emailVerified), twoFactorAuthPreference = COALESCE(?, twoFactorAuthPreference), otpSecret = COALESCE(?, otpSecret), otpAlgorithm = COALESCE(?, otpAlgorithm), otpStep = COALESCE(?, otpStep), otpDigits = COALESCE(?, otpDigits) WHERE uuid = ?";

	/**
	 * Query for retrieving a folder by its ID
	 */
	private static final String LOOKUP_FOLDER_BY_ID_STATEMENT = "SELECT * FROM Folders WHERE uuid = ?";

	/**
	 * Query for folders owned by a single user
	 */
	private static final String LOOKUP_FOLDERS_FOR_USER_STATEMENT = "SELECT * FROM Folders WHERE userId = ?";

	/**
	 * Query to update a folder
	 */
	private static final String UPDATE_FOLDER_STATEMENT = "UPDATE Folders SET name = COALESCE(?, name) WHERE uuid = ? AND userId = ?";

	/**
	 * Query for a single note
	 */
	private static final String LOOKUP_NOTE_BY_ID_STATEMENT = "SELECT * FROM Notes WHERE uuid = ?";

	/**
	 * Query for updating a note
	 */
	private static final String UPDATE_NOTE_STATEMENT = "UPDATE Notes SET\n" + "  title = COALESCE(?, title),\n"
			+ "  folderId = COALESCE(?, folderId),\n" + "  body = COALESCE(?, body),\n"
			+ "  created = COALESCE(?, created),\n" + "  lastUpdated = COALESCE(?, lastUpdated)\n"
			+ "WHERE uuid = ? AND userId = ?;";

	/**
	 * Query for inserting users into the database
	 */
	private static final String INSERT_USER_STATEMENT = "INSERT INTO Users(uuid, email, password, phone, givenName, surname, emailVerified, twoFactorAuthPreference, otpSecret, otpAlgorithm, otpStep, otpDigits) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	/**
	 * Query for inserting notes into the database
	 */
	private static final String INSERT_NOTE_STATEMENT = "INSERT INTO Notes(uuid, folderId, userId, title, body, created, lastUpdated) VALUES (?, ?, ?, ?, ?, ? ,?)";

	/**
	 * Query for inserting folders into the database
	 */
	private static final String INSERT_FOLDER_STATEMENT = "INSERT INTO Folders(uuid, userId, name) VALUES (?, ?, ?)";

	/**
	 * Query for notes owned by a single user
	 */
	private static final String LOOKUP_NOTES_FOR_USER_STATEMENT = "SELECT * FROM Notes WHERE userId = ?";

	/**
	 * Query to delete a note
	 */
	private static final String DELETE_NOTE_STATEMENT = "DELETE FROM Notes WHERE userId = ? AND uuid = ?";

	/**
	 * Event logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(SqlDao.class);

	/**
	 * Security configurations
	 */
	protected final SecurityConfiguration securityConfiguration;

	/**
	 * SQL connection pool
	 */
	private final DataSource datasource;

	/**
	 * Construct a new instance
	 * 
	 * @param datasource SQL connection source
	 * @param securityConfiguration application security settings
	 */
	public SqlDao(final DataSource datasource, final SecurityConfiguration securityConfiguration) {
		this.datasource = datasource;
		this.securityConfiguration = securityConfiguration;
	}

	/**
	 * Resilience configuration key
	 * 
	 * @return resilience key
	 */
	@Override
	public String getResilienceRegistryKey() {
		return RESILIENCE_KEY;
	}

	/**
	 * Authenticate a user with their email and password
	 * 
	 * @param email user email address
	 * @param password user password
	 * @return the user data
	 * @throws DataAccessException accessing data failed
	 * @throws AuthenticationException authentication failed
	 */
	@Override
	public User authenticateUser(final String email, final String password)
			throws DataAccessException, AuthenticationException {
		return runPasswordCheckQuery(email, password, LOOKUP_USER_BY_EMAIL_STATEMENT);
	}

	/**
	 * Authenticate a user with their email and password
	 * 
	 * @param email user email address
	 * @param password user password
	 * @return the user data
	 * @throws DataAccessException accessing data failed
	 * @throws AuthenticationException authentication failed
	 */
	protected User runPasswordCheckQuery(final String email, final String password, final String query)
			throws DataAccessException, AuthenticationException {
		final Connection connection = buildConnection();

		final ResultSet resultSet = runQuery(connection, query, Arrays.asList(email));

		final List<User> users = readUsersFromResultSet(resultSet);

		if (users.isEmpty()) {
			DbUtils.closeQuietly(connection, null, resultSet);
			throw new AuthenticationDeniedException("User not found");
		}

		final User userFromDatabase = users.get(0);

		final String hashedPassword = userFromDatabase.getPassword();
		if (!checkPassword(password, hashedPassword)) {
			DbUtils.closeQuietly(connection, null, resultSet);
			throw new AuthenticationDeniedException("Passwords don't match");
		}

		DbUtils.closeQuietly(connection, null, resultSet);

		return userFromDatabase;
	}

	/**
	 * Submit a user to the database to be registered
	 * 
	 * @param user the user to register
	 * @return the registered user payload
	 * @throws DataAccessException if adding the user to the database fails
	 */
	@Override
	public User registerUser(final User user) throws DataAccessException {

		final Connection connection = buildConnection();

		final UUID uuid = UUID.randomUUID();

		user.setEmailVerified(false);

		try {
			runUpdate(connection, INSERT_USER_STATEMENT,
					Arrays.asList(uuid, user.getEmail(), hashPassword(user.getPassword()), user.getPhone(),
							user.getGivenName(), user.getSurname(), user.isEmailVerified(),
							user.getTwoFactorAuthPreference(), user.getOtpSecret(), user.getOtpAlgorithm(),
							user.getOtpStep(), user.getOtpDigits()),
					new IHandlesSqlErrors() {

						/**
						 * Check for the SQL state to see if a uniqueness constraint was violated
						 * 
						 * @param e the exception to handle
						 */
						@Override
						public void handleError(final SQLException e) throws DataAccessException {
							if (e.getSQLState().equals("23505")) {
								DbUtils.closeQuietly(connection);
								throw new EntityAlreadyExistsException("A user with the provided email already exists",
										e);
							}

						}
					});
		} catch (final GeneralSecurityException e) {
			throw new DataAccessException("Failed to hash password", e);
		} finally {
			DbUtils.closeQuietly(connection);
		}

		final User createdUser = new User(user);
		createdUser.setUuid(uuid);

		// for security purposes, don't return the user's password and otp secret
		createdUser.setPassword(null);
		createdUser.setOtpSecret(null);

		return createdUser;
	}

	/**
	 * Get all notes for a given user
	 * 
	 * @param userId the user whose notes we're looking up
	 * @param folderId if specified, only get notes in a given folder
	 * @return the notes for this user
	 * @throws DataAccessException error accessing data
	 */
	@Override
	public List<Note> getNotesForUser(final UUID userId, final Optional<UUID> folderId) throws DataAccessException {

		final Connection connection = buildConnection();

		String query = LOOKUP_NOTES_FOR_USER_STATEMENT;

		final List<Object> boundVariables = new ArrayList<>();
		boundVariables.add(userId);

		if (folderId.isPresent()) {
			query += " AND folderId = ?";
			boundVariables.add(folderId.get());
		}

		final ResultSet resultSet = runQuery(connection, query, boundVariables);

		final List<Note> notes = readNotesFromResultSet(resultSet);

		DbUtils.closeQuietly(resultSet);

		if (notes.size() == 0) {
			final ResultSet getUserResultSet = runQuery(connection, LOOKUP_USER_BY_ID_STATEMENT, Arrays.asList(userId));

			final List<User> users = readUsersFromResultSet(getUserResultSet);

			DbUtils.closeQuietly(getUserResultSet);

			if (users.size() == 0) {
				DbUtils.closeQuietly(connection);
				throw new EntityNotFoundException("User with ID " + userId + " not found");
			}

			// we also want to notify the caller if the specified folder ID is not found
			if (folderId.isPresent()) {
				final ResultSet getFolderResultSet = runQuery(connection, LOOKUP_FOLDER_BY_ID_STATEMENT,
						Arrays.asList(folderId.get()));

				final List<Folder> folder = readFoldersFromResultSet(getFolderResultSet);

				DbUtils.closeQuietly(getFolderResultSet);

				if (folder.size() == 0) {
					DbUtils.closeQuietly(connection);
					throw new EntityNotFoundException("Folder with ID " + folderId + " not found");
				}
			}
		}

		DbUtils.closeQuietly(connection);

		return notes;
	}

	/**
	 * Create a new note
	 * 
	 * @param userId the ID of the user who is creating the note
	 * @param note the note to create
	 * @return the created note
	 * @throws DataAccessException error accessing data
	 */
	@Override
	public Note createNote(final UUID userId, final Note note) throws DataAccessException {
		final Connection connection = buildConnection();

		if (note.getUuid() == null) {
			throw new IllegalArgumentException("Note ID must be asserted in order to create note");
		}

		runUpdate(
				connection, INSERT_NOTE_STATEMENT, Arrays.asList(note.getUuid(), note.getFolder(), userId,
						note.getTitle(), note.getBody(), note.getCreated(), note.getLastUpdated()),
				new IHandlesSqlErrors() {

					/**
					 * Catch foreign key violation errors and re-throw
					 * 
					 * @param e SQL exception
					 * @throws DataAccessException re-thrown exception
					 */
					@Override
					public void handleError(SQLException e) throws DataAccessException {
						if (e.getSQLState().equals("23503")) {
							DbUtils.closeQuietly(connection);
							throw new EntityNotFoundException("A user with this folder ID does not exist", e);
						}
					}

				});

		DbUtils.closeQuietly(connection);

		final Note createdNote = new Note(note);

		createdNote.setUser(userId);

		return createdNote;
	}

	/**
	 * Update a note
	 * 
	 * @param userId the ID of the user whose note we're deleting
	 * @param noteId the ID of the note to delete
	 * @param updateBody the update body
	 * @return the updated note
	 * @throws DataAccessException error accessing data
	 */
	@Override
	public Note updateNote(final UUID userId, final UUID noteId, final Note updateBody) throws DataAccessException {
		if (updateBody.getUuid() != null) {
			throw new DataAccessException("Note ID can't be updated");
		}

		if (updateBody.getUser() != null) {
			throw new DataAccessException("User ID can't be updated");
		}

		final Connection connection = buildConnection();

		final int rowsAffected = runUpdate(
				connection, UPDATE_NOTE_STATEMENT, Arrays.asList(updateBody.getTitle(), updateBody.getFolder(),
						updateBody.getBody(), updateBody.getCreated(), updateBody.getLastUpdated(), noteId, userId),
				new IHandlesSqlErrors() {

					@Override
					public void handleError(final SQLException e) throws DataAccessException {

						// currently, the only foreign key constraint that could be violated in this
						// query
						// is folderId
						if (e.getSQLState().equals("23503")) {
							DbUtils.closeQuietly(connection);
							throw new EntityNotFoundException("Folder ID " + updateBody.getFolder() + " not found", e);
						}
					}
				});

		if (rowsAffected == 0) {
			DbUtils.closeQuietly(connection);
			throw new EntityNotFoundException("Note with ID " + noteId + " not found");
		}

		final ResultSet resultSet = runQuery(connection, LOOKUP_NOTE_BY_ID_STATEMENT, Arrays.asList(noteId));

		final List<Note> notesUpdated = readNotesFromResultSet(resultSet);

		DbUtils.closeQuietly(connection, null, resultSet);

		if (notesUpdated.size() == 0) {
			throw new DataAccessException("Unexpected state encountered. Failed to find note with ID " + noteId);
		}

		return notesUpdated.get(0);
	}

	/**
	 * Delete a note
	 * 
	 * @param userId ID of the user whose note we're deleting
	 * @param noteId ID of the note to delete
	 * @throws DataAccessException error accessing data
	 */
	@Override
	public void deleteNote(final UUID userId, final UUID noteId) throws DataAccessException {
		final Connection connection = buildConnection();

		final int rowsAffected = runUpdate(connection, DELETE_NOTE_STATEMENT, Arrays.asList(userId, noteId));

		DbUtils.closeQuietly(connection);

		if (rowsAffected == 0) {
			throw new EntityNotFoundException("No note with ID " + noteId + " for user " + userId + " found");
		}
	}

	/**
	 * Get all folders for a user
	 * 
	 * @param userId the user whose folders we're looking up
	 * @return folders for the user
	 * @throws DataAccessException error accessing data
	 */
	@Override
	public List<Folder> getFoldersForUser(final UUID userId) throws DataAccessException {
		final Connection connection = buildConnection();

		final ResultSet resultSet = runQuery(connection, LOOKUP_FOLDERS_FOR_USER_STATEMENT, Arrays.asList(userId));

		final List<Folder> folders = readFoldersFromResultSet(resultSet);

		DbUtils.closeQuietly(resultSet);

		if (folders.size() == 0) {
			final ResultSet getUserResultSet = runQuery(connection, LOOKUP_USER_BY_ID_STATEMENT, Arrays.asList(userId));

			final List<User> users = readUsersFromResultSet(getUserResultSet);

			DbUtils.closeQuietly(getUserResultSet);

			if (users.size() == 0) {
				DbUtils.closeQuietly(connection);
				throw new EntityNotFoundException("User with ID " + userId + " not found");
			}
		}
		DbUtils.closeQuietly(connection);

		DbUtils.closeQuietly(connection);

		return folders;
	}

	/**
	 * Create a folder
	 * 
	 * @param userId the user identifier
	 * @param folderToCreate the folder to create
	 * @return the created folder
	 */
	@Override
	public Folder createFolder(final UUID userId, final Folder folderToCreate) throws DataAccessException {
		final Connection connection = buildConnection();

		final UUID uuid = UUID.randomUUID();

		runUpdate(connection, INSERT_FOLDER_STATEMENT, Arrays.asList(uuid, userId, folderToCreate.getName()),
				new IHandlesSqlErrors() {

					/**
					 * Handle an error
					 * 
					 * @param e the exception
					 * @throws DataAccessException not actually thrown here
					 */
					@Override
					public void handleError(final SQLException e) throws DataAccessException {
						if (e.getSQLState().equals("23503")) {
							DbUtils.closeQuietly(connection);
							throw new EntityNotFoundException("A user with this folder ID does not exist", e);
						}
					}
				});

		DbUtils.closeQuietly(connection);

		final Folder createdFolder = new Folder(folderToCreate);
		createdFolder.setUuid(uuid);
		createdFolder.setUser(userId);

		return createdFolder;
	}

	@Override
	public Folder updateFolder(final UUID userId, final UUID folderId, Folder updatePayload)
			throws DataAccessException {
		if (updatePayload.getUuid() != null) {
			throw new DataAccessException("Folder ID may not be updated");
		}

		if (updatePayload.getUser() != null) {
			throw new DataAccessException("User ID may not be updated");
		}

		final Connection connection = buildConnection();

		runUpdate(connection, UPDATE_FOLDER_STATEMENT, Arrays.asList(updatePayload.getName(), folderId, userId));

		final ResultSet resultSet = runQuery(connection, LOOKUP_FOLDER_BY_ID_STATEMENT, Arrays.asList(folderId));

		final List<Folder> folders = readFoldersFromResultSet(resultSet);

		DbUtils.closeQuietly(connection, null, resultSet);

		if (folders.isEmpty()) {
			throw new EntityNotFoundException("Folder with ID " + folderId + " not found");
		}

		return folders.get(0);
	}

	/**
	 * Search for a note in the database based on a set of search terms
	 * 
	 * @param userId the user ID
	 * @param query search terms
	 * @return notes from the query. If user ID doesn't exist, an empty array is
	 * returned
	 * @throws DataAccessException error accessing data
	 */
	@Override
	public List<Note> searchNotes(final UUID userId, List<String> query) throws DataAccessException {
		String searchQuery = "SELECT * FROM Notes WHERE userId = ?";

		if (query.size() > 0) {
			searchQuery = buildNoteSearchQuery(query.size());
		}

		final Connection connection = buildConnection();

		final List<Object> search = new ArrayList<Object>();
		search.add(userId);

		final List<String> searchTerms = new ArrayList<String>();
		for (final String queryTerm : query) {
			searchTerms.add("%" + queryTerm + "%");
		}

		search.addAll(new ArrayList<Object>(searchTerms));

		search.addAll(new ArrayList<Object>(searchTerms));

		final ResultSet resultSet = runQuery(connection, searchQuery, search);

		final List<Note> notes = readNotesFromResultSet(resultSet);

		DbUtils.closeQuietly(connection, null, resultSet);

		return notes;
	}

	@Override
	public User updateUser(final UUID userUuid, final User updatePayload) throws DataAccessException {
		if (updatePayload.getUuid() != null) {
			throw new DataAccessException("User ID may not be updated");
		}

		final Connection connection = buildConnection();

		try {
			runUpdate(connection, UPDATE_USER_STATEMENT,
					Arrays.asList(updatePayload.getEmail(), hashPassword(updatePayload.getPassword()),
							updatePayload.getPhone(), updatePayload.getGivenName(), updatePayload.getSurname(),
							updatePayload.isEmailVerified(), updatePayload.getTwoFactorAuthPreference(),
							updatePayload.getOtpSecret(), updatePayload.getOtpAlgorithm(), updatePayload.getOtpStep(),
							updatePayload.getOtpDigits(), userUuid));
		} catch (GeneralSecurityException e) {
			DbUtils.closeQuietly(connection);
			throw new DataAccessException("Failed to hash password", e);
		}

		final ResultSet resultSet = runQuery(connection, LOOKUP_USER_BY_ID_STATEMENT, Arrays.asList(userUuid));

		final List<User> users = readUsersFromResultSet(resultSet);

		DbUtils.closeQuietly(connection, null, resultSet);

		if (users.isEmpty()) {
			throw new EntityNotFoundException("User with ID " + userUuid + " not found");
		}

		if (users.size() > 1) {
			throw new DataAccessException(
					"Unexpected state encountered. Multiple users with ID " + userUuid + " found");
		}

		return users.get(0);
	}

	@Override
	public User lookupUser(final UUID userId) throws DataAccessException {
		final Connection connection = buildConnection();

		final ResultSet resultSet = runQuery(connection, LOOKUP_USER_BY_ID_STATEMENT, Arrays.asList(userId));

		final List<User> users = readUsersFromResultSet(resultSet);

		if (users.isEmpty()) {
			throw new EntityNotFoundException("User with ID " + userId + " not found");
		}

		if (users.size() > 1) {
			throw new DataAccessException("Multiple users with ID " + userId + " found in database");
		}

		DbUtils.closeQuietly(connection, null, resultSet);

		return users.get(0);
	}

	/**
	 * Hash a password using the Blowfish cipher, random salt, and a configured cost
	 * factor
	 * 
	 * @param password the password to hash
	 * @return the hashed value, with the cost factor and salt appended
	 * @throws GeneralSecurityException if generating the hash fails
	 */
	private String hashPassword(final String password) throws GeneralSecurityException {
		if (password == null) {
			return null;
		}

		final byte[] salt = new byte[16];

		SecureRandom.getInstanceStrong().nextBytes(salt);

		final int cost = securityConfiguration.getHashCost();

		final String hash = Base64.getEncoder()
				.encodeToString(BCrypt.generate(password.getBytes(), salt, securityConfiguration.getHashCost()));

		return String.format("$%s$%s$%s", cost, Base64.getEncoder().encodeToString(salt), hash);
	}

	/**
	 * Check a user's password against a hashed password entry from the database
	 * 
	 * @param password user password entered
	 * @param hashedPassword hashed password from database
	 * @return true if passwords match, false if not
	 */
	private boolean checkPassword(final String password, final String hashedPassword) {
		final String[] parts = hashedPassword.split("\\$");

		final int cost = Integer.parseInt(parts[1]);

		final byte[] salt = Base64.getDecoder().decode(parts[2]);

		final byte[] hashedCheckPassword = BCrypt.generate(password.getBytes(), salt, cost);

		return parts[3].equals(Base64.getEncoder().encodeToString(hashedCheckPassword));
	}

	/**
	 * Read users from a result set
	 * 
	 * @param resultSet the SQL result set to read from
	 * @return the list of users retrieved
	 * @throws DataAccessException if reading from the result set failed.
	 */
	protected List<User> readUsersFromResultSet(final ResultSet resultSet) throws DataAccessException {
		final List<User> users = new ArrayList<User>();

		LOGGER.info("Result set: " + resultSet);
		try {
			while (resultSet.next()) {
				final User user = new User();
				user.setUuid((UUID) resultSet.getObject("uuid"));
				user.setEmail(resultSet.getString("email"));
				user.setPassword(resultSet.getString("password"));
				user.setGivenName(resultSet.getString("givenName"));
				user.setPhone(resultSet.getString("phone"));
				user.setSurname(resultSet.getString("surname"));
				user.setEmailVerified(resultSet.getBoolean("emailVerified"));
				user.setTwoFactorAuthPreference(resultSet.getInt("twoFactorAuthPreference"));
				user.setOtpSecret(resultSet.getString("otpSecret"));
				user.setOtpAlgorithm(resultSet.getString("otpAlgorithm"));
				user.setOtpStep(resultSet.getInt("otpStep"));
				user.setOtpDigits(resultSet.getInt("otpDigits"));

				users.add(user);
			}
		} catch (final SQLException e) {
			LOGGER.error("SQL error state: " + e.getSQLState());
			throw new DataAccessException("Failed to read user from result set", e);
		}

		return users;
	}

	/**
	 * Read folders from a result set
	 * 
	 * @param resultSet the SQL result set to read from
	 * @return the list of folders retrieved
	 * @throws DataAccessException if reading from the result set failed.
	 */
	private List<Folder> readFoldersFromResultSet(final ResultSet resultSet) throws DataAccessException {
		final List<Folder> folders = new ArrayList<Folder>();

		try {
			while (resultSet.next()) {
				final Folder folder = new Folder();
				folder.setUuid((UUID) resultSet.getObject("uuid"));
				folder.setUser((UUID) resultSet.getObject("userId"));
				folder.setName(resultSet.getString("name"));

				folders.add(folder);
			}
		} catch (final SQLException e) {
			LOGGER.error("SQL error state: " + e.getSQLState());
			throw new DataAccessException("Failed to read folder from result set", e);
		}

		return folders;
	}

	/**
	 * Read notes from a result set
	 * 
	 * @param resultSet the SQL result set to read from
	 * @return the list of notes retrieved
	 * @throws DataAccessException if reading from the result set failed.
	 */
	private List<Note> readNotesFromResultSet(final ResultSet resultSet) throws DataAccessException {
		final List<Note> notes = new ArrayList<Note>();
		try {
			while (resultSet.next()) {
				final Note note = new Note();
				note.setUuid((UUID) resultSet.getObject("uuid"));
				note.setFolder((UUID) resultSet.getObject("folderId"));
				note.setUser((UUID) resultSet.getObject("userId"));
				note.setTitle(resultSet.getString("title"));
				note.setBody(resultSet.getString("body"));

				if (resultSet.getTimestamp("created") != null) {
					note.setCreated(new Date(resultSet.getTimestamp("created").getTime()));
				}

				if (resultSet.getTimestamp("lastUpdated") != null) {
					note.setLastUpdated(new Date(resultSet.getTimestamp("lastUpdated").getTime()));
				}

				notes.add(note);
			}
		} catch (final SQLException e) {
			LOGGER.error("SQL error state: " + e.getSQLState());
			throw new DataAccessException("Failed to read note from result set", e);
		}

		return notes;
	}

	/**
	 * Build an SQL connection. Don't forget to close it after you're done!
	 * 
	 * @return a new database connection
	 * @throws DataAccessException error occurred building the connection
	 */
	protected Connection buildConnection() throws DataAccessException {

		Connection connection;
		try {
			connection = datasource.getConnection();
		} catch (final SQLException e) {
			LOGGER.error("SQL error state: " + e.getSQLState());
			throw new DataAccessException("SQL Error Occurred", e);
		}

		return connection;
	}

	/**
	 * Run a database query using a prepared statement and bound variables
	 * 
	 * @param connection the database connection to run the query against
	 * @param query the query to run
	 * @param boundData the data to bind to the prepared statement
	 * @param isQuery true if the statement is a query and is expected to return a
	 * result set (e.g. a SELECT statement)
	 * @return the result set from the query
	 * @throws DataAccessException error occurred running the query. The connection
	 * will be closed before the exception is thrown
	 */
	protected ResultSet runQuery(final Connection connection, final String query, List<Object> boundData)
			throws DataAccessException {
		return runQuery(connection, query, boundData, new IHandlesSqlErrors() {

			/**
			 * Handle an error
			 * 
			 * @param e the exception
			 * @throws DataAccessException not actually thrown here
			 */
			@Override
			public void handleError(final SQLException e) throws DataAccessException {
				// no handling
			}
		});
	}

	/**
	 * Run a database query using a prepared statement and bound variables
	 * 
	 * @param connection the database connection to run the query against
	 * @param query the query to run
	 * @param boundData the data to bind to the prepared statement
	 * @param queryErrorHandler handle an error that occurs as a result of running
	 * the query
	 * @param isQuery true if the statement is a query and is expected to return a
	 * result set (e.g. a SELECT statement)
	 * @return the result set from the query
	 * @throws DataAccessException error occurred running the query. The connection
	 * will be closed before the exception is thrown
	 */
	private ResultSet runQuery(final Connection connection, final String query, List<Object> boundData,
			final IHandlesSqlErrors queryErrorHandler) throws DataAccessException {

		PreparedStatement statement = null;

		try {
			statement = connection.prepareStatement(query);
		} catch (final SQLException e) {
			LOGGER.error("SQL error state: " + e.getSQLState());
			DbUtils.closeQuietly(connection);
			throw new DataAccessException("SQL Error Occurred", e);
		}

		ResultSet resultSet = null;
		try {
			int parameterIndex = 1;
			for (final Object boundVar : boundData) {
				setBoundVariable(statement, parameterIndex, boundVar);
				parameterIndex++;
			}

			LOGGER.debug("Executing query: " + statement.toString());
			resultSet = statement.executeQuery();

		} catch (final SQLException e) {
			LOGGER.error("SQL error state: " + e.getSQLState());
			queryErrorHandler.handleError(e);
			DbUtils.closeQuietly(statement);
			DbUtils.closeQuietly(connection);
			throw new DataAccessException("SQL Error Occurred", e);
		}

		return resultSet;
	}

	/**
	 * Run a database query using a prepared statement and bound variables
	 * 
	 * @param connection the database connection to run the query against
	 * @param query the query to run
	 * @param boundData the data to bind to the prepared statement
	 * @return the result set from the query
	 * @throws DataAccessException error occurred running the query. The connection
	 * will be closed before the exception is thrown
	 */
	private int runUpdate(final Connection connection, final String query, List<Object> boundData)
			throws DataAccessException {
		return runUpdate(connection, query, boundData, new IHandlesSqlErrors() {

			/**
			 * Handle an error
			 * 
			 * @param e the exception
			 * @throws DataAccessException not actually thrown here
			 */
			@Override
			public void handleError(final SQLException e) throws DataAccessException {
				// no handling
			}
		});
	}

	/**
	 * Run a database query using a prepared statement and bound variables
	 * 
	 * @param connection the database connection to run the query against
	 * @param query the query to run
	 * @param boundData the data to bind to the prepared statement
	 * @param queryErrorHandler handle an error that occurs as a result of running
	 * the query
	 * @return the number of rows affected
	 * @throws DataAccessException error occurred running the query. The connection
	 * will be closed before the exception is thrown
	 */
	protected int runUpdate(final Connection connection, final String query, List<Object> boundData,
			final IHandlesSqlErrors queryErrorHandler) throws DataAccessException {

		PreparedStatement statement = null;

		try {
			statement = connection.prepareStatement(query);
		} catch (final SQLException e) {
			LOGGER.error("SQL error state: " + e.getSQLState());
			DbUtils.closeQuietly(connection);
			throw new DataAccessException("SQL Error Occurred", e);
		}

		try {
			int parameterIndex = 1;
			for (final Object boundVar : boundData) {
				setBoundVariable(statement, parameterIndex, boundVar);
				parameterIndex++;
			}

			return statement.executeUpdate();

		} catch (final SQLException e) {
			LOGGER.error("SQL error state: " + e.getSQLState());
			queryErrorHandler.handleError(e);
			DbUtils.closeQuietly(statement);
			DbUtils.closeQuietly(connection);
			throw new DataAccessException("SQL Error Occurred", e);
		}
	}

	/**
	 * Bind a variable to a prepared statement
	 * 
	 * @param statement prepared SQL statement
	 * @param parameterIndex parameter index within the context of the bound
	 * statement
	 * @param boundVar the variable to bind to the prepared statement/query
	 * @throws SQLException if binding variables fails
	 */
	private void setBoundVariable(final PreparedStatement statement, final int parameterIndex, final Object boundVar)
			throws SQLException {
		if (boundVar instanceof String) {
			statement.setString(parameterIndex, ((String) boundVar));
		} else if (boundVar instanceof Date) {
			statement.setTimestamp(parameterIndex, new Timestamp(((Date) boundVar).getTime()));
		} else {
			statement.setObject(parameterIndex, boundVar);
		}
	}

	/**
	 * Build a query to search the notes table by a set of search terms
	 * 
	 * @param numTerms number of terms we're going to search by
	 * @return the search query
	 */
	private String buildNoteSearchQuery(final int numTerms) {
		String searchQuery = "SELECT * FROM Notes WHERE (userId = ?";

		final String[] titleLikeQueries = new String[numTerms];
		for (int i = 0; i < numTerms; i++) {
			titleLikeQueries[i] = "title LIKE ?";
		}

		final String[] bodyLikeQueries = new String[numTerms];
		for (int i = 0; i < numTerms; i++) {
			bodyLikeQueries[i] = "body LIKE ?";
		}

		searchQuery += " AND (" + String.join(" AND ", titleLikeQueries) + ")";

		searchQuery += " OR (" + String.join(" AND ", bodyLikeQueries) + ")";

		searchQuery += ")";

		return searchQuery;
	}

	@Override
	public boolean checkOtp(final String otp, final UUID userUuid) throws DataAccessException {
		final Connection connection = buildConnection();

		final ResultSet resultSet = runQuery(connection, LOOKUP_USER_BY_ID_STATEMENT, Arrays.asList(userUuid));

		final List<User> users = readUsersFromResultSet(resultSet);

		DbUtils.closeQuietly(connection, null, resultSet);

		if (users.isEmpty()) {
			throw new EntityNotFoundException("User with ID " + userUuid + " not found");
		}

		if (users.size() > 1) {
			throw new DataAccessException("Multiple users with ID " + userUuid + " found in database");
		}

		if (users.get(0).getOtpAlgorithm() == null) {
			throw new DataAccessException("OTP algorithm not set for user " + userUuid);
		}

		HashingAlgorithm algorithm;
		try {
			algorithm = HashingAlgorithm.valueOf(users.get(0).getOtpAlgorithm());
		} catch (final IllegalArgumentException e) {
			throw new DataAccessException("User " + userUuid + " has an invalid OTP algorithm set", e);
		}

		final TimeProvider timeProvider = new SystemTimeProvider();
		final CodeGenerator codeGenerator = new DefaultCodeGenerator(algorithm,
				(users.get(0).getOtpDigits() != 0) ? users.get(0).getOtpDigits() : 6);
		final DefaultCodeVerifier otpCodeVerifier = new DefaultCodeVerifier(codeGenerator, timeProvider);

		otpCodeVerifier.setTimePeriod((users.get(0).getOtpStep() != 0) ? users.get(0).getOtpStep() : 30);

		if (users.get(0).getOtpSecret() == null) {
			LOGGER.info("Attempted to check OTP, but no OTP set up for user " + userUuid);
			return false;
		}

		LOGGER.info("OTP secret key: " + users.get(0).getOtpSecret());

		return otpCodeVerifier.isValidCode(users.get(0).getOtpSecret(), otp);
	}

	@Override
	public boolean isOtpAlgorithmValid(final String algorithm) {
		try {
			HashingAlgorithm.valueOf(algorithm);
			return true;
		} catch (final IllegalArgumentException e) {
			return false;
		}
	}
}
