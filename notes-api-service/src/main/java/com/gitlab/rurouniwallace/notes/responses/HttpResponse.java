package com.gitlab.rurouniwallace.notes.responses;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Base response to an HTTP operation
 */
public abstract class HttpResponse {

	/**
	 * The HTTP status
	 */
	@JsonIgnore
	protected int httpStatus = 200;

	/**
	 * @return the httpStatus
	 */
	public int getHttpStatus() {
		return httpStatus;
	}

	/**
	 * @param httpStatus the httpStatus to set
	 */
	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}
}
