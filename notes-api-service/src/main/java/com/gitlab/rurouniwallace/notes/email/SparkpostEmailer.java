package com.gitlab.rurouniwallace.notes.email;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.rurouniwallace.notes.config.SparkpostConfiguration;
import com.gitlab.rurouniwallace.notes.email.sparkpost.TransmissionContent;
import com.gitlab.rurouniwallace.notes.email.sparkpost.TransmissionRecipient;
import com.gitlab.rurouniwallace.notes.email.sparkpost.TransmissionRequest;
import com.gitlab.rurouniwallace.notes.exceptions.EmailTransmissionException;

public class SparkpostEmailer implements ISendsEmails {

	private static final Logger LOGGER = LoggerFactory.getLogger(SparkpostEmailer.class);

	private static final String RESILIENCE_KEY = "sparkpost";

	private final SparkpostConfiguration config;

	private final Client client;

	private final ObjectMapper objectMapper;

	public SparkpostEmailer(final SparkpostConfiguration config, final Client client) {
		this.config = config;
		this.client = client;
		objectMapper = new ObjectMapper();
	}

	@Override
	public String getResilienceRegistryKey() {
		return RESILIENCE_KEY;
	}

	@Override
	public void sendVerificationEmail(final String emailAddress, final String verificationUrl)
			throws EmailTransmissionException {
		final String url = String.format("%s/api/v1/transmissions", config.getHostname());
		final WebTarget target = client.target(url);

		final TransmissionContent content = new TransmissionContent();
		content.setTemplateId(config.getEmailVerificationTemplate());

		final List<TransmissionRecipient> recipients = Arrays.asList(new TransmissionRecipient(emailAddress));

		final Map<String, String> substitutionData = new HashMap<>();
		substitutionData.put("verification_url", verificationUrl);

		final TransmissionRequest request = new TransmissionRequest(recipients, content, substitutionData);

		String requestString;
		try {
			requestString = objectMapper.writeValueAsString(request);
		} catch (final JsonProcessingException e) {
			throw new EmailTransmissionException("Failed to write request body as JSON", e);
		}

		LOGGER.info("Sending POST to " + url + " with request body " + requestString);

		Response response;
		try {
			response = target.request().header("Authorization", config.getApikey())
					.post(Entity.entity(requestString, MediaType.APPLICATION_JSON));
		} catch (final ProcessingException e) {
			throw new EmailTransmissionException("Failed to make HTTP request to SparkPost API", e);
		}

		final String responseString = response.readEntity(String.class);

		LOGGER.info("Received raw response " + responseString);

		if (response.getStatus() != HttpStatus.OK_200) {
			throw new EmailTransmissionException("Non-200 status received from Sparkpost API: " + response.getStatus());
		}
	}
}
