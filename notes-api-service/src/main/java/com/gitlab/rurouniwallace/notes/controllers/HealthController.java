package com.gitlab.rurouniwallace.notes.controllers;

import java.util.SortedMap;

import javax.ws.rs.container.AsyncResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.health.HealthCheck.Result;
import com.gitlab.rurouniwallace.notes.health.Health;
import com.gitlab.rurouniwallace.notes.health.HealthCheck;
import com.gitlab.rurouniwallace.notes.messages.HealthMessages;
import com.gitlab.rurouniwallace.notes.resilience.ResilienceRegistry;

import io.dropwizard.setup.Environment;

public class HealthController extends BaseController {

	private static final String RESILIENCE_KEY = "health";

	/**
	 * Event logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(HealthController.class);

	/**
	 * Dropwizard environment handle
	 */
	private final Environment environment;

	/**
	 * Construct a new instance
	 * 
	 * @param environment dropwizard environment handle
	 */
	public HealthController(final Environment environment, final ResilienceRegistry resilienceRegistry) {
		super(resilienceRegistry);
		this.environment = environment;
	}

	/**
	 * Run health check
	 * 
	 * @return result of health check
	 */
	public void checkHealth(final AsyncResponse response) {
		respondAsync(RESILIENCE_KEY, response, () -> {
			final SortedMap<String, Result> results = environment.healthChecks()
					.runHealthChecks(environment.getHealthCheckExecutorService());
			final Health health = new Health();

			for (final String key : results.keySet()) {
				final HealthCheck okCheck = new HealthCheck(HealthMessages.STATUS_OK, key + " is healthy");

				final HealthCheck criticalCheck = new HealthCheck(HealthMessages.STATUS_CRITICAL,
						key + " has a problem");

				final Result result = results.get(key);

				final HealthCheck check = result.isHealthy() ? okCheck : criticalCheck;

				health.addCheck(key, check);
			}

			return health;
		});
	}
}
