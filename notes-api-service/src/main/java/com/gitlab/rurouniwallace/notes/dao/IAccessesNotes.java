package com.gitlab.rurouniwallace.notes.dao;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.gitlab.rurouniwallace.notes.exceptions.DataAccessException;
import com.gitlab.rurouniwallace.notes.models.Folder;
import com.gitlab.rurouniwallace.notes.models.Note;
import com.gitlab.rurouniwallace.notes.resilience.IResilient;

/**
 * Interface for accessing user notes
 */
public interface IAccessesNotes extends IResilient {

	/**
	 * Get all notes for a given user
	 * 
	 * @param userId the user whose notes we're looking up
	 * @param folderId if specified, only get notes in a given folder
	 * @return the notes for this user
	 * @throws DataAccessException error accessing data
	 */
	public List<Note> getNotesForUser(final UUID userId, final Optional<UUID> folderId) throws DataAccessException;

	/**
	 * Create a new note
	 * 
	 * @param userId the ID of the user who is creating the note
	 * @param note the note to create
	 * @return the created note
	 * @throws DataAccessException error accessing data
	 */
	public Note createNote(final UUID userId, final Note note) throws DataAccessException;

	/**
	 * Update a note
	 * 
	 * @param userId the ID of the user whose note we're deleting
	 * @param noteId the ID of the note to delete
	 * @param updateBody the update body
	 * @return the updated note
	 * @throws DataAccessException error accessing data
	 */
	public Note updateNote(final UUID userId, final UUID noteId, final Note updateBody) throws DataAccessException;

	/**
	 * Delete a note
	 * 
	 * @param userId ID of the user whose note we're deleting
	 * @param noteId ID of the note to delete
	 * @throws DataAccessException error accessing data
	 */
	public void deleteNote(final UUID userId, final UUID noteId) throws DataAccessException;

	/**
	 * Create a new folder
	 * 
	 * @param userId the ID of the user creating this folder
	 * @param folderToCreate the folder to create
	 * @return the created folder
	 * @throws DataAccessException
	 */
	public Folder createFolder(final UUID userId, final Folder folderToCreate) throws DataAccessException;

	/**
	 * Update a folder
	 * 
	 * @param userId the ID of the user whose folder we're updating
	 * @param folderId the ID of the folder we're updating
	 * @param updatePayload the update payload
	 * @return the updated folder
	 * @throws DataAccessException
	 */
	public Folder updateFolder(final UUID userId, final UUID folderId, final Folder updatePayload)
			throws DataAccessException;

	/**
	 * Get all folders for a user
	 * 
	 * @param userId the user whose folders we're looking up
	 * @return folders for the user
	 * @throws DataAccessException error accessing data
	 */
	public List<Folder> getFoldersForUser(final UUID userId) throws DataAccessException;

	/**
	 * Search for a note in the database based on a set of search terms
	 * 
	 * @param userId the user ID
	 * @param query search terms
	 * @return notes from the query. If user ID doesn't exist, an empty array is
	 * returned
	 * @throws DataAccessException error accessing data
	 */
	public List<Note> searchNotes(final UUID userId, final List<String> query) throws DataAccessException;
}