package com.gitlab.rurouniwallace.notes.controllers;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.Cookie;

import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gitlab.rurouniwallace.notes.config.SessionConfiguration;
import com.gitlab.rurouniwallace.notes.dao.IAccessesNotes;
import com.gitlab.rurouniwallace.notes.exceptions.EntityNotFoundException;
import com.gitlab.rurouniwallace.notes.models.Note;
import com.gitlab.rurouniwallace.notes.resilience.ResilienceRegistry;
import com.gitlab.rurouniwallace.notes.responses.NoteResponse;
import com.gitlab.rurouniwallace.notes.responses.NotesListResponse;
import com.gitlab.rurouniwallace.notes.responses.StandardResponse;
import com.gitlab.rurouniwallace.notes.responses.StatusCode;

/**
 * Logic controller for the notes resource
 */
public class NoteController extends BaseController {

	/**
	 * Event logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(NoteController.class);

	/**
	 * User data access interface
	 */
	private final IAccessesNotes notesDao;

	private final SearchController searchController;

	/**
	 * Auth session configuration
	 */
	private final SessionConfiguration sessionConfig;

	/**
	 * Construct a new instance
	 * 
	 * @param userDao the user data access interface
	 */
	public NoteController(final IAccessesNotes notesDao, SearchController searchController,
			final SessionConfiguration sessionConfig, final ResilienceRegistry resilienceRegistry) {
		super(resilienceRegistry);
		this.notesDao = notesDao;
		this.searchController = searchController;
		this.sessionConfig = sessionConfig;
	}

	/**
	 * Get all notes for a user, optionally filtering by folder
	 * 
	 * @param userId the user identifier
	 * @param folderId the folder to filter by
	 * @param response the HTTP response
	 */
	public void getNotesForUser(final UUID userId, final Optional<UUID> folderId, final Cookie sessionCookie,
			final AsyncResponse response) {
		// checkAuth(sessionCookie, sessionConfig, userId, response);

		final String resilienceKey = notesDao.getResilienceRegistryKey();
		respondAsync(resilienceKey, response, () -> {
			List<Note> notes;
			try {
				notes = notesDao.getNotesForUser(userId, folderId);
			} catch (final EntityNotFoundException e) {
				return new NotesListResponse("Note ID or folder ID not found", StatusCode.ENTITY_NOT_FOUND,
						HttpStatus.NOT_FOUND_404);
			}

			return new NotesListResponse(notes, StatusCode.SUCCESS);
		});
	}

	/**
	 * Create a new note
	 * 
	 * @param userId the user identifier
	 * @param note the note to create
	 * @param response the response
	 */
	public void createNote(final UUID userId, final Note note, final Cookie sessionCookie,
			final AsyncResponse response) {
		// checkAuth(sessionCookie, sessionConfig, userId, response);

		if (note.getUuid() != null) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"UUID may not be asserted when creating a note"));
		}

		if (note.getUser() != null) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"User ID must not appear in request body"));
		}

		if (note.getCreated() != null) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"Creation date must not appear in request body"));
		}

		if (note.getLastUpdated() != null) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"Last updated date must not appear in request body"));
		}

		note.setUuid(UUID.randomUUID());
		note.setUser(userId);

		final Date currrentDate = new Date();
		note.setCreated(currrentDate);
		note.setLastUpdated(currrentDate);

		respondAsync(notesDao.getResilienceRegistryKey(), response, () -> {
			Note noteFromDao;
			try {
				noteFromDao = notesDao.createNote(userId, note);
			} catch (final EntityNotFoundException e) {
				return new NoteResponse("User ID " + userId + " not found", StatusCode.ENTITY_NOT_FOUND);
			}

			return new NoteResponse(noteFromDao, StatusCode.CREATED, HttpStatus.CREATED_201);
		});

		searchController.indexNote(userId, note);
	}

	/**
	 * Update a note
	 * 
	 * @param userId the user identifier
	 * @param noteId the ID of the note to update
	 * @param updatePayload the update to make to the note
	 * @param response the HTTP response
	 */
	public void updateNote(final UUID userId, final UUID noteId, final Note updatePayload, final Cookie sessionCookie,
			final AsyncResponse response) {
		// checkAuth(sessionCookie, sessionConfig, userId, response);

		if (updatePayload.getUuid() != null) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"UUID may not be updated"));
		}

		if (updatePayload.getUser() != null) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"User ID may not be updated"));
		}

		if (updatePayload.getCreated() != null) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"Creation date may not be updated"));
		}

		if (updatePayload.getLastUpdated() != null) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"Last updated date may not be updated"));
		}

		final Date currrentDate = new Date();
		updatePayload.setLastUpdated(currrentDate);

		final String resilienceKey = notesDao.getResilienceRegistryKey();
		respondAsync(resilienceKey, response, () -> {
			final Note note;
			try {
				note = notesDao.updateNote(userId, noteId, updatePayload);
			} catch (final EntityNotFoundException e) {
				return new NoteResponse("Note with ID " + noteId + " not found", StatusCode.ENTITY_NOT_FOUND,
						HttpStatus.NOT_FOUND_404);
			}

			return new NoteResponse(note, StatusCode.SUCCESS);
		});

		searchController.updateIndexedNote(userId, noteId, updatePayload);
	}

	/**
	 * Delete a note
	 * 
	 * @param userId the user identifier
	 * @param noteId the ID of the note to delete
	 * @param sessionCookie session cookie
	 * @param response the HTTP response
	 */
	public void deleteNote(final UUID userId, final UUID noteId, final Cookie sessionCookie,
			final AsyncResponse response) {
		// checkAuth(sessionCookie, sessionConfig, userId, response);

		final String resilienceKey = notesDao.getResilienceRegistryKey();
		respondAsync(resilienceKey, response, () -> {
			try {
				notesDao.deleteNote(userId, noteId);
			} catch (final EntityNotFoundException e) {
				return new StandardResponse("Note with ID " + noteId + " not found", StatusCode.ENTITY_NOT_FOUND,
						HttpStatus.NOT_FOUND_404);
			}

			return new StandardResponse(StatusCode.SUCCESS);
		});

		searchController.deleteIndexedNote(userId, noteId);
	}

	/**
	 * Search for a note or set of notes
	 * 
	 * @param userId user whose notes we're searching
	 * @param query the search query
	 * @param sessionCookie session cookie
	 * @param response the HTTP response
	 */
	public void searchNotes(final UUID userId, final String query, final Integer numResults, final Cookie sessionCookie,
			final AsyncResponse response) {
		// checkAuth(sessionCookie, sessionConfig, userId, response);

		if (query == null) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"Query must be specified"));
		}

		if (numResults == null) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"NumResults must be specified"));
		}

		if (numResults <= 0) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"NumResults must be greater than 0"));
		}

		final Set<String> terms = new HashSet<>(Arrays.asList(query.split(",")));

		searchController.searchNotes(userId, terms, numResults, response);
	}
}
