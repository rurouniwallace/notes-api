package com.gitlab.rurouniwallace.notes.email.sparkpost;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TransmissionRequest {

	@JsonProperty("substitution_data")
	private Map<String, String> substitutionData;

	private List<TransmissionRecipient> recipients;

	private TransmissionContent content;

	/**
	 * Construct a new instance
	 * 
	 * @param recipients
	 * @param content
	 * @param substitutionData
	 */
	public TransmissionRequest(final List<TransmissionRecipient> recipients, final TransmissionContent content,
			final Map<String, String> substitutionData) {
		this.recipients = recipients;
		this.content = content;
		this.substitutionData = substitutionData;
	}

	/**
	 * @return the substitutionData
	 */
	public Map<String, String> getSubstitutionData() {
		return substitutionData;
	}

	/**
	 * @param substitutionData the substitutionData to set
	 */
	public void setSubstitutionData(Map<String, String> substitutionData) {
		this.substitutionData = substitutionData;
	}

	/**
	 * @return the recipients
	 */
	public List<TransmissionRecipient> getRecipients() {
		return recipients;
	}

	/**
	 * @param recipients the recipients to set
	 */
	public void setRecipients(final List<TransmissionRecipient> recipients) {
		this.recipients = recipients;
	}

	/**
	 * @return the content
	 */
	public TransmissionContent getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(final TransmissionContent content) {
		this.content = content;
	}
}
