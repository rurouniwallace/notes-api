package com.gitlab.rurouniwallace.notes.resilience;

import java.util.Map;

import io.github.resilience4j.bulkhead.ThreadPoolBulkhead;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.ratelimiter.RateLimiter;
import io.github.resilience4j.timelimiter.TimeLimiter;

public class ResilienceRegistry {

	private final Map<String, CircuitBreaker> circuitBreakers;

	private final Map<String, ThreadPoolBulkhead> threadPoolBulkheads;

	private final Map<String, RateLimiter> rateLimiters;

	private final Map<String, TimeLimiter> timeLimiters;

	public ResilienceRegistry(final Map<String, CircuitBreaker> circuitBreakers,
			final Map<String, ThreadPoolBulkhead> threadPoolBulkheads, final Map<String, RateLimiter> rateLimiters,
			final Map<String, TimeLimiter> timeLimiters) {
		this.circuitBreakers = circuitBreakers;
		this.threadPoolBulkheads = threadPoolBulkheads;
		this.rateLimiters = rateLimiters;
		this.timeLimiters = timeLimiters;
	}

	/**
	 * @return the circuitBreakers
	 */
	public Map<String, CircuitBreaker> getCircuitBreakers() {
		return circuitBreakers;
	}

	/**
	 * @return the threadPoolBulkheads
	 */
	public Map<String, ThreadPoolBulkhead> getThreadPoolBulkheads() {
		return threadPoolBulkheads;
	}

	/**
	 * @return the rateLimiters
	 */
	public Map<String, RateLimiter> getRateLimiters() {
		return rateLimiters;
	}

	/**
	 * @return the timeLimiters
	 */
	public Map<String, TimeLimiter> getTimeLimiters() {
		return timeLimiters;
	}
}
