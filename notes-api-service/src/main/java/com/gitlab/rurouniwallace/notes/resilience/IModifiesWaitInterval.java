package com.gitlab.rurouniwallace.notes.resilience;

import io.vavr.control.Either;

public interface IModifiesWaitInterval {

	public int modifyWaitInterval(final Either<Object, Throwable> either);
}
