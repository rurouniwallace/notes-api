package com.gitlab.rurouniwallace.notes.passwords.verification;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gitlab.rurouniwallace.notes.config.HIBPConfiguration;

public class HIBPConnector implements IChecksPasswordCompromised {

	private static final Logger LOGGER = LoggerFactory.getLogger(HIBPConnector.class);

	private static final String RESILIENCE_KEY = "hibp";

	private final HIBPConfiguration config;

	private final Client httpClient;

	public HIBPConnector(final HIBPConfiguration config, final Client httpClient) {
		this.config = config;
		this.httpClient = httpClient;
	}

	@Override
	public String getResilienceRegistryKey() {
		return RESILIENCE_KEY;
	}

	@Override
	public int isCompromised(final String password) throws PasswordVerificationException {
		final MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-1");
		} catch (final NoSuchAlgorithmException e) {
			throw new PasswordVerificationException(
					"Invalid hash algorithm specified. Likely a programming error or typo", e);
		}

		digest.reset();

		try {
			digest.update(password.getBytes("utf8"));
		} catch (final UnsupportedEncodingException e) {
			throw new PasswordVerificationException(
					"Invalid password byte encoding specified. Likely a programming error or typo", e);
		}

		final String hashedPassword = String.format("%040x", new BigInteger(1, digest.digest()));

		final String hashedPasswordPrefix = hashedPassword.substring(0, 5);
		final String hashedPasswordSuffix = hashedPassword.substring(5);

		final String url = String.format("%s/range/%s", config.getBaseUrl(), hashedPasswordPrefix);
		final WebTarget target = httpClient.target(url);

		LOGGER.info("Sending GET to " + url);

		Response response;

		try {
			response = target.request().header("Add-Padding", "true").get();
		} catch (final ProcessingException e) {
			throw new PasswordVerificationException("Failed to make HTTP request to HIBP API", e);
		}

		if (response.getStatus() > 300) {
			throw new PasswordVerificationException(
					"Non-200 HTTP response returned from HIBP API: " + response.getStatus());
		}

		final String responseString = response.readEntity(String.class);

		LOGGER.info("Received raw response " + responseString);

		final String[] results = responseString.split("\\n");

		int finalResult = 0;
		for (final String currentResult : results) {
			final String[] resultParts = currentResult.split(":");

			final String resultHashSuffix = resultParts[0];
			final int resultCount = Integer.parseInt(resultParts[1].trim());

			if (hashedPasswordSuffix.toUpperCase().equals(resultHashSuffix.toUpperCase())) {
				finalResult = resultCount;
				break;
			}
		}

		return finalResult;
	}

}
