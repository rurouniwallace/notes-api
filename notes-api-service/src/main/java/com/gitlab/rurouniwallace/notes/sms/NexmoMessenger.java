package com.gitlab.rurouniwallace.notes.sms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gitlab.rurouniwallace.notes.config.NexmoConfiguration;
import com.gitlab.rurouniwallace.notes.exceptions.SmsTransmissionException;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.nexmo.client.HttpConfig;
import com.nexmo.client.NexmoClient;
import com.nexmo.client.sms.MessageStatus;
import com.nexmo.client.sms.SmsSubmissionResponse;
import com.nexmo.client.sms.SmsSubmissionResponseMessage;
import com.nexmo.client.sms.messages.TextMessage;

public class NexmoMessenger implements ISendsSms {

	private static final Logger LOGGER = LoggerFactory.getLogger(NexmoMessenger.class);

	private static final String RESILIENCE_KEY = "nexmo";

	private final NexmoConfiguration config;

	public NexmoMessenger(final NexmoConfiguration config) {
		this.config = config;
	}

	@Override
	public void sendSms(final String destination, final String message) throws SmsTransmissionException {

		// Nexmo is very finnicky about what phone number formats it will accept, so we
		// have to
		// finangle a bit
		final PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		final PhoneNumber phoneHandle;
		try {
			phoneHandle = phoneUtil.parse(destination, "US");
		} catch (final NumberParseException e) {
			throw new SmsTransmissionException("Invalid phone number format", e);
		}

		final String formattedDestination = phoneUtil.format(phoneHandle, PhoneNumberFormat.E164);

		LOGGER.debug(String.format("Formatted destination phone number %s", formattedDestination));

		final HttpConfig httpConfig = HttpConfig.builder().baseUri(config.getHostname()).build();

		final NexmoClient client = NexmoClient.builder().apiKey(config.getApiKey()).apiSecret(config.getApiSecret())
				.httpConfig(httpConfig).build();

		final SmsSubmissionResponse responses = client.getSmsClient()
				.submitMessage(new TextMessage(config.getSourcePhoneNumber(), formattedDestination, message));

		boolean error = false;
		for (final SmsSubmissionResponseMessage response : responses.getMessages()) {
			if (response.getStatus() != MessageStatus.OK) {
				LOGGER.error(String.format("Nexmo transmission error. Status: %s, message: %s",
						response.getStatus().toString(), response.getErrorText()));
				error = true;
			}
		}

		if (error) {
			throw new SmsTransmissionException("Nexmo transmission error");
		}
	}

	@Override
	public String getResilienceRegistryKey() {
		return RESILIENCE_KEY;
	}

}
