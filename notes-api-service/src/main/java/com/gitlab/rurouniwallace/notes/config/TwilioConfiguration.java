package com.gitlab.rurouniwallace.notes.config;

public class TwilioConfiguration {

	private String apiKey;

	private String apiSecret;

	private String accountSid;

	private String sourcePhoneNumber;

	private String hostname;

	public TwilioConfiguration() {
		// Empty constructor
	}

	public TwilioConfiguration(final String apiKey, final String apiSecret, final String accountSid,
			final String sourcePhoneNumber, final String hostname) {
		this.apiKey = apiKey;
		this.apiSecret = apiSecret;
		this.accountSid = accountSid;
		this.sourcePhoneNumber = sourcePhoneNumber;
		this.hostname = hostname;
	}

	/**
	 * @return the apiKey
	 */
	public String getApiKey() {
		return apiKey;
	}

	/**
	 * @param apiKey the apiKey to set
	 */
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	/**
	 * @return the apiSecret
	 */
	public String getApiSecret() {
		return apiSecret;
	}

	/**
	 * @param apiSecret the apiSecret to set
	 */
	public void setApiSecret(String apiSecret) {
		this.apiSecret = apiSecret;
	}

	/**
	 * @return the accountSid
	 */
	public String getAccountSid() {
		return accountSid;
	}

	/**
	 * @param accountSid the accountSid to set
	 */
	public void setAccountSid(String accountSid) {
		this.accountSid = accountSid;
	}

	/**
	 * @return the sourcePhoneNumber
	 */
	public String getSourcePhoneNumber() {
		return sourcePhoneNumber;
	}

	/**
	 * @param sourcePhoneNumber the sourcePhoneNumber to set
	 */
	public void setSourcePhoneNumber(String sourcePhoneNumber) {
		this.sourcePhoneNumber = sourcePhoneNumber;
	}

	/**
	 * @return the hostname
	 */
	public String getHostname() {
		return hostname;
	}

	/**
	 * @param hostname the hostname to set
	 */
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
}
