package com.gitlab.rurouniwallace.notes.config;

/**
 * Rate limiter configurations
 */
public class RateLimiterConfiguration {

	/**
	 * The default wait time a thread waits for a permission
	 */
	private Integer timeoutDuration;

	/**
	 * The period of a limit refresh
	 */
	private Integer limitRefreshPeriod;

	/**
	 * The number of permissions available during one limit refresh period
	 */
	private Integer limitForPeriod;

	/**
	 * @return the timeoutDuration
	 */
	public Integer getTimeoutDuration() {
		return timeoutDuration;
	}

	/**
	 * @param timeoutDuration the timeoutDuration to set
	 */
	public void setTimeoutDuration(Integer timeoutDuration) {
		this.timeoutDuration = timeoutDuration;
	}

	/**
	 * @return the limitRefreshPeriod
	 */
	public Integer getLimitRefreshPeriod() {
		return limitRefreshPeriod;
	}

	/**
	 * @param limitRefreshPeriod the limitRefreshPeriod to set
	 */
	public void setLimitRefreshPeriod(Integer limitRefreshPeriod) {
		this.limitRefreshPeriod = limitRefreshPeriod;
	}

	/**
	 * @return the limitForPeriod
	 */
	public Integer getLimitForPeriod() {
		return limitForPeriod;
	}

	/**
	 * @param limitForPeriod the limitForPeriod to set
	 */
	public void setLimitForPeriod(Integer limitForPeriod) {
		this.limitForPeriod = limitForPeriod;
	}
}
