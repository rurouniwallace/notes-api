package com.gitlab.rurouniwallace.notes.config;

/**
 * Resilience4j time limiter configurations
 */
public class TimeLimiterConfiguration {

	/**
	 * Timeout duration
	 */
	private Integer timeoutDuration;

	/**
	 * Whether or not to call cancel on the running future
	 */
	private Boolean cancelRunningFuture;

	/**
	 * @return the timeoutDuration
	 */
	public Integer getTimeoutDuration() {
		return timeoutDuration;
	}

	/**
	 * @param timeoutDuration the timeoutDuration to set
	 */
	public void setTimeoutDuration(Integer timeoutDuration) {
		this.timeoutDuration = timeoutDuration;
	}

	/**
	 * @return the cancelRunningFuture
	 */
	public Boolean getCancelRunningFuture() {
		return cancelRunningFuture;
	}

	/**
	 * @param cancelRunningFuture the cancelRunningFuture to set
	 */
	public void setCancelRunningFuture(Boolean cancelRunningFuture) {
		this.cancelRunningFuture = cancelRunningFuture;
	}
}
