package com.gitlab.rurouniwallace.notes.exceptions;

@SuppressWarnings("serial")
public class SmsTransmissionException extends Exception {

	/**
	 * Construct a new instance
	 */
	public SmsTransmissionException() {
		super();
	}

	/**
	 * Construct a new instance
	 */
	public SmsTransmissionException(final String message) {
		super(message);
	}

	/**
	 * Construct a new instance
	 */
	public SmsTransmissionException(final String message, final Exception cause) {
		super(message, cause);
	}
}
