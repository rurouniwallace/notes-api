package com.gitlab.rurouniwallace.notes.resilience;

public interface IEvaluatesRetryOnException {

	public boolean checkRetry(final Object result);
}
