package com.gitlab.rurouniwallace.notes.responses;

import com.gitlab.rurouniwallace.notes.models.Folder;

/**
 * Response to a folder operation
 */
public class FolderResponse extends StandardResponse {

	/**
	 * The folder in the response
	 */
	private Folder folder;

	/**
	 * Construct a new instance
	 */
	public FolderResponse() {
		// empty constructor
	}

	/**
	 * Construct a new instance
	 * 
	 * @param message the response message
	 * @param status the response status
	 */
	public FolderResponse(final String message, final StatusCode status) {
		this.message = message;
		this.status = status;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param status the response status
	 */
	public FolderResponse(final StatusCode status) {
		this.status = status;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param status the response status
	 * @param httpStatus HTTP status
	 */
	public FolderResponse(final StatusCode status, final int httpStatus) {
		this.status = status;
		this.httpStatus = httpStatus;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param httpStatus HTTP status
	 * @param message the response message
	 * @param status the response status
	 */
	public FolderResponse(final String message, final StatusCode status, final int httpStatus) {
		this.httpStatus = httpStatus;
		this.message = message;
		this.status = status;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param note the note for the response
	 * @param status the response status
	 */
	public FolderResponse(final Folder folder, final StatusCode status) {
		this.folder = folder;
		this.status = status;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param note the note for the response
	 * @param status the response status
	 * @param httpStatus the HTTP status
	 */
	public FolderResponse(final Folder folder, final StatusCode status, final int httpStatus) {
		this.folder = folder;
		this.status = status;
		this.httpStatus = httpStatus;
	}

	/**
	 * @return the folder
	 */
	public Folder getFolder() {
		return folder;
	}

	/**
	 * @param folder the folder to set
	 */
	public void setFolder(Folder folder) {
		this.folder = folder;
	}
}
