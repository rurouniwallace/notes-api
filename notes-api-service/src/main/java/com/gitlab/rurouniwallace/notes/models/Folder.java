package com.gitlab.rurouniwallace.notes.models;

import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A folder for notes
 */
public class Folder {

	/**
	 * The unique identifier
	 */
	private UUID uuid;

	/**
	 * The user who owns this folder
	 */
	private UUID user;

	/**
	 * The folder name
	 */
	private String name;

	/**
	 * Notes contained in the folder
	 */
	@JsonIgnore
	private Map<String, Note> notes;

	/**
	 * Construct a new instance
	 */
	public Folder() {
		// empty constructor
	}

	/**
	 * Construct a new instance
	 * 
	 * @param uuid the unique identifier
	 * @param user the user who owns this folder
	 * @param name the name of the folder
	 */
	public Folder(final UUID uuid, final UUID user, final String name) {
		this.uuid = uuid;
		this.user = user;
		this.name = name;
	}

	public Folder(final Folder other) {
		this.uuid = other.uuid;
		this.user = other.user;
		this.name = other.name;
	}

	/**
	 * @return the uuid
	 */
	public UUID getUuid() {
		return uuid;
	}

	/**
	 * @param uuid the uuid to set
	 */
	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	/**
	 * @return the user
	 */
	public UUID getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(UUID user) {
		this.user = user;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Check against another instance for equality
	 * 
	 * @return true if equal, false otherwise
	 */
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Folder)) {
			return false;
		}

		return Objects.deepEquals(((Folder) other).uuid, this.uuid)
				&& Objects.deepEquals(((Folder) other).user, this.user)
				&& Objects.deepEquals(((Folder) other).name, this.name);
	}

	/**
	 * Write as a string
	 * 
	 * @return instance as a string
	 */
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		final String newLine = System.getProperty("line.separator");

		result.append("Folder {" + newLine);
		result.append("\tuuid: " + uuid + newLine);
		result.append("\tuser: " + user + newLine);
		result.append("\tname: " + name + newLine);
		result.append("}");

		return result.toString();
	}
}
