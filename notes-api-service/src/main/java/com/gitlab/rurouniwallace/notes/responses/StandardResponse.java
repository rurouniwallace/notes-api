package com.gitlab.rurouniwallace.notes.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Base response model for the API
 */
@JsonInclude(Include.NON_NULL)
public class StandardResponse extends HttpResponse {

	/**
	 * Response message
	 */
	@JsonProperty
	protected String message;

	/**
	 * Response status code
	 */
	@JsonProperty
	protected StatusCode status;

	/**
	 * Construct a new instance
	 */
	public StandardResponse() {
		// empty constructor
	}

	/**
	 * Construct a new instance
	 * 
	 * @param message response message
	 * @param status response status
	 */
	public StandardResponse(final String message, final StatusCode status) {
		this.message = message;
		this.status = status;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param message response message
	 * @param status response status
	 * @param httpStatus HTTP status
	 */
	public StandardResponse(final String message, final StatusCode status, final int httpStatus) {
		this.message = message;
		this.status = status;
		this.httpStatus = httpStatus;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param status response status
	 * @param httpStatus HTTP status
	 */
	public StandardResponse(final StatusCode status, final int httpStatus) {
		this.status = status;
		this.httpStatus = httpStatus;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param status HTTP status
	 */
	public StandardResponse(final StatusCode status) {
		this.status = status;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the status
	 */
	public StatusCode getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(StatusCode status) {
		this.status = status;
	}

	@Override
	public boolean equals(final Object other) {

		return true;
		/*
		 * if (!(other instanceof StandardResponse)) { return false; }
		 * 
		 * return Objects.deepEquals(((StandardResponse) other).status, this.status) &&
		 * Objects.deepEquals(((StandardResponse) other).httpStatus, this.httpStatus);
		 */
	}
}
