package com.gitlab.rurouniwallace.notes.controllers;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gitlab.rurouniwallace.notes.config.EmailVerificationConfiguration;
import com.gitlab.rurouniwallace.notes.config.SessionConfiguration;
import com.gitlab.rurouniwallace.notes.dao.IAccessesUsers;
import com.gitlab.rurouniwallace.notes.exceptions.AuthenticationDeniedException;
import com.gitlab.rurouniwallace.notes.exceptions.EntityNotFoundException;
import com.gitlab.rurouniwallace.notes.exceptions.UnexpectedStateException;
import com.gitlab.rurouniwallace.notes.models.User;
import com.gitlab.rurouniwallace.notes.requests.AuthenticationRequest;
import com.gitlab.rurouniwallace.notes.requests.EmailVerificationRequest;
import com.gitlab.rurouniwallace.notes.requests.VerifyOtpRequest;
import com.gitlab.rurouniwallace.notes.resilience.ResilienceRegistry;
import com.gitlab.rurouniwallace.notes.responses.StandardResponse;
import com.gitlab.rurouniwallace.notes.responses.StatusCode;
import com.gitlab.rurouniwallace.notes.responses.UserResponse;
import com.gitlab.rurouniwallace.notes.sms.ISendsSms;

import dev.samstevens.totp.code.CodeGenerator;
import dev.samstevens.totp.code.DefaultCodeGenerator;
import dev.samstevens.totp.code.HashingAlgorithm;
import dev.samstevens.totp.exceptions.CodeGenerationException;
import dev.samstevens.totp.time.SystemTimeProvider;
import dev.samstevens.totp.time.TimeProvider;
import io.github.resilience4j.bulkhead.ThreadPoolBulkhead;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.decorators.Decorators;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.vavr.CheckedFunction0;
import io.vavr.CheckedFunction1;

public class AuthController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);

	/**
	 * User data access interface
	 */
	private final IAccessesUsers userDao;

	/**
	 * Session configuration
	 */
	private final SessionConfiguration sessionConfig;

	private final EmailVerificationConfiguration emailVerificationConfig;

	private final ISendsSms smsMessenger;

	/**
	 * Construct a new instance
	 * 
	 * @param userDao the user data access interface
	 */
	public AuthController(final IAccessesUsers userDao, final ISendsSms smsMessenger,
			final SessionConfiguration sessionConfig, final EmailVerificationConfiguration emailVerificationConfig,
			final ResilienceRegistry resilienceRegistry) {
		super(resilienceRegistry);
		this.userDao = userDao;
		this.sessionConfig = sessionConfig;
		this.emailVerificationConfig = emailVerificationConfig;
		this.smsMessenger = smsMessenger;
	}

	/**
	 * Authenticate a user
	 * 
	 * @param authnRequest the authentication request
	 * @param response the authentication response
	 */
	public void authenticateUser(final AuthenticationRequest authnRequest, final AsyncResponse response) {
		if (authnRequest.getEmail() == null || authnRequest.getEmail().isEmpty()) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"Email address required"));
		}

		if (authnRequest.getPassword() == null || authnRequest.getPassword().isEmpty()) {
			response.resume(buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY_422, StatusCode.INVALID_ARGUMENTS,
					"Password required"));
		}

		final String resilienceKey = userDao.getResilienceRegistryKey();

		executeAuthAsync(resilienceKey, response, () -> {
			User user;
			try {
				user = userDao.authenticateUser(authnRequest.getEmail(), authnRequest.getPassword());
			} catch (final AuthenticationDeniedException e) {
				return new UserResponse("Authentication failed", StatusCode.DENY, HttpStatus.FORBIDDEN_403);
			}

			if (user.getTwoFactorAuthPreference() != 0) {
				final User userToReturn = new User(user.getUuid(), user.getTwoFactorAuthPreference());

				return new UserResponse(userToReturn, StatusCode.TFA_REQUIRED);
			}

			return new UserResponse(user, StatusCode.ALLOW);
		});
	}

	public void verifyOtp(final VerifyOtpRequest request, final Cookie sessionCookie, final AsyncResponse response) {
		final String resilienceKey = userDao.getResilienceRegistryKey();

		checkAuth(sessionCookie, sessionConfig, request.getUserId(), response,
				Arrays.asList(AUTH_STATUS_AUTHENTICATED, AUTH_STATUS_TFA_REQUIRED));

		respondAsync(resilienceKey, response, () -> {
			final StandardResponse denyResponse = new UserResponse("Invalid OTP code", StatusCode.DENY,
					HttpStatus.FORBIDDEN_403);

			final boolean valid = userDao.checkOtp(request.getCode(), request.getUserId());

			if (!valid) {
				return denyResponse;
			}

			User user;

			try {
				user = userDao.lookupUser(request.getUserId());
			} catch (final EntityNotFoundException e) {
				LOGGER.info("Attempted to verify OTP code for a user ID that does not exist " + request.getUserId());
				return denyResponse;
			}

			return new UserResponse(user, StatusCode.ALLOW);
		});
	}

	public void sendSmsOtpVerification(final UUID userId, final Cookie sessionCookie, final AsyncResponse response) {
		final String resilienceKey = userDao.getResilienceRegistryKey();

		checkAuth(sessionCookie, sessionConfig, userId, response,
				Arrays.asList(AUTH_STATUS_AUTHENTICATED, AUTH_STATUS_TFA_REQUIRED));

		// user must be looked up first, then the text message sent, so we'll
		// join both threads into one

		final CheckedFunction0<User> userSupplier = () -> {
			return userDao.lookupUser(userId);
		};

		final CompletableFuture<User> futureUser = buildCompletableFuture(resilienceKey, userSupplier, (exception) -> {
			final StandardResponse responseBody;
			if (exception instanceof EntityNotFoundException) {
				LOGGER.error("User not found for sending OTP verification via SMS", exception);
				responseBody = new StandardResponse("User not found", StatusCode.ENTITY_NOT_FOUND,
						HttpStatus.NOT_FOUND_404);
			} else {
				responseBody = handleException(exception);
			}
			response.resume(responseBody);
		});

		final CheckedFunction1<User, StandardResponse> smsSenderThread = (user) -> {
			if (user == null) {
				throw new UnexpectedStateException("No user returned by user lookup worker thread");
			}

			if (StringUtils.isBlank(user.getOtpSecret())) {
				return new StandardResponse("User OTP secret not configured", StatusCode.INVALID_STATE,
						HttpStatus.CONFLICT_409);
			}

			if (StringUtils.isBlank(user.getPhone())) {
				return new StandardResponse("User phone number not set", StatusCode.INVALID_STATE,
						HttpStatus.CONFLICT_409);
			}

			if (StringUtils.isBlank(user.getOtpAlgorithm())) {
				return new StandardResponse("User OTP algorithm not set", StatusCode.INVALID_STATE,
						HttpStatus.CONFLICT_409);
			}

			try {
				HashingAlgorithm.valueOf(user.getOtpAlgorithm());
			} catch (final IllegalArgumentException e) {
				return new StandardResponse("User OTP algorithm invalid", StatusCode.INVALID_STATE,
						HttpStatus.CONFLICT_409);
			}

			if (user.getOtpDigits() <= 0) {
				return new StandardResponse("User OTP digits not set", StatusCode.INVALID_STATE,
						HttpStatus.CONFLICT_409);
			}

			if (user.getOtpStep() <= 0) {
				return new StandardResponse("User OTP time interval not set", StatusCode.INVALID_STATE,
						HttpStatus.CONFLICT_409);
			}

			final String code = generateOtpCode(user);

			smsMessenger.sendSms(user.getPhone(), "Your temporary 2FA code is " + code);

			return new StandardResponse("2FA SMS sent successfully", StatusCode.SUCCESS, HttpStatus.OK_200);
		};

		futureUser.thenAcceptAsync(user -> {
			final String smsResilienceKey = smsMessenger.getResilienceRegistryKey();

			final CompletableFuture<StandardResponse> smsTransmission = buildCompletableFuture(smsResilienceKey,
					smsSenderThread, user, (throwable) -> {
						response.resume(handleException(throwable));
					});

			smsTransmission.thenAcceptAsync((result) -> {
				LOGGER.info("SMS transmission complete");
				response.resume(result);
			});
			LOGGER.debug("SMS transmission thread started");
		});
		LOGGER.debug("User lookup thread started");
	}

	public void verifyEmail(final EmailVerificationRequest request, final AsyncResponse response) {

		final String resilienceKey = userDao.getResilienceRegistryKey();

		respondAsync(resilienceKey, response, () -> {
			final String token = request.getToken();
			final String email = request.getEmail();
			final UUID userId = request.getUserId();

			final StandardResponse denyResponse = new StandardResponse("Verification token rejected", StatusCode.DENY,
					HttpStatus.FORBIDDEN_403);

			Claims claims;
			try {
				claims = Jwts.parser().setSigningKey(emailVerificationConfig.getKey()).parseClaimsJws(token).getBody();
			} catch (final JwtException e) {
				LOGGER.info("Email verification failed. Token invalid", e);
				return denyResponse;
			}

			if (!claims.get("type").equals(EMAIL_VERIFICATION_TOKEN_TYPE)) {
				LOGGER.info("Email verification failed. Attempted to use non-verification token");
				return denyResponse;
			}

			if (!claims.getSubject().equals(userId.toString())) {
				LOGGER.info("Email verification failed. User ID address doesn't match token");
				return denyResponse;
			}

			if (!claims.get("email").equals(email)) {
				LOGGER.info("Email verification failed. Email address doesn't match token");
				return denyResponse;
			}

			User user;
			try {
				user = userDao.lookupUser(userId);
			} catch (final EntityNotFoundException e) {
				LOGGER.info("Attempted to verify email for a user ID that does not exist " + userId);
				return denyResponse;
			}

			if (!user.getEmail().equals(email)) {
				return new StandardResponse("User email has changed. Please re-do email verification", StatusCode.DENY,
						HttpStatus.CONFLICT_409);
			}

			final User updatePayload = new User();
			updatePayload.setEmailVerified(true);

			userDao.updateUser(userId, updatePayload);

			return new StandardResponse(StatusCode.SUCCESS);
		});
	}

	/**
	 * Build authentication token claims
	 * 
	 * @param user the user to authenticate
	 * @param authInstant the instant of authentication
	 * @return claims
	 */
	private Map<String, Object> buildAuthTokenClaims(final User user, final Date authInstant, final int ttl,
			final String authStatus) {
		final Map<String, Object> claims = new HashMap<>();

		LOGGER.info("User for claims: " + user);

		final Date expiration = new Date(authInstant.getTime() + ttl * 1000);

		claims.put("sub", user.getUuid());
		claims.put("iat", authInstant.getTime() / 1000);
		claims.put("nbf", authInstant.getTime() / 1000);
		claims.put("exp", expiration.getTime() / 1000);
		claims.put("jti", UUID.randomUUID().toString());
		claims.put("status", authStatus);

		return claims;
	}

	private void buildAuthCookie(final ResponseBuilder builder, final UserResponse response) {
		final User user = response.getUser();
		final Date authInstant = new Date();
		if (response.getStatus() == StatusCode.ALLOW) {
			final String sessionToken = Jwts.builder()
					.addClaims(
							buildAuthTokenClaims(user, authInstant, sessionConfig.getTtl(), AUTH_STATUS_AUTHENTICATED))
					.signWith(SignatureAlgorithm.forName(sessionConfig.getAlg()), sessionConfig.getKey()).compact();

			final NewCookie sessionCookie = new NewCookie("session", sessionToken, sessionConfig.getPath(),
					sessionConfig.getDomain(), sessionConfig.getComment(), sessionConfig.getTtl(),
					sessionConfig.isSecure());

			builder.cookie(sessionCookie);
		} else if (response.getStatus() == StatusCode.TFA_REQUIRED) {

			final String sessionToken = Jwts.builder()
					.addClaims(buildAuthTokenClaims(user, authInstant, sessionConfig.getTfaTtl(),
							AUTH_STATUS_TFA_REQUIRED))
					.signWith(SignatureAlgorithm.forName(sessionConfig.getAlg()), sessionConfig.getKey()).compact();

			final NewCookie sessionCookie = new NewCookie("session", sessionToken, sessionConfig.getPath(),
					sessionConfig.getDomain(), sessionConfig.getComment(), sessionConfig.getTtl(),
					sessionConfig.isSecure());

			builder.cookie(sessionCookie);
		}
	}

	/**
	 * Execute a function asynchronously, with a configured resilience4j circuit
	 * breaker and a threadpool bulkhead. The result will be an HTTP response with
	 * an auth cookie.
	 * 
	 * @param resilienceKey key to resilience configs to use
	 * @param response asynchronous response
	 * @param supplier lambda function to execute
	 */
	protected void executeAuthAsync(final String resilienceKey, final AsyncResponse response,
			final CheckedFunction0<UserResponse> supplier) {

		final CircuitBreaker circuitBreaker = getCircuitBreaker(resilienceKey);

		final ThreadPoolBulkhead threadPoolBulkhead = getThreadPoolBulkhead(resilienceKey);

		final CheckedFunction0<UserResponse> supplierWithCircuitBreaker = Decorators.ofCheckedSupplier(supplier)
				.withCircuitBreaker(circuitBreaker).decorate();

		final CompletableFuture<UserResponse> async = Decorators.ofSupplier(() -> {
			try {
				return supplierWithCircuitBreaker.apply();
			} catch (Throwable e) {
				LOGGER.error("Health check failed", e);
				return new UserResponse(StatusCode.INTERNAL_SERVER_ERROR);
			}
		}).withThreadPoolBulkhead(threadPoolBulkhead).get().toCompletableFuture();

		async.thenAccept(result -> {
			LOGGER.info(String.format("Returning value: %s", result.getHttpStatus()));

			final ResponseBuilder builder = Response.status(result.getHttpStatus()).entity(result);

			buildAuthCookie(builder, result);

			response.resume(builder.build());
		});
	}

	private String generateOtpCode(final User user) throws CodeGenerationException {
		LOGGER.debug("User for OTP " + user);
		final CodeGenerator codeGenerator = new DefaultCodeGenerator(HashingAlgorithm.valueOf(user.getOtpAlgorithm()),
				user.getOtpDigits());

		final TimeProvider timeProvider = new SystemTimeProvider();

		final long counter = Math.floorDiv(timeProvider.getTime(), user.getOtpStep());

		return codeGenerator.generate(user.getOtpSecret(), counter);
	}
}
