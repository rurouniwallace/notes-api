package com.gitlab.rurouniwallace.notes.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gitlab.rurouniwallace.notes.models.User;

/**
 * Response to a user request
 */
@JsonInclude(Include.NON_NULL)
public class UserResponse extends StandardResponse {

	/**
	 * Construct a new instance
	 */
	public UserResponse() {
		// empty constructor
	}

	/**
	 * Construct a new instance
	 * 
	 * @param message the response message
	 * @param status the response status
	 */
	public UserResponse(final String message, final StatusCode status) {
		this.message = message;
		this.status = status;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param httpStatus HTTP status
	 * @param message the response message
	 * @param status the response status
	 */
	public UserResponse(final String message, final StatusCode status, final int httpStatus) {
		this.httpStatus = httpStatus;
		this.message = message;
		this.status = status;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param user the user for the response
	 * @param status the response status
	 */
	public UserResponse(final User user, final StatusCode status) {
		this.user = user;
		this.status = status;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param httpStatus HTTP status
	 * @param user the user for the response
	 * @param status the response status
	 */
	public UserResponse(final User user, final StatusCode status, final int httpStatus) {
		this.httpStatus = httpStatus;
		this.user = user;
		this.status = status;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param status the response status
	 */
	public UserResponse(final StatusCode status) {
		this.status = status;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param status the response status
	 * @param httpStatus HTTP status
	 */
	public UserResponse(final StatusCode status, final int httpStatus) {
		this.status = status;
		this.httpStatus = httpStatus;
	}

	/**
	 * User to provide in response
	 */
	@JsonProperty
	private User user;

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

}
