package com.gitlab.rurouniwallace.notes.exceptions;

/**
 * A bad parameter was passed
 */
@SuppressWarnings("serial")
public class BadParameterException extends Exception {

	/**
	 * Construct a new instance
	 */
	public BadParameterException() {
		super();
	}

	/**
	 * Construct a new instance
	 */
	public BadParameterException(final String message) {
		super(message);
	}

	/**
	 * Construct a new instance
	 */
	public BadParameterException(final String message, final Exception cause) {
		super(message, cause);
	}
}
