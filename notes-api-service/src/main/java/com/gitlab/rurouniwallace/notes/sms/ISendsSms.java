package com.gitlab.rurouniwallace.notes.sms;

import com.gitlab.rurouniwallace.notes.exceptions.SmsTransmissionException;
import com.gitlab.rurouniwallace.notes.resilience.IResilient;

public interface ISendsSms extends IResilient {

	public void sendSms(final String destination, final String message) throws SmsTransmissionException;
}
