package com.gitlab.rurouniwallace.notes.dao;

import java.util.UUID;

import com.gitlab.rurouniwallace.notes.exceptions.AuthenticationException;
import com.gitlab.rurouniwallace.notes.exceptions.DataAccessException;
import com.gitlab.rurouniwallace.notes.models.User;
import com.gitlab.rurouniwallace.notes.resilience.IResilient;

/**
 * Data access object that accesses user data
 */
public interface IAccessesUsers extends IResilient {

	/**
	 * Authenticate a user with their email and password
	 * 
	 * @param email user email address
	 * @param password user password
	 * @return the user data
	 * @throws DataAccessException error occurred accessing database
	 * @throws AuthenticationException authentication failed
	 */
	public User authenticateUser(final String email, final String password)
			throws DataAccessException, AuthenticationException;

	/**
	 * Check user's OTP
	 * 
	 * @param otp the OTP to check
	 * @param userUuid ID of the user we're checking
	 * @return true if OTP is valid, false if not
	 */
	public boolean checkOtp(final String otp, final UUID userUuid) throws DataAccessException;

	/**
	 * Registers a new user
	 * 
	 * @param user the user to register
	 * @return the created user
	 * @throws DataAccessException error occurred accessing datasource
	 */
	public User registerUser(final User user) throws DataAccessException;

	/**
	 * Update a user
	 * 
	 * @param userUuid the ID of the user to update
	 * @param updatePayload the update to make to the user
	 * @return the updated user
	 * @throws DataAccessException error occurred updating the user in datasource
	 */
	public User updateUser(final UUID userUuid, final User updatePayload) throws DataAccessException;

	/**
	 * Look up a user by ID
	 * 
	 * @param userId the ID of the user to look up
	 * @return the user looked up
	 * @throws DataAccessException
	 */
	public User lookupUser(final UUID userId) throws DataAccessException;

	boolean isOtpAlgorithmValid(final String algorithm);
}
