package com.gitlab.rurouniwallace.notes.responses;

import com.gitlab.rurouniwallace.notes.models.Note;

/**
 * Response to an API note operation
 */
public class NoteResponse extends StandardResponse {

	/**
	 * Note in the response
	 */
	private Note note;

	/**
	 * Construct a new instance
	 */
	public NoteResponse() {
		// empty constructor
	}

	/**
	 * Construct a new instance
	 * 
	 * @param message the response message
	 * @param status the response status
	 */
	public NoteResponse(final String message, final StatusCode status) {
		this.message = message;
		this.status = status;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param status the response status
	 */
	public NoteResponse(final StatusCode status) {
		this.status = status;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param status the response status
	 * @param httpStatus HTTP status
	 */
	public NoteResponse(final StatusCode status, final int httpStatus) {
		this.status = status;
		this.httpStatus = httpStatus;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param httpStatus HTTP status
	 * @param message the response message
	 * @param status the response status
	 */
	public NoteResponse(final String message, final StatusCode status, final int httpStatus) {
		this.httpStatus = httpStatus;
		this.message = message;
		this.status = status;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param note the note for the response
	 * @param status the response status
	 */
	public NoteResponse(final Note note, final StatusCode status) {
		this.note = note;
		this.status = status;
	}

	/**
	 * Construct a new instance
	 * 
	 * @param note the note for the response
	 * @param status the response status
	 * @param httpStatus the HTTP status
	 */
	public NoteResponse(final Note note, final StatusCode status, final int httpStatus) {
		this.note = note;
		this.status = status;
		this.httpStatus = httpStatus;
	}

	/**
	 * @return the note
	 */
	public Note getNote() {
		return note;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(Note note) {
		this.note = note;
	}
}
