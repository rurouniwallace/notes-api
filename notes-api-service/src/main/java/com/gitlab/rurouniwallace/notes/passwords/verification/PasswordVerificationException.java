package com.gitlab.rurouniwallace.notes.passwords.verification;

@SuppressWarnings("serial")
public class PasswordVerificationException extends Exception {

	public PasswordVerificationException() {
		super();
	}

	public PasswordVerificationException(final String message) {
		super(message);
	}

	public PasswordVerificationException(final String message, final Exception cause) {
		super(message, cause);
	}
}
