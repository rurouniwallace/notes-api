package com.gitlab.rurouniwallace.notes.passwords.verification;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import javax.ws.rs.client.ClientBuilder;

import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.gitlab.rurouniwallace.notes.config.HIBPConfiguration;

public class HIBPConnectorTest {

	private static HIBPConfiguration config;

	private static WireMockServer wireMockServer;

	private static final String TEST_PASSWORD = "test1234";

	private static final int NUM_COMPROMISED_SITES = 54917;

	private HIBPConnector connector;

	@BeforeAll
	public static void setUpBeforeClass() {
		WireMock.configureFor("localhost", 8090);

		wireMockServer = new WireMockServer(wireMockConfig().port(8090));
		wireMockServer.start();

		config = new HIBPConfiguration("http://localhost:8090/passwords", "");
	}

	@BeforeEach
	public void setUp() {
		wireMockServer.resetAll();

		connector = new HIBPConnector(config, ClientBuilder.newClient());
	}

	private void stubPasswordCheckSuccess() {
		stubFor(get(urlPathMatching("/passwords/range/[a-zA-Z0-9]+"))
				.willReturn(aResponse().withHeader("Content-Type", "text/plain").withStatus(HttpStatus.OK_200)
						.withBody(fixture("fixtures/downstreamResponses/hibp/success.txt"))));
	}

	private void stubPasswordCheckBadGateway() {
		stubFor(get(urlPathMatching("/passwords/range/[a-zA-Z0-9]+")).willReturn(
				aResponse().withHeader("Content-Type", "text/plain").withStatus(HttpStatus.SERVICE_UNAVAILABLE_503)));
	}

	private void stubPasswordCheckTooManyRequests() {
		stubFor(get(urlPathMatching("/passwords/range/[a-zA-Z0-9]+")).willReturn(
				aResponse().withHeader("Content-Type", "text/plain").withStatus(HttpStatus.TOO_MANY_REQUESTS_429)));
	}

	@Test
	public void checkPasswordCompromised_PassesSha1HashPrefix() throws PasswordVerificationException {
		stubPasswordCheckSuccess();

		connector.isCompromised(TEST_PASSWORD);

		verify(getRequestedFor(urlPathMatching("/passwords/range/(?i)9bc34")));
	}

	@Test
	public void checkPasswordCompromised_PassesPaddingHeader() throws PasswordVerificationException {
		stubPasswordCheckSuccess();

		connector.isCompromised(TEST_PASSWORD);

		verify(getRequestedFor(urlPathMatching("/passwords/range/\\w+")).withHeader("Add-Padding",
				matching("(?i)true")));
	}

	@Test
	public void checkPasswordCompromised_Success_ReturnsNumberOfCompromisedSites()
			throws PasswordVerificationException {
		stubPasswordCheckSuccess();

		final int result = connector.isCompromised(TEST_PASSWORD);

		assertEquals(NUM_COMPROMISED_SITES, result);
	}

	@Test
	public void checkPasswordCompromised_BadGateway_ThrowsException() {
		stubPasswordCheckBadGateway();

		assertThrows(PasswordVerificationException.class, () -> {
			connector.isCompromised(TEST_PASSWORD);
		});
	}

	@Test
	public void checkPasswordCompromised_TooManyRequests_ThrowsException() {
		stubPasswordCheckTooManyRequests();

		assertThrows(PasswordVerificationException.class, () -> {
			connector.isCompromised(TEST_PASSWORD);
		});
	}

	@AfterAll
	public static void tearDownAfterClass() {
		wireMockServer.stop();
	}
}
