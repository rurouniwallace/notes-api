package com.gitlab.rurouniwallace.notes.dao.sql;

import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.bouncycastle.crypto.generators.BCrypt;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.LoggerFactory;

import com.gitlab.rurouniwallace.notes.config.SecurityConfiguration;
import com.gitlab.rurouniwallace.notes.models.Folder;
import com.gitlab.rurouniwallace.notes.models.Note;
import com.gitlab.rurouniwallace.notes.models.User;

import dev.samstevens.totp.code.CodeGenerator;
import dev.samstevens.totp.code.CodeVerifier;
import dev.samstevens.totp.code.DefaultCodeGenerator;
import dev.samstevens.totp.time.SystemTimeProvider;
import dev.samstevens.totp.time.TimeProvider;
import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;

/**
 * Base test class for the SQL data access layer
 */
public abstract class BaseSqlDaoTest {

	/**
	 * JDBC URL
	 */
	protected static final String MOCK_CONNECTION_URL = "jdbc:hsqldb:mem:myDb;sql.sql.syntax_pgs=true";

	/**
	 * Database connection source
	 */
	protected static BasicDataSource dataSource;

	/**
	 * Security configuration
	 */
	protected SecurityConfiguration securityConfig;

	protected CodeGenerator codeGenerator = new DefaultCodeGenerator();

	protected CodeVerifier otpCodeVerifier;

	/**
	 * Set up database and initialize the schema using Liquibase
	 * 
	 * @throws SQLException an SQL error occurred
	 * @throws LiquibaseException Liquibase error occurred
	 */
	@BeforeAll
	public static void setUpBeforeClass() throws SQLException, LiquibaseException {

		dataSource = new BasicDataSource();
		dataSource.setUrl(MOCK_CONNECTION_URL);

		final Connection connection = dataSource.getConnection();

		final Database database = DatabaseFactory.getInstance()
				.findCorrectDatabaseImplementation(new JdbcConnection(connection));

		final Liquibase liquibase = new Liquibase("liquibase/changelog.xml", new ClassLoaderResourceAccessor(),
				database);
		liquibase.update(new Contexts(), new LabelExpression());
		liquibase.close();
	}

	/**
	 * Initialize configurations
	 */
	@BeforeEach
	public void setUp() {
		securityConfig = new SecurityConfiguration();
		securityConfig.setHashCost(5);

		final TimeProvider timeProvider = new SystemTimeProvider();
	}

	/**
	 * Hash a password using the Blowfish cipher, random salt, and a configured cost
	 * factor
	 * 
	 * @param password the password to hash
	 * @return the hashed value, with the cost factor and salt appended
	 * @throws GeneralSecurityException if generating the hash fails
	 */
	protected String hashPassword(final String password) throws GeneralSecurityException {

		final byte[] salt = new byte[16];

		SecureRandom.getInstanceStrong().nextBytes(salt);

		final int cost = securityConfig.getHashCost();

		final String hash = Base64.getEncoder()
				.encodeToString(BCrypt.generate(password.getBytes(), salt, securityConfig.getHashCost()));

		return String.format("$%s$%s$%s", cost, Base64.getEncoder().encodeToString(salt), hash);
	}

	/**
	 * Insert a user into the database
	 * 
	 * @param user user to insert
	 * @throws SQLException an SQL error occurred
	 */
	protected static void insertUserIntoDatabase(final User user) throws SQLException {
		final Connection connection = dataSource.getConnection();

		final PreparedStatement insertUserStatement = connection.prepareStatement(
				"INSERT INTO Users (uuid, email, password, phone, givenName, surname, emailVerified, twoFactorAuthPreference, otpSecret, otpAlgorithm, otpStep, otpDigits) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

		// we use setObject for all Integer fields so that JDBC handles null values
		// properly
		insertUserStatement.setObject(1, user.getUuid());
		insertUserStatement.setString(2, user.getEmail());
		insertUserStatement.setString(3, user.getPassword());
		insertUserStatement.setString(4, user.getPhone());
		insertUserStatement.setString(5, user.getGivenName());
		insertUserStatement.setString(6, user.getSurname());
		insertUserStatement.setBoolean(7, (user.isEmailVerified() != null) ? user.isEmailVerified() : false);
		insertUserStatement.setObject(8, user.getTwoFactorAuthPreference());
		insertUserStatement.setString(9, user.getOtpSecret());
		insertUserStatement.setString(10, user.getOtpAlgorithm());
		insertUserStatement.setObject(11, user.getOtpStep());
		insertUserStatement.setObject(12, user.getOtpDigits());

		insertUserStatement.executeUpdate();

		DbUtils.closeQuietly(connection);
		DbUtils.closeQuietly(insertUserStatement);
	}

	/**
	 * Insert a folder into the database
	 * 
	 * @param folder folder to insert
	 * @throws SQLException an SQL error occurred
	 */
	protected static void insertFolderIntoDatabase(final Folder folder) throws SQLException {
		final Connection connection = dataSource.getConnection();

		final PreparedStatement insertFolderStatement = connection
				.prepareStatement("INSERT INTO Folders (uuid, userId, name) VALUES (?, ?, ?)");
		insertFolderStatement.setObject(1, folder.getUuid());
		insertFolderStatement.setObject(2, folder.getUser());
		insertFolderStatement.setString(3, folder.getName());

		insertFolderStatement.executeUpdate();

		DbUtils.closeQuietly(connection);
		DbUtils.closeQuietly(insertFolderStatement);
	}

	/**
	 * Insert a note into the database
	 * 
	 * @param note note to insert
	 * @throws SQLException an SQL error occurred
	 */
	protected static void insertNoteIntoDatabase(final Note note) throws SQLException {
		final Connection connection = dataSource.getConnection();

		final PreparedStatement insertNoteStatement = connection.prepareStatement(
				"INSERT INTO Notes (uuid, folderId, userId, title, body, created, lastUpdated) VALUES (?, ?, ?, ?, ?, ?, ?)");
		insertNoteStatement.setObject(1, note.getUuid());
		insertNoteStatement.setObject(2, note.getFolder());
		insertNoteStatement.setObject(3, note.getUser());
		insertNoteStatement.setString(4, note.getTitle());
		insertNoteStatement.setString(5, note.getBody());
		insertNoteStatement.setTimestamp(6,
				(note.getCreated() != null) ? new Timestamp(note.getCreated().getTime()) : null);
		insertNoteStatement.setTimestamp(7,
				(note.getCreated() != null) ? new Timestamp(note.getLastUpdated().getTime()) : null);

		final int rowsUpdated = insertNoteStatement.executeUpdate();

		LoggerFactory.getLogger(BaseSqlDaoTest.class).info("Note to insert: " + note);

		LoggerFactory.getLogger(BaseSqlDaoTest.class)
				.info("Note rows updated: " + rowsUpdated + " user: " + note.getUser());

		DbUtils.closeQuietly(connection);
		DbUtils.closeQuietly(insertNoteStatement);
	}

	/**
	 * Retrieve a user from the database
	 * 
	 * @param uuid ID of the user we're retrieving
	 * @return the user from the database
	 * @throws SQLException an SQL error occurred
	 */
	protected User getUserFromDatabase(final UUID uuid) throws SQLException {
		final Connection connection = dataSource.getConnection();

		final PreparedStatement getUserStatement = connection.prepareStatement("SELECT * FROM Users WHERE uuid = ?");
		getUserStatement.setObject(1, uuid);

		final ResultSet queryResults = getUserStatement.executeQuery();

		if (!queryResults.next()) {
			return null;
		}

		final User userFromDatabase = new User();

		userFromDatabase.setUuid((UUID) queryResults.getObject("uuid"));
		userFromDatabase.setEmail(queryResults.getString("email"));
		userFromDatabase.setPassword(queryResults.getString("password"));
		userFromDatabase.setPhone(queryResults.getString("phone"));
		userFromDatabase.setGivenName(queryResults.getString("givenName"));
		userFromDatabase.setSurname(queryResults.getString("surname"));
		userFromDatabase.setEmailVerified(queryResults.getBoolean("emailVerified"));
		userFromDatabase.setTwoFactorAuthPreference(queryResults.getInt("twoFactorAuthPreference"));
		userFromDatabase.setOtpSecret(queryResults.getString("otpSecret"));
		userFromDatabase.setOtpAlgorithm(queryResults.getString("otpAlgorithm"));
		userFromDatabase.setOtpStep(queryResults.getInt("otpStep"));
		userFromDatabase.setOtpDigits(queryResults.getInt("otpDigits"));

		DbUtils.closeQuietly(connection, getUserStatement, queryResults);

		return userFromDatabase;
	}

	/**
	 * Retrieve a folder from the database
	 * 
	 * @param uuid ID of the folder we're retrieving
	 * @return the folder from the database
	 * @throws SQLException an SQL error occurred
	 */
	protected Folder getFolderFromDatabase(final UUID uuid) throws SQLException {
		final Connection connection = dataSource.getConnection();

		final PreparedStatement getFolderStatement = connection
				.prepareStatement("SELECT * FROM Folders WHERE uuid = ?");
		getFolderStatement.setObject(1, uuid);

		final ResultSet queryResults = getFolderStatement.executeQuery();

		if (!queryResults.next()) {
			return null;
		}

		final Folder folder = new Folder();

		folder.setUuid((UUID) queryResults.getObject("uuid"));
		folder.setUser((UUID) queryResults.getObject("userId"));
		folder.setName(queryResults.getString("name"));

		DbUtils.closeQuietly(connection, getFolderStatement, queryResults);

		return folder;
	}

	/**
	 * Retrieve a note from the database
	 * 
	 * @param uuid ID of the note we're retrieving
	 * @return the folder from the database
	 * @throws SQLException an SQL error occurred
	 */
	protected Note getNoteFromDatabase(final UUID uuid) throws SQLException {
		final Connection connection = dataSource.getConnection();

		final PreparedStatement getNoteStatement = connection.prepareStatement("SELECT * FROM Notes WHERE uuid = ?");
		getNoteStatement.setObject(1, uuid);

		final ResultSet queryResults = getNoteStatement.executeQuery();

		if (!queryResults.next()) {
			return null;
		}

		final Note noteFromDatabase = new Note();

		noteFromDatabase.setUuid((UUID) queryResults.getObject("uuid"));
		noteFromDatabase.setFolder((UUID) queryResults.getObject("folderId"));
		noteFromDatabase.setUser((UUID) queryResults.getObject("userId"));
		noteFromDatabase.setTitle(queryResults.getString("title"));
		noteFromDatabase.setBody(queryResults.getString("body"));
		noteFromDatabase.setCreated(new Date(queryResults.getTimestamp("created").getTime()));
		noteFromDatabase.setLastUpdated(new Date(queryResults.getTimestamp("lastUpdated").getTime()));

		DbUtils.closeQuietly(connection);
		DbUtils.closeQuietly(getNoteStatement);
		DbUtils.closeQuietly(queryResults);

		return noteFromDatabase;
	}

	protected static String generateRandomEmail() {
		return RandomStringUtils.randomAlphabetic(16).toLowerCase() + "@example.com";
	}
}
