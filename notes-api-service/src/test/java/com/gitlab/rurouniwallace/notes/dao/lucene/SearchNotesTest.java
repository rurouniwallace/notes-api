package com.gitlab.rurouniwallace.notes.dao.lucene;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Stream;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.gitlab.rurouniwallace.notes.dao.LuceneDao;
import com.gitlab.rurouniwallace.notes.exceptions.DataAccessException;
import com.gitlab.rurouniwallace.notes.models.Note;
import com.gitlab.rurouniwallace.notes.models.User;

public class SearchNotesTest {

	private static User user1;

	private static User user2;

	private static LuceneDao dao;

	private static Directory index;

	private static Analyzer analyzer;

	private static IndexWriter writer;

	private static Note salmonRecipeNote;

	private static Note marinatedChickenNote;

	private static Note favoriteFoodsNote;

	private static Note myNote;

	private static Note pcBuildNote;

	@BeforeAll
	public static void setUpBeforeClass() throws IOException {
		user1 = new User(UUID.randomUUID(), "searchNotes@example.com", "test1234", "(716)555-5555", "Test", "User");

		user2 = new User(UUID.randomUUID(), "rurouniwallace@gmail.com", "abcd1234", "(716)444-4444s", "Glenn",
				"Wallace");

		salmonRecipeNote = new Note(UUID.randomUUID(), null, user1.getUuid(), "Salmon Recipe",
				"One half pound of salmon. One tablespoon of lemon juice. One tablespoon of butter. One teaspoon of garlic cloves");

		marinatedChickenNote = new Note(UUID.randomUUID(), null, user1.getUuid(), "Marinaded Chicken Recipe",
				"Use this recipe for fileted chicken. Two chicken breast filets. Three tablespoons of soy sauce. One teaspoon of garlic cloves. One tablespoon of lime juice. One tablespoon of honey.");

		favoriteFoodsNote = new Note(UUID.randomUUID(), null, user1.getUuid(), "My Favorite Foods",
				"Chicken. Salmon. Turkey. Brown rice. Kale.");

		myNote = new Note(UUID.randomUUID(), null, user1.getUuid(), "My Note",
				"This is my note. There are many like it but this one is mine.");

		pcBuildNote = new Note(UUID.randomUUID(), null, user1.getUuid(), "PC Build",
				"One Core i7 CPU. GeForce 1080-Ti. 16 GB of RAM. 2 TB hard drive.");

		analyzer = new StandardAnalyzer();
		index = new RAMDirectory();

		final IndexWriterConfig config = new IndexWriterConfig(analyzer);

		writer = new IndexWriter(index, config);

		insertNoteIntoIndex(index, user1.getUuid(), salmonRecipeNote, analyzer);
		insertNoteIntoIndex(index, user1.getUuid(), marinatedChickenNote, analyzer);
		insertNoteIntoIndex(index, user2.getUuid(), favoriteFoodsNote, analyzer);
		insertNoteIntoIndex(index, user1.getUuid(), myNote, analyzer);
		insertNoteIntoIndex(index, user1.getUuid(), pcBuildNote, analyzer);

		dao = new LuceneDao(index, analyzer, writer);
	}

	@ParameterizedTest
	@MethodSource("provideSearchTerms")
	public void searchNotes_MultipleSearchTerms_ReturnsExpectedNotes(final Set<String> searchTerms,
			final List<Note> expectedResults) throws DataAccessException {
		final List<Note> searchResults = dao.searchNotes(user1.getUuid(), searchTerms, 5);

		// we don't care about the order of results
		assertEquals(new HashSet<>(expectedResults), new HashSet<>(searchResults));
	}

	@Test
	public void searchNotes_IndexEmpty_ReturnsEmptyResultsList() throws DataAccessException {
		final Analyzer analyzer = new StandardAnalyzer();
		index = new RAMDirectory();

		final LuceneDao dao = new LuceneDao(index, analyzer, writer);

		final List<Note> searchResults = dao.searchNotes(user1.getUuid(), new HashSet<String>(Arrays.asList("stuff")),
				5);

		assertEquals(0, searchResults.size(), "There should be 0 search results when querying an empty search index");
	}

	private static void insertNoteIntoIndex(final Directory index, final UUID userId, final Note note,
			final Analyzer analyzer) throws IOException {
		final IndexWriterConfig config = new IndexWriterConfig(analyzer);

		final Document doc = new Document();
		doc.add(new TextField("title", note.getTitle(), Field.Store.YES));
		doc.add(new TextField("body", note.getBody(), Field.Store.YES));
		doc.add(new StringField("uuid", note.getUuid().toString(), Field.Store.YES));
		doc.add(new StringField("userId", userId.toString(), Field.Store.YES));

		if (note.getCreated() != null) {
			doc.add(new StoredField("created", note.getCreated().getTime()));
		}

		if (note.getLastUpdated() != null) {
			doc.add(new StoredField("lastUpdated", note.getLastUpdated().getTime()));
		}

		writer.addDocument(doc);
		writer.commit();
	}

	private static Stream<Arguments> provideSearchTerms() {
		final List<Arguments> arguments = new ArrayList<>();

		arguments.add(Arguments.of(new HashSet<String>(Arrays.asList("recipe")),
				Arrays.asList(salmonRecipeNote, marinatedChickenNote)));
		arguments.add(Arguments.of(new HashSet<String>(Arrays.asList("recipe", "chicken")),
				Arrays.asList(marinatedChickenNote)));
		arguments.add(Arguments.of(new HashSet<String>(Arrays.asList("soy")), Arrays.asList(marinatedChickenNote)));
		arguments.add(Arguments.of(new HashSet<String>(Arrays.asList("mine")), Arrays.asList(myNote)));
		arguments.add(Arguments.of(new HashSet<String>(Arrays.asList("tablespoon", "garlic", "chicken")),
				Arrays.asList(marinatedChickenNote)));
		arguments.add(Arguments.of(new HashSet<String>(Arrays.asList("drive")), Arrays.asList(pcBuildNote)));
		arguments.add(
				Arguments.of(new HashSet<String>(Arrays.asList("this")), Arrays.asList(myNote, marinatedChickenNote)));

		return arguments.stream();
	}
}
