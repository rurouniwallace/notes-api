package com.gitlab.rurouniwallace.notes.dao.sql;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import org.bouncycastle.crypto.generators.BCrypt;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.gitlab.rurouniwallace.notes.dao.SqlDao;
import com.gitlab.rurouniwallace.notes.exceptions.AuthenticationDeniedException;
import com.gitlab.rurouniwallace.notes.exceptions.AuthenticationException;
import com.gitlab.rurouniwallace.notes.exceptions.DataAccessException;
import com.gitlab.rurouniwallace.notes.exceptions.EntityAlreadyExistsException;
import com.gitlab.rurouniwallace.notes.exceptions.EntityNotFoundException;
import com.gitlab.rurouniwallace.notes.models.User;

import dev.samstevens.totp.code.CodeGenerator;
import dev.samstevens.totp.code.DefaultCodeGenerator;
import dev.samstevens.totp.code.HashingAlgorithm;
import dev.samstevens.totp.exceptions.CodeGenerationException;
import dev.samstevens.totp.secret.DefaultSecretGenerator;
import dev.samstevens.totp.secret.SecretGenerator;
import dev.samstevens.totp.time.SystemTimeProvider;
import dev.samstevens.totp.time.TimeProvider;

public class UserAccessTest extends BaseSqlDaoTest {

	@Test
	public void registerUser_InsertSuccess_InsertedUserMatchesOriginal() throws DataAccessException, SQLException {
		final SqlDao dao = new SqlDao(dataSource, securityConfig);

		final SecretGenerator otpSecretGenerator = new DefaultSecretGenerator(64);
		final String otpSecret = otpSecretGenerator.generate();

		final User userToRegister = new User(generateRandomEmail(), "test1234", "(716)888-8888", "Testy", "Testerson",
				false, 0, otpSecret, HashingAlgorithm.SHA256.toString(), 30, 6);

		final User createdUser = dao.registerUser(userToRegister);

		final User userInDatabase = getUserFromDatabase(createdUser.getUuid());

		// we want to ignore the UUID. In another test case we'll check to verify that a
		// UUID was
		// generated
		userToRegister.setUuid(null);
		userInDatabase.setUuid(null);

		// ignore the password. In another test case we'll check that it was hashed
		// properly
		userToRegister.setPassword(null);
		userInDatabase.setPassword(null);

		assertEquals(userToRegister, userInDatabase,
				"User pulled from database after registration doesn't match what was expected");
	}

	@Test
	public void registerUser_InsertSuccess_InsertedUserEmailNotVerifiedByDefault()
			throws DataAccessException, SQLException {
		final SqlDao dao = new SqlDao(dataSource, securityConfig);

		final User userToRegister = new User(generateRandomEmail(), "test1234", "(716)888-8888", "Testy", "Testerson");

		final User createdUser = dao.registerUser(userToRegister);

		final User userInDatabase = getUserFromDatabase(createdUser.getUuid());

		assertFalse(userInDatabase.isEmailVerified(),
				"User email must be unverified by default upon being inserted into database");
	}

	@Test
	public void registerUser_InsertSuccess_PasswordhashedProperly() throws DataAccessException, SQLException {
		final SqlDao dao = new SqlDao(dataSource, securityConfig);

		final String password = "test1234";
		final User userToRegister = new User(generateRandomEmail(), password, "(716)888-8888", "Testy", "Testerson");

		final User createdUser = dao.registerUser(userToRegister);

		final User userInDatabase = getUserFromDatabase(createdUser.getUuid());

		final String hashedPassword = userInDatabase.getPassword();

		final String[] parts = hashedPassword.split("\\$");

		final int costFactor = Integer.parseInt(parts[1]);
		final byte[] salt = Base64.getDecoder().decode(parts[2]);

		final String expectedHash = Base64.getEncoder()
				.encodeToString(BCrypt.generate(password.getBytes(), salt, costFactor));
		final String hashFromDatabase = parts[3];

		assertEquals(expectedHash, hashFromDatabase, "Hash in database doesn't match what was expected");
	}

	@Test
	public void registerUser_UserEmailAlreadyExists_ThrowException() throws DataAccessException, SQLException {
		final SqlDao dao = new SqlDao(dataSource, securityConfig);

		final String email = generateRandomEmail();

		insertUserIntoDatabase(new User(UUID.randomUUID(), email, "(716)888-8888", "test1234", "Testy", "Testerson"));

		assertThrows(EntityAlreadyExistsException.class, () -> {
			dao.registerUser(new User(email, "(716)854-2020", "abcd1234", "Another", "Tester"));
		});
	}

	@Test
	public void authenticateUser_EmailAndPasswordCorrect_ReturnUser()
			throws SQLException, DataAccessException, AuthenticationException, GeneralSecurityException {
		final SqlDao dao = new SqlDao(dataSource, securityConfig);

		final String email = generateRandomEmail();
		final String password = "test1234";

		final User user = new User(UUID.randomUUID(), email, hashPassword(password), "(716)888-8888", "Testy",
				"Testerson", false);
		insertUserIntoDatabase(user);

		final User userFromDao = dao.authenticateUser(email, password);

		assertEquals(user, userFromDao);
	}

	@Test
	public void authenticateUser_EmailAddressNotFound_ThrowAuthenticationDeniedException() {
		final SqlDao dao = new SqlDao(dataSource, securityConfig);

		assertThrows(AuthenticationDeniedException.class, () -> {
			dao.authenticateUser("nonexistent@example.com", "test1234");
		});
	}

	@Test
	public void authenticateUser_PasswordIncorrect_ThrowAuthenticationDeniedException()
			throws GeneralSecurityException, SQLException, DataAccessException, AuthenticationException {
		final SqlDao dao = new SqlDao(dataSource, securityConfig);

		final String email = generateRandomEmail();
		final String password = "test1234";

		final User user = new User(UUID.randomUUID(), email, hashPassword(password), "(716)888-8888", "Testy",
				"Testerson");
		insertUserIntoDatabase(user);

		assertThrows(AuthenticationDeniedException.class, () -> {
			dao.authenticateUser(email, "different password");
		});
	}

	@Test
	public void checkOtp_OtpInvalid_ReturnFalse()
			throws GeneralSecurityException, SQLException, DataAccessException, AuthenticationException {
		final SqlDao dao = new SqlDao(dataSource, securityConfig);

		final String email = generateRandomEmail();
		final String password = "test1234";

		final SecretGenerator otpSecretGenerator = new DefaultSecretGenerator(64);
		final String otpSecret = otpSecretGenerator.generate();

		final User user = new User(UUID.randomUUID(), email, hashPassword(password), "(716)888-8888", "Testy",
				"Testerson");

		user.setTwoFactorAuthPreference(1);
		user.setOtpSecret(otpSecret);
		user.setOtpAlgorithm(HashingAlgorithm.SHA1.toString());

		insertUserIntoDatabase(user);

		// there's a 1 in 999,999 chance that this will be true when it should be false
		assertFalse(dao.checkOtp("864692", user.getUuid()));
	}

	@Test
	public void checkOtp_UserNotFound_ThrowException()
			throws GeneralSecurityException, SQLException, DataAccessException, AuthenticationException {
		final SqlDao dao = new SqlDao(dataSource, securityConfig);

		final String email = generateRandomEmail();
		final String password = "test1234";

		final SecretGenerator otpSecretGenerator = new DefaultSecretGenerator(64);
		final String otpSecret = otpSecretGenerator.generate();

		final User user = new User(UUID.randomUUID(), email, hashPassword(password), "(716)888-8888", "Testy",
				"Testerson");

		user.setTwoFactorAuthPreference(1);
		user.setOtpSecret(otpSecret);
		user.setOtpAlgorithm(HashingAlgorithm.SHA1.toString());

		insertUserIntoDatabase(user);

		assertThrows(EntityNotFoundException.class, () -> {
			dao.checkOtp("864692", UUID.randomUUID());
		});
	}

	@ParameterizedTest
	@MethodSource("provideOtpConfigurations")
	public void checkOtp_OtpValid_ReturnTrue(final HashingAlgorithm algorithm, final int step, final int digits)
			throws GeneralSecurityException, SQLException, DataAccessException, AuthenticationException,
			CodeGenerationException {
		final SqlDao dao = new SqlDao(dataSource, securityConfig);

		final String email = generateRandomEmail();
		final String password = "test1234";

		final SecretGenerator otpSecretGenerator = new DefaultSecretGenerator(64);
		final String otpSecret = otpSecretGenerator.generate();

		final User user = new User(UUID.randomUUID(), email, hashPassword(password), "(716)888-8888", "Testy",
				"Testerson");

		final TimeProvider timeProvider = new SystemTimeProvider();

		user.setTwoFactorAuthPreference(1);
		user.setOtpSecret(otpSecret);
		user.setOtpAlgorithm(algorithm.toString());
		user.setOtpStep(step);
		user.setOtpDigits(digits);

		insertUserIntoDatabase(user);

		final CodeGenerator codeGenerator = new DefaultCodeGenerator(algorithm, digits);

		// this is a little sketchy. We're assuming the 0th bucket will be the correct
		// bucket, but technically if there's a latency of over 30 seconds between
		// generating this code and the DAO checking it, the bucket will have
		// incremented and the test will fail. We'll assume for the sake of simplicity
		// (and sanity) that there won't be a 30 second delay between them

		final long counter = Math.floorDiv(timeProvider.getTime(), user.getOtpStep());

		final String code = codeGenerator.generate(otpSecret, counter);

		assertTrue(dao.checkOtp(code, user.getUuid()));
	}

	private static Stream<Arguments> provideOtpConfigurations() {
		final List<Arguments> arguments = new ArrayList<>();

		// the standard configuration
		arguments.add(Arguments.of(HashingAlgorithm.SHA1, 30, 6));

		// a little bit more secure
		arguments.add(Arguments.of(HashingAlgorithm.SHA256, 10, 7));

		// all the way to the extreme
		arguments.add(Arguments.of(HashingAlgorithm.SHA512, 1, 20));

		return arguments.stream();
	}
}
