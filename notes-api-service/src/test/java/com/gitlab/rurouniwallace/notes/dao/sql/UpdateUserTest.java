package com.gitlab.rurouniwallace.notes.dao.sql;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import org.bouncycastle.crypto.generators.BCrypt;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.LoggerFactory;

import com.gitlab.rurouniwallace.notes.dao.SqlDao;
import com.gitlab.rurouniwallace.notes.exceptions.DataAccessException;
import com.gitlab.rurouniwallace.notes.exceptions.EntityNotFoundException;
import com.gitlab.rurouniwallace.notes.models.User;

import dev.samstevens.totp.code.HashingAlgorithm;
import dev.samstevens.totp.secret.DefaultSecretGenerator;
import dev.samstevens.totp.secret.SecretGenerator;

public class UpdateUserTest extends BaseSqlDaoTest {

	private User userUnderTest;

	private SqlDao dao;

	@BeforeEach
	public void setUp() {
		super.setUp();

		dao = new SqlDao(dataSource, securityConfig);

		userUnderTest = new User(UUID.randomUUID(), generateRandomEmail(), "test1234", "(716)555-5555", "Test", "User",
				false);
	}

	@ParameterizedTest
	@MethodSource("provideUserUpdates")
	public void updateUser_UpdateIndividualField_FieldSuccessfullyUpdated(final String fieldToUpdate,
			final Object updatedValue) throws SQLException, DataAccessException, GeneralSecurityException {

		insertUserIntoDatabase(userUnderTest);

		dao.updateUser(userUnderTest.getUuid(), createUpdatePayload(fieldToUpdate, updatedValue));

		final User expectedUpdatedUser = updateField(new User(userUnderTest), fieldToUpdate, updatedValue);

		final User userFromDatabase = getUserFromDatabase(userUnderTest.getUuid());

		assertEquals(expectedUpdatedUser, userFromDatabase);
	}

	// @Test
	public void updateUser_UpdatePassword_CheckPasswordHashedCorrectly()
			throws SQLException, DataAccessException, GeneralSecurityException {
		insertUserIntoDatabase(userUnderTest);

		final String password = "test5678";

		dao.updateUser(userUnderTest.getUuid(), createUpdatePayload("password", password));

		final User userFromDatabase = getUserFromDatabase(userUnderTest.getUuid());

		LoggerFactory.getLogger(UpdateUserTest.class)
				.info("password = " + password + ", hashed = " + userFromDatabase.getPassword());

		assertTrue(checkHash(password, userFromDatabase.getPassword()));
	}

	@ParameterizedTest
	@MethodSource("provideUserUpdates")
	public void updateUser_UpdateIndividualField_UpdatedUserReturned(final String fieldToUpdate,
			final Object updatedValue) throws SQLException, DataAccessException, GeneralSecurityException {
		insertUserIntoDatabase(userUnderTest);

		dao.updateUser(userUnderTest.getUuid(), createUpdatePayload(fieldToUpdate, updatedValue));

		final User expectedUpdatedUser = updateField(new User(userUnderTest), fieldToUpdate, updatedValue);

		final User userFromDao = getUserFromDatabase(userUnderTest.getUuid());

		assertEquals(expectedUpdatedUser, userFromDao);
	}

	@Test
	public void updateUser_AttemptToUpdateId_ThrowException() throws SQLException, DataAccessException {
		insertUserIntoDatabase(userUnderTest);

		assertThrows(DataAccessException.class, () -> {
			dao.updateUser(userUnderTest.getUuid(), createUpdatePayload("uuid", UUID.randomUUID()));
		});
	}

	@Test
	public void updateUser_UserIdNotFound_ThrowException() throws SQLException {
		insertUserIntoDatabase(userUnderTest);

		assertThrows(EntityNotFoundException.class, () -> {
			dao.updateUser(UUID.randomUUID(), createUpdatePayload("email", generateRandomEmail()));
		});
	}

	private User createUpdatePayload(final String fieldToUpdate, final Object updatedValue)
			throws GeneralSecurityException {
		return updateField(new User(), fieldToUpdate, updatedValue);
	}

	private User updateField(final User user, final String fieldToUpdate, final Object updatedValue)
			throws GeneralSecurityException {
		switch (fieldToUpdate) {
		case "uuid":
			user.setUuid((UUID) updatedValue);
			break;
		case "email":
			user.setEmail((String) updatedValue);
			break;
		case "password":
			user.setPassword(hashPassword((String) updatedValue));
			break;
		case "givenName":
			user.setGivenName((String) updatedValue);
			break;
		case "surname":
			user.setSurname((String) updatedValue);
			break;
		case "phone":
			user.setPhone((String) updatedValue);
			break;
		case "emailVerified":
			user.setEmailVerified((Boolean) updatedValue);
			break;
		case "twoFactorAuthPreference":
			user.setTwoFactorAuthPreference((Integer) updatedValue);
			break;
		case "otpSecret":
			user.setOtpSecret((String) updatedValue);
			break;
		case "otpAlgorithm":
			user.setOtpAlgorithm((String) updatedValue);
			break;
		case "otpStep":
			user.setOtpStep((Integer) updatedValue);
			break;
		case "otpDigits":
			user.setOtpDigits((Integer) updatedValue);
			break;
		}
		return user;
	}

	private static Stream<Arguments> provideUserUpdates() {
		final List<Arguments> arguments = new ArrayList<>();

		arguments.add(Arguments.of("email", generateRandomEmail()));
		// arguments.add(Arguments.of("password", "test5678"));
		arguments.add(Arguments.of("givenName", "Mynewname"));
		arguments.add(Arguments.of("surname", "Ijustgotmarried"));
		arguments.add(Arguments.of("phone", "(716)534-9698"));
		arguments.add(Arguments.of("emailVerified", true));
		arguments.add(Arguments.of("twoFactorAuthPreference", 1));
		arguments.add(Arguments.of("otpStep", 30));
		arguments.add(Arguments.of("otpDigits", 6));

		final SecretGenerator otpSecretGenerator = new DefaultSecretGenerator(64);
		final String otpSecret = otpSecretGenerator.generate();
		arguments.add(Arguments.of("otpSecret", otpSecret));
		arguments.add(Arguments.of("otpAlgorithm", HashingAlgorithm.SHA256.toString()));

		return arguments.stream();
	}

	protected boolean checkHash(final String password, final String hashed) {
		final String[] parts = hashed.split("\\$");

		final int cost = Integer.parseInt(parts[1]);

		final byte[] salt = Base64.getDecoder().decode(parts[2]);

		final byte[] hashedCheckPassword = BCrypt.generate(password.getBytes(), salt, cost);

		LoggerFactory.getLogger(UpdateUserTest.class)
				.info("Checking hash: " + Base64.getEncoder().encodeToString(hashedCheckPassword));

		return parts[3].equals(Base64.getEncoder().encodeToString(hashedCheckPassword));
	}
}
