package com.gitlab.rurouniwallace.notes.dao.sql;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.gitlab.rurouniwallace.notes.dao.SqlDao;
import com.gitlab.rurouniwallace.notes.exceptions.DataAccessException;
import com.gitlab.rurouniwallace.notes.models.Note;
import com.gitlab.rurouniwallace.notes.models.User;

import liquibase.exception.LiquibaseException;

public class SearchNotesTest extends BaseSqlDaoTest {

	private static User user;

	private static Note salmonRecipeNote;

	private static Note marinatedChickenNote;

	private static Note myNote;

	private static Note pcBuildNote;

	private SqlDao dao;

	@BeforeAll
	public static void setUpBeforeClass() throws SQLException, LiquibaseException {
		BaseSqlDaoTest.setUpBeforeClass();

		user = new User(UUID.randomUUID(), "searchNotes@example.com", "test1234", "(716)555-5555", "Test", "User");

		salmonRecipeNote = new Note(UUID.randomUUID(), null, user.getUuid(), "Salmon Recipe",
				"One half pound of salmon. One tablespoon of lemon juice. One tablespoon of butter. One teaspoon of garlic cloves");

		marinatedChickenNote = new Note(UUID.randomUUID(), null, user.getUuid(), "Marinaded Chicken Recipe",
				"Two chicken breast filets. Three tablespoons of soy sauce. One teaspoon of garlic cloves. One tablespoon of lime juice. One tablespoon of honey.");

		myNote = new Note(UUID.randomUUID(), null, user.getUuid(), "My Note",
				"This is my note. There are many like it but this one is mine.");

		pcBuildNote = new Note(UUID.randomUUID(), null, user.getUuid(), "PC Build",
				"One Core i7 CPU. GeForce 1080-Ti. 16 GB of RAM. 2 TB hard drive.");

		insertUserIntoDatabase(user);

		insertNoteIntoDatabase(salmonRecipeNote);
		insertNoteIntoDatabase(marinatedChickenNote);
		insertNoteIntoDatabase(myNote);
		insertNoteIntoDatabase(pcBuildNote);
	}

	@BeforeEach
	public void setUp() {
		super.setUp();

		dao = new SqlDao(dataSource, securityConfig);
	}

	@ParameterizedTest
	@MethodSource("provideSearchTerms")
	@Disabled
	public void searchNote_MultipleSearchTerms_ReturnsExpectedNotes(final List<String> searchTerms,
			final List<Note> expectedResults) throws DataAccessException {
		final List<Note> notes = dao.searchNotes(user.getUuid(), searchTerms);

		final List<Note> expectedNotes = Arrays.asList(marinatedChickenNote, salmonRecipeNote);

		// we don't care about the ordering of the elements found
		assertEquals(new HashSet<Note>(expectedResults), new HashSet<Note>(notes));
	}

	@Test
	@Disabled
	public void searchNote_UserIdNotFound_ThrowException() throws DataAccessException {
		assertEquals(new ArrayList<Note>(), dao.searchNotes(UUID.randomUUID(), new ArrayList<String>()));
	}

	public static Stream<Arguments> provideSearchTerms() {
		final List<Arguments> arguments = new ArrayList<>();

		arguments.add(Arguments.of(Arrays.asList("Recipe"), Arrays.asList(salmonRecipeNote, marinatedChickenNote)));
		arguments.add(Arguments.of(Arrays.asList("soy"), Arrays.asList(marinatedChickenNote)));
		arguments.add(Arguments.of(Arrays.asList("mine"), Arrays.asList(myNote)));
		arguments.add(
				Arguments.of(Arrays.asList("tablespoon", "garlic", "chicken"), Arrays.asList(marinatedChickenNote)));
		arguments.add(Arguments.of(Arrays.asList("drive"), Arrays.asList(pcBuildNote)));

		// if no search terms are provided, all notes for the user should be returned
		arguments.add(Arguments.of(new ArrayList<String>(),
				Arrays.asList(salmonRecipeNote, marinatedChickenNote, myNote, pcBuildNote)));

		return arguments.stream();
	}
}
