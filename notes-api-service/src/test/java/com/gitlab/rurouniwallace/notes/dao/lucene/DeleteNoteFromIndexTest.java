package com.gitlab.rurouniwallace.notes.dao.lucene;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.util.UUID;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.gitlab.rurouniwallace.notes.dao.LuceneDao;
import com.gitlab.rurouniwallace.notes.exceptions.DataAccessException;
import com.gitlab.rurouniwallace.notes.exceptions.EntityNotFoundException;
import com.gitlab.rurouniwallace.notes.models.Note;
import com.gitlab.rurouniwallace.notes.models.User;

public class DeleteNoteFromIndexTest {

	private static User user;

	private static User otherUser;

	private static Note otherUsersNote;

	private static LuceneDao dao;

	private static Directory index;

	private static Analyzer analyzer;

	private static Note note;

	private static IndexWriter writer;

	@BeforeEach
	public void setUp() throws IOException {
		user = new User(UUID.randomUUID(), "deleteNote@example.com", "test1234", "(716)777-7777", "Test", "User");

		analyzer = new StandardAnalyzer();
		index = new RAMDirectory();

		final IndexWriterConfig config = new IndexWriterConfig(analyzer);
		writer = new IndexWriter(index, config);

		dao = new LuceneDao(index, analyzer, writer);

		note = new Note(UUID.randomUUID(), UUID.randomUUID(), user.getUuid(), "Goodbye",
				"So say goodbye and hit the road\n" + "Pack it up and disappear\n"
						+ "You better have some place to go\n" + "'Cause you can't come back around here");

		insertNoteIntoIndex(index, analyzer, user.getUuid(), note);

		otherUser = new User(UUID.randomUUID(), "deleteNote@example.com", "test1234", "(716)777-7777", "Other", "Dude");

		otherUsersNote = new Note(UUID.randomUUID(), UUID.randomUUID(), user.getUuid(), "Total Stranger",
				"I'm someone else. I am irrelevant to this test");
	}

	@Test
	public void deleteNoteFromIndex_NoteSuccessfullyRemoved() throws IOException, DataAccessException {
		dao.deleteIndexedNote(user.getUuid(), note.getUuid());

		final IndexReader reader = DirectoryReader.open(index);
		final IndexSearcher searcher = new IndexSearcher(reader);

		final Query query = new TermQuery(new Term("uuid", note.getUuid().toString()));

		final TopDocs docs = searcher.search(query, 1);

		final ScoreDoc[] hits = docs.scoreDocs;

		assertEquals(0, hits.length, "Note was not successfully removed from index");
	}

	@Test
	public void deleteNoteFromIndex_UserIdDoesntExist_ThrowException() {
		assertThrows(EntityNotFoundException.class, () -> {
			dao.deleteIndexedNote(UUID.randomUUID(), note.getUuid());
		});
	}

	@Test
	public void deleteNoteFromIndex_NoteIdDoesntExist_ThrowException() {
		assertThrows(EntityNotFoundException.class, () -> {
			dao.deleteIndexedNote(user.getUuid(), UUID.randomUUID());
		});
	}

	@Test
	public void deleteNoteFromIndex_AttemptToUpdateAnotherUsersNotes_ThrowException() {
		assertThrows(EntityNotFoundException.class, () -> {
			dao.deleteIndexedNote(user.getUuid(), otherUsersNote.getUuid());
		});
	}

	private static void insertNoteIntoIndex(final Directory index, final Analyzer analyzer, final UUID userId,
			final Note note) throws IOException {
		final IndexWriterConfig config = new IndexWriterConfig(analyzer);

		final Document doc = new Document();
		doc.add(new TextField("title", note.getTitle(), Field.Store.YES));
		doc.add(new TextField("body", note.getBody(), Field.Store.YES));
		doc.add(new StringField("uuid", note.getUuid().toString(), Field.Store.YES));
		doc.add(new StringField("userId", userId.toString(), Field.Store.YES));

		if (note.getFolder() != null) {
			doc.add(new StringField("folderId", note.getFolder().toString(), Field.Store.YES));
		}

		if (note.getCreated() != null) {
			doc.add(new StoredField("created", note.getCreated().getTime()));
		}

		if (note.getLastUpdated() != null) {
			doc.add(new StoredField("lastUpdated", note.getLastUpdated().getTime()));
		}

		writer.addDocument(doc);
		writer.commit();
	}
}
