package com.gitlab.rurouniwallace.notes.dao.sql;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.sql.SQLException;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.gitlab.rurouniwallace.notes.dao.SqlDao;
import com.gitlab.rurouniwallace.notes.exceptions.DataAccessException;
import com.gitlab.rurouniwallace.notes.exceptions.EntityNotFoundException;
import com.gitlab.rurouniwallace.notes.models.User;

import dev.samstevens.totp.code.HashingAlgorithm;
import dev.samstevens.totp.secret.DefaultSecretGenerator;
import dev.samstevens.totp.secret.SecretGenerator;

public class LookpUserTest extends BaseSqlDaoTest {

	private SqlDao dao;

	@BeforeEach
	public void setUp() {
		super.setUp();

		dao = new SqlDao(dataSource, securityConfig);
	}

	@Test
	public void lookupUser_UserReturned() throws SQLException, DataAccessException {
		final UUID userId = UUID.randomUUID();
		final User userUnderTest = new User(userId, generateRandomEmail(), "test1234", "(716)555-5555", "Test", "User",
				true);

		final SecretGenerator otpSecretGenerator = new DefaultSecretGenerator(64);
		final String otpSecret = otpSecretGenerator.generate();

		userUnderTest.setOtpSecret(otpSecret);
		userUnderTest.setTwoFactorAuthPreference(1);
		userUnderTest.setOtpAlgorithm(HashingAlgorithm.SHA256.toString());
		userUnderTest.setOtpStep(30);
		userUnderTest.setOtpDigits(6);

		insertUserIntoDatabase(userUnderTest);

		final User userFromDao = dao.lookupUser(userId);

		assertEquals(userUnderTest, userFromDao);
	}

	@Test
	public void lookupUser_UserNotFound_ThrowException() throws SQLException {

		// throw in an extraneous user and make sure it's not returned
		final User extraneousUser = new User(UUID.randomUUID(), generateRandomEmail(), "test1234", "(716)555-5555",
				"Test", "User", true);

		insertUserIntoDatabase(extraneousUser);

		assertThrows(EntityNotFoundException.class, () -> {
			dao.lookupUser(UUID.randomUUID());
		});
	}

	@Test
	public void lookupUser_AllConnectionsClosed() throws SQLException, DataAccessException {
		final UUID userId = UUID.randomUUID();

		final User userToInsert = new User(userId, generateRandomEmail(), "test1234", "(716)555-5555", "Test", "User",
				true);

		insertUserIntoDatabase(userToInsert);

		dao.lookupUser(userId);

		assertEquals(0, dataSource.getNumActive());
	}
}