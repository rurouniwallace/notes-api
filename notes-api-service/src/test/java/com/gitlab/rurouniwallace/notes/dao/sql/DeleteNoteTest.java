package com.gitlab.rurouniwallace.notes.dao.sql;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.sql.SQLException;
import java.util.Date;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.gitlab.rurouniwallace.notes.dao.SqlDao;
import com.gitlab.rurouniwallace.notes.exceptions.DataAccessException;
import com.gitlab.rurouniwallace.notes.exceptions.EntityNotFoundException;
import com.gitlab.rurouniwallace.notes.models.Folder;
import com.gitlab.rurouniwallace.notes.models.Note;
import com.gitlab.rurouniwallace.notes.models.User;

/**
 * Test cases for deleting a note
 */
public class DeleteNoteTest extends BaseSqlDaoTest {

	private SqlDao dao;

	@BeforeEach
	public void setUp() {
		super.setUp();
		dao = new SqlDao(dataSource, securityConfig);
	}

	/**
	 * Test that when a note is deleted, the note is no longer in the database
	 * 
	 * @throws SQLException if an SQL error occurs accessing the database
	 * @throws DataAccessException if a data access error occurs in the DAO
	 */
	@Test
	public void deleteNote_DeleteNoteSuccessful_NoteNoLongerInDatabase() throws SQLException, DataAccessException {
		final UUID userUuid = UUID.randomUUID();
		final User userToCreate = new User(userUuid, "deletiontest1@example.com");
		insertUserIntoDatabase(userToCreate);

		final UUID folderUuid = UUID.randomUUID();
		final Folder folderToCreate = new Folder(folderUuid, userUuid, "My Folder");
		insertFolderIntoDatabase(folderToCreate);

		final UUID noteUuid = UUID.randomUUID();
		final Note noteToCreate = new Note(noteUuid, folderUuid, userUuid, "Cool Note",
				"To be or not to be, that is the question", new Date(), new Date());
		insertNoteIntoDatabase(noteToCreate);

		dao.deleteNote(userUuid, noteUuid);

		final Note noteFromDatabase = getNoteFromDatabase(noteUuid);

		assertNull(noteFromDatabase);
	}

	/**
	 * Test that when the specfied note ID does not exist, an "entity not found"
	 * exception is thrown
	 * 
	 * @throws SQLException if an SQL error occurs accessing the database
	 */
	@Test
	public void deleteNote_NoteIdDoesNotExist_ThrowException() throws SQLException {
		final UUID userUuid = UUID.randomUUID();
		final User userToCreate = new User(userUuid, "deletiontest2@example.com");
		insertUserIntoDatabase(userToCreate);

		final UUID folderUuid = UUID.randomUUID();
		final Folder folderToCreate = new Folder(folderUuid, userUuid, "My Folder");
		insertFolderIntoDatabase(folderToCreate);

		final UUID noteUuid = UUID.randomUUID();
		final Note noteToCreate = new Note(noteUuid, folderUuid, userUuid, "Cool Note",
				"To be or not to be, that is the question", new Date(), new Date());
		insertNoteIntoDatabase(noteToCreate);

		assertThrows(EntityNotFoundException.class, () -> {
			dao.deleteNote(userUuid, UUID.randomUUID());
		});
	}

	/**
	 * Verify that when the specified user ID does not exist, an exception is thrown
	 * 
	 * @throws SQLException if an SQL error occurs accessing the database
	 */
	@Test
	public void deleteNote_UserDoesNotExist_ThrowException() throws SQLException {
		final UUID userUuid = UUID.randomUUID();
		final User userToCreate = new User(userUuid, "deletiontest3@example.com");
		insertUserIntoDatabase(userToCreate);

		final UUID folderUuid = UUID.randomUUID();
		final Folder folderToCreate = new Folder(folderUuid, userUuid, "My Folder");
		insertFolderIntoDatabase(folderToCreate);

		final UUID noteUuid = UUID.randomUUID();
		final Note noteToCreate = new Note(noteUuid, folderUuid, userUuid, "Cool Note",
				"To be or not to be, that is the question", new Date(), new Date());
		insertNoteIntoDatabase(noteToCreate);

		assertThrows(EntityNotFoundException.class, () -> {
			dao.deleteNote(UUID.randomUUID(), noteUuid);
		});
	}

	/**
	 * Test that when we attempt to delete a note that is owned by another user,
	 * throw an "entity not found" exception
	 * 
	 * @throws SQLException if an SQL error occurs accessing the database
	 */
	@Test
	public void deleteNote_NoteOwnedByAnotherUser_ThrowException() throws SQLException {
		final UUID ourUserUuid = UUID.randomUUID();
		final User ourUser = new User(ourUserUuid, "deletiontest4@example.com");
		insertUserIntoDatabase(ourUser);

		final UUID noteUuid = UUID.randomUUID();
		final Note noteToCreate = new Note(noteUuid, null, ourUserUuid, "Cool Note",
				"To be or not to be, that is the question", new Date(), new Date());
		insertNoteIntoDatabase(noteToCreate);

		final UUID otherUserUuid = UUID.randomUUID();
		final User otherUser = new User(otherUserUuid, "deletiontest5@example.com");
		insertUserIntoDatabase(otherUser);

		final UUID otherNoteUuid = UUID.randomUUID();
		final Note otherNote = new Note(otherNoteUuid, null, otherUserUuid, "Cool Note",
				"To be or not to be, that is the question", new Date(), new Date());
		insertNoteIntoDatabase(otherNote);

		assertThrows(EntityNotFoundException.class, () -> {
			dao.deleteNote(ourUserUuid, otherNoteUuid);
		});
	}
}
