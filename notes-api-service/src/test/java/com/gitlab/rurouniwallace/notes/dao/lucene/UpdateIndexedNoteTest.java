package com.gitlab.rurouniwallace.notes.dao.lucene;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.gitlab.rurouniwallace.notes.dao.LuceneDao;
import com.gitlab.rurouniwallace.notes.exceptions.DataAccessException;
import com.gitlab.rurouniwallace.notes.exceptions.EntityNotFoundException;
import com.gitlab.rurouniwallace.notes.models.Note;
import com.gitlab.rurouniwallace.notes.models.User;

public class UpdateIndexedNoteTest {

	private User user;

	private User otherUser;

	private Note otherUsersNote;

	private LuceneDao dao;

	private Directory index;

	private Analyzer analyzer;

	private Note note;

	private IndexWriter writer;

	@BeforeEach
	public void setUp() throws IOException {
		user = new User(UUID.randomUUID(), "searchNotes@example.com", "test1234", "(716)555-5555", "Test", "User");

		analyzer = new StandardAnalyzer();
		index = new RAMDirectory();

		final IndexWriterConfig config = new IndexWriterConfig(analyzer);
		writer = new IndexWriter(index, config);

		dao = new LuceneDao(index, analyzer, writer);

		note = new Note(UUID.randomUUID(), UUID.randomUUID(), user.getUuid(), "Pursuit of Happiness",
				"I'm on the pursuit of happiness and I know, Everything that shine ain't always gonna be gold",
				new Date(1597959912345L), new Date(1613804168657L));

		insertNoteIntoIndex(index, analyzer, user.getUuid(), note);

		// throw in another user and another note
		otherUser = new User(UUID.randomUUID(), "someotherguy@example.com", "abcd1234", "(716)333-3333", "Other",
				"Guy");

		otherUsersNote = new Note(UUID.randomUUID(), UUID.randomUUID(), otherUser.getUuid(), "Another Note",
				"Here is another note with some text and stuff", new Date(1597959912345L), new Date(1613804168657L));

		insertNoteIntoIndex(index, analyzer, otherUser.getUuid(), otherUsersNote);
	}

	@ParameterizedTest
	@MethodSource("provideNoteUpdates")
	public void updateIndexedNote_UpdateSingleField_NoteUpdatedSuccessfully(final String field, final String value)
			throws DataAccessException, IOException {
		final Note updatePayload = createUpdatePayload(field, value);

		dao.updateIndexedNote(user.getUuid(), note.getUuid(), updatePayload);

		final IndexReader reader = DirectoryReader.open(index);
		final IndexSearcher searcher = new IndexSearcher(reader);

		final Query query = new TermQuery(new Term("uuid", note.getUuid().toString()));

		final TopDocs docs = searcher.search(query, 1);

		final ScoreDoc[] hits = docs.scoreDocs;

		final int docId = hits[0].doc;

		final Document document = searcher.doc(docId);

		final Note noteFromIndex = readNoteFromDocument(document);

		final Note expectedUpdatedNote = updateField(note, field, value);

		assertEquals(expectedUpdatedNote, noteFromIndex);
	}

	@Test
	public void updateIndexedNote_NoteIdDoesntExist_ThrowException() throws DataAccessException {

		final Note updatePayload = new Note();
		updatePayload.setBody("I'm on a pursuit of emptiness and I know...");

		assertThrows(EntityNotFoundException.class, () -> {
			dao.updateIndexedNote(user.getUuid(), UUID.randomUUID(), updatePayload);
		});
	}

	@Test
	public void updateIndexedNote_UserIdDoesntExist_ThrowException() throws DataAccessException {

		final Note updatePayload = new Note();
		updatePayload.setBody("I'm on a pursuit of caffeine hits and I know...");

		assertThrows(EntityNotFoundException.class, () -> {
			dao.updateIndexedNote(UUID.randomUUID(), note.getUuid(), updatePayload);
		});
	}

	@Test
	public void updateIndexedNote_AttemptToUpdateAnotherUsersNotes_ThrowException() {
		final Note updatePayload = new Note();
		updatePayload.setBody("I'm on a pursuit of caffeine hits and I know...");

		assertThrows(EntityNotFoundException.class, () -> {
			dao.updateIndexedNote(user.getUuid(), otherUsersNote.getUuid(), updatePayload);
		});
	}

	@AfterEach
	public void tearDown() throws IOException {
		writer.close();
	}

	private void insertNoteIntoIndex(final Directory index, final Analyzer analyzer, final UUID userId, final Note note)
			throws IOException {
		final IndexWriterConfig config = new IndexWriterConfig(analyzer);

		final Document doc = new Document();
		doc.add(new TextField("title", note.getTitle(), Field.Store.YES));
		doc.add(new TextField("body", note.getBody(), Field.Store.YES));
		doc.add(new StringField("uuid", note.getUuid().toString(), Field.Store.YES));
		doc.add(new StringField("userId", userId.toString(), Field.Store.YES));

		if (note.getFolder() != null) {
			doc.add(new StringField("folderId", note.getFolder().toString(), Field.Store.YES));
		}

		if (note.getCreated() != null) {
			doc.add(new StoredField("created", note.getCreated().getTime()));
		}

		if (note.getLastUpdated() != null) {
			doc.add(new StoredField("lastUpdated", note.getLastUpdated().getTime()));
		}

		writer.addDocument(doc);
		writer.commit();
	}

	/**
	 * Read a note instance from a Lucene document
	 * 
	 * @param document Lucene document
	 * @return the note read from the document
	 */
	private Note readNoteFromDocument(final Document document) {
		final Note note = new Note();

		if (document.get("uuid") != null) {
			note.setUuid(UUID.fromString(document.get("uuid")));
		}

		if (document.get("folderId") != null) {
			note.setFolder(UUID.fromString(document.get("folderId")));
		}

		if (document.get("userId") != null) {
			note.setUser(UUID.fromString(document.get("userId")));
		}

		note.setTitle(document.get("title"));
		note.setBody(document.get("body"));

		if (document.get("lastUpdated") != null) {
			note.setLastUpdated(new Date(Long.parseLong(document.get("lastUpdated"))));
		}

		if (document.get("created") != null) {
			note.setCreated(new Date(Long.parseLong(document.get("created"))));
		}

		return note;
	}

	private Note createUpdatePayload(final String fieldToUpdate, final String updatedValue) {
		return updateField(new Note(), fieldToUpdate, updatedValue);
	}

	private Note updateField(final Note updatePayload, final String fieldToUpdate, final String updatedValue) {
		switch (fieldToUpdate) {
		case "uuid":
			updatePayload.setUuid(UUID.fromString(updatedValue));
			break;
		case "folderId":
			updatePayload.setFolder(UUID.fromString(updatedValue));
			break;
		case "userId":
			updatePayload.setUser(UUID.fromString(updatedValue));
			break;
		case "title":
			updatePayload.setTitle(updatedValue);
			break;
		case "body":
			updatePayload.setBody(updatedValue);
			break;
		case "created":
			updatePayload.setCreated(new Date(Long.parseLong(updatedValue)));
			break;
		case "lastUpdated":
			updatePayload.setLastUpdated(new Date(Long.parseLong(updatedValue)));
			break;
		}
		return updatePayload;
	}

	private static Stream<Arguments> provideNoteUpdates() {
		final List<Arguments> arguments = new ArrayList<>();

		arguments.add(Arguments.of("folderId", UUID.randomUUID().toString()));
		arguments.add(Arguments.of("title", "Pursuit of Sadness"));
		arguments.add(Arguments.of("body",
				"I'm on a pursuit of sadness and I know, Everything that's cold and hard isn't a stone"));
		arguments.add(Arguments.of("created", "1614901610276"));
		arguments.add(Arguments.of("lastUpdated", "1614901637821"));

		return arguments.stream();
	}

}
