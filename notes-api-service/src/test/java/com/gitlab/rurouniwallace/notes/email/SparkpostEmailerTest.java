package com.gitlab.rurouniwallace.notes.email;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.matchingJsonPath;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.junit.jupiter.api.Assertions.assertThrows;

import javax.ws.rs.client.ClientBuilder;

import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.gitlab.rurouniwallace.notes.config.SparkpostConfiguration;
import com.gitlab.rurouniwallace.notes.exceptions.EmailTransmissionException;

public class SparkpostEmailerTest {

	private static final String API_KEY = "2ac1498c1c9082d551c99b950637a8602ace0487";

	private static final String EMAIL_VERIFICATION_TEMPLATE = "notes-api-email-verification";

	private static final String EMAIL_ADDRESS = "unittest@example.com";

	private static WireMockServer wireMockServer;

	private static SparkpostConfiguration config;

	private SparkpostEmailer emailer;

	private String verificationUrl;

	@BeforeAll
	public static void setUpBeforeClass() {
		WireMock.configureFor("localhost", 8090);

		wireMockServer = new WireMockServer(wireMockConfig().port(8090));
		wireMockServer.start();

		config = new SparkpostConfiguration(API_KEY, "http://localhost:8090", EMAIL_VERIFICATION_TEMPLATE);
	}

	@BeforeEach
	public void setUp() {
		wireMockServer.resetAll();

		emailer = new SparkpostEmailer(config, ClientBuilder.newClient());

		verificationUrl = "http://example.com";
	}

	private void stubTransmissionSuccess() {
		stubFor(post(urlPathEqualTo("/api/v1/transmissions"))
				.willReturn(aResponse().withHeader("Content-Type", "application/json").withStatus(HttpStatus.OK_200)
						.withBody(fixture("fixtures/downstreamResponses/transmissionSuccessResponse.json"))));
	}

	@Test
	public void sendVerificationEmail_PassesAuthorizationHeader() throws EmailTransmissionException {
		stubTransmissionSuccess();

		emailer.sendVerificationEmail(EMAIL_ADDRESS, verificationUrl);

		verify(postRequestedFor(urlPathEqualTo("/api/v1/transmissions")).withHeader("Authorization", equalTo(API_KEY)));
	}

	@Test
	public void sendVerificationEmail_PassesEmailVerificationTemplate() throws EmailTransmissionException {
		stubTransmissionSuccess();

		emailer.sendVerificationEmail(EMAIL_ADDRESS, verificationUrl);

		verify(postRequestedFor(urlPathEqualTo("/api/v1/transmissions")).withRequestBody(
				matchingJsonPath("$.content[?(@.template_id == '" + EMAIL_VERIFICATION_TEMPLATE + "')]")));
	}

	@Test
	public void sendVerificationEmail_PassesRecipientEmailAddress() throws EmailTransmissionException {
		stubTransmissionSuccess();

		emailer.sendVerificationEmail(EMAIL_ADDRESS, verificationUrl);

		verify(postRequestedFor(urlPathEqualTo("/api/v1/transmissions"))
				.withRequestBody(matchingJsonPath("$.recipients[0][?(@.address == '" + EMAIL_ADDRESS + "')]")));
	}

	@Test
	public void sendVerificationEmail_PassesVerificationUrl() throws EmailTransmissionException {
		stubTransmissionSuccess();

		emailer.sendVerificationEmail(EMAIL_ADDRESS, verificationUrl);

		verify(postRequestedFor(urlPathEqualTo("/api/v1/transmissions")).withRequestBody(
				matchingJsonPath("$.substitution_data[?(@.verification_url == '" + verificationUrl + "')]")));
	}

	@Test
	public void sendVerificationEmail_ServiceUnavailableResponse_ThrowException() {
		stubFor(post(urlPathEqualTo("/api/v1/transmissions")).willReturn(aResponse()
				.withHeader("Content-Type", "application/json").withStatus(HttpStatus.SERVICE_UNAVAILABLE_503)
				.withBody(fixture("fixtures/downstreamResponses/transmissionServiceUnavailable.json"))));

		assertThrows(EmailTransmissionException.class, () -> {
			emailer.sendVerificationEmail(EMAIL_ADDRESS, verificationUrl);
		});
	}

	@Test
	public void sendVerificationEmail_TooManyRequestsResponse_ThrowException() {
		stubFor(post(urlPathEqualTo("/api/v1/transmissions")).willReturn(
				aResponse().withHeader("Content-Type", "application/json").withStatus(HttpStatus.TOO_MANY_REQUESTS_429)
						.withBody(fixture("fixtures/downstreamResponses/transmissionTooManyRequestsResponse.json"))));

		assertThrows(EmailTransmissionException.class, () -> {
			emailer.sendVerificationEmail(EMAIL_ADDRESS, verificationUrl);
		});
	}

	@Test
	public void sendVerificationEmail_SendingLimitExceeded_ThrowException() {
		stubFor(post(urlPathEqualTo("/api/v1/transmissions")).willReturn(
				aResponse().withHeader("Content-Type", "application/json").withStatus(HttpStatus.ENHANCE_YOUR_CALM_420)
						.withBody(fixture("fixtures/downstreamResponses/transmissionOverSendingLimit.json"))));

		assertThrows(EmailTransmissionException.class, () -> {
			emailer.sendVerificationEmail(EMAIL_ADDRESS, verificationUrl);
		});
	}

	@Test
	public void sendVerificationEmail_InternalServerError_ThrowException() {
		stubFor(post(urlPathEqualTo("/api/v1/transmissions")).willReturn(aResponse()
				.withHeader("Content-Type", "application/json").withStatus(HttpStatus.INTERNAL_SERVER_ERROR_500)));

		assertThrows(EmailTransmissionException.class, () -> {
			emailer.sendVerificationEmail(EMAIL_ADDRESS, verificationUrl);
		});
	}

	@AfterAll
	public static void tearDownAfterClass() {
		wireMockServer.stop();
	}
}
