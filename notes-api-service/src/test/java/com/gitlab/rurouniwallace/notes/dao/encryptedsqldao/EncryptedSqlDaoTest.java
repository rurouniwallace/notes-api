package com.gitlab.rurouniwallace.notes.dao.encryptedsqldao;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.spec.KeySpec;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Base64;
import java.util.HashMap;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.lang3.RandomStringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.rurouniwallace.notes.config.SecurityConfiguration;
import com.gitlab.rurouniwallace.notes.exceptions.DataAccessException;
import com.gitlab.rurouniwallace.notes.models.User;

import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;

public class EncryptedSqlDaoTest {

	/**
	 * JDBC URL
	 */
	protected static final String MOCK_CONNECTION_URL = "jdbc:hsqldb:mem:myDb;sql.sql.syntax_pgs=true";

	/**
	 * Sample password to use in tests
	 */
	protected static final String TEST_PASSWORD = "test1234";

	/**
	 * Encryption algorithm to use for encrypting user data in database
	 */
	protected static final String ENCRYPTION_ALGORITHM = "AES";

	protected static final String INITIALIZATION_VECTOR = "NRSUFGV4XPMWWB3Z";

	/**
	 * Database connection source
	 */
	protected static BasicDataSource dataSource;

	/**
	 * Security configuration
	 */
	protected SecurityConfiguration securityConfig;

	/**
	 * User being tested
	 */
	protected User testUser;

	/**
	 * Set up database and initialize the schema using Liquibase
	 * 
	 * @throws SQLException an SQL error occurred
	 * @throws LiquibaseException Liquibase error occurred
	 */
	@BeforeAll
	public static void setUpBeforeClass() throws SQLException, LiquibaseException {

		dataSource = new BasicDataSource();
		dataSource.setUrl(MOCK_CONNECTION_URL);

		final Connection connection = dataSource.getConnection();

		final Database database = DatabaseFactory.getInstance()
				.findCorrectDatabaseImplementation(new JdbcConnection(connection));

		final Liquibase liquibase = new Liquibase("liquibase/changelog.xml", new ClassLoaderResourceAccessor(),
				database);
		liquibase.update(new Contexts(), new LabelExpression());
		liquibase.close();

	}

	/**
	 * Initialize configurations
	 * 
	 * @throws NoSuchAlgorithmException
	 */
	@BeforeEach
	public void setUp() throws NoSuchAlgorithmException {
		securityConfig = new SecurityConfiguration();
		securityConfig.setHashCost(5);
		securityConfig.setInitializationVector(INITIALIZATION_VECTOR);

		testUser = new User(UUID.randomUUID(), generateRandomEmail(), TEST_PASSWORD, generateRandomPhone(), "Test",
				"User", false);
		testUser.setEncryptionKey("test1234");
		testUser.setEncryptionSalt("test");
	}

	@Test
	public void registerUser_UserCredentialsSavedSuccessfully() throws DataAccessException, SQLException {
		final EncryptedSqlDao dao = new EncryptedSqlDao(dataSource, securityConfig);

		dao.registerUser(testUser);

		final User userCredsFromDatabase = getUserCredentialsFromDatabase(testUser);

		final User expectedUserCredsFromDatabase = new User(testUser.getUuid(), testUser.getEmail(),
				testUser.getPassword());

		assertEquals(expectedUserCredsFromDatabase, userCredsFromDatabase);
	}

	@Test
	public void registerUser_EncryptedDataCreated()
			throws DataAccessException, SQLException, JsonProcessingException, GeneralSecurityException {
		final EncryptedSqlDao dao = new EncryptedSqlDao(dataSource, securityConfig);

		dao.registerUser(testUser);

		final String encryptedUserDataFromDatabase = getEncryptedUserDataFromDatabase(testUser);

		final UserData userData = buildUserDataPayload(testUser);

		final String jsonEncodedUserData = jsonEncodeObject(userData);

		LoggerFactory.getLogger(EncryptedSqlDaoTest.class).info("Encoded data: " + jsonEncodedUserData);

		final String base64EncodedData = Base64.getEncoder().encodeToString(jsonEncodedUserData.getBytes());

		final String expectedEncryptedUserData = aesEncryptString(base64EncodedData, testUser.getEncryptionKey(),
				testUser.getEncryptionSalt());

		assertEquals(expectedEncryptedUserData, encryptedUserDataFromDatabase);
	}

	private String generateRandomEmail() {
		return RandomStringUtils.randomAlphabetic(16).toLowerCase() + "@example.com";
	}

	private String aesEncryptString(final String dataToEncrypt, final String encryptionKey, final String salt)
			throws GeneralSecurityException {
		byte[] dataInBytes = dataToEncrypt.getBytes();

		Security.addProvider(new BouncyCastleProvider());
		final Cipher encryptionCipher = Cipher.getInstance("AES/GCM/NoPadding", "BC");

		final SecretKeySpec keySpec = buildSecretKeyFromPassword(encryptionKey, salt);

		final IvParameterSpec iv = new IvParameterSpec(securityConfig.getInitializationVector().getBytes());

		encryptionCipher.init(Cipher.ENCRYPT_MODE, keySpec, iv);
		final byte[] encryptedBytes = encryptionCipher.doFinal(dataInBytes);
		return Base64.getEncoder().encodeToString(encryptedBytes);

	}

	private String generateRandomSalt() {
		return RandomStringUtils.randomAscii(64);
	}

	private SecretKeySpec buildSecretKeyFromPassword(final String password, final String salt)
			throws GeneralSecurityException {
		KeySpec spec = new PBEKeySpec(password.toCharArray(), salt.getBytes(), 65536, 256); // AES-256
		SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
		byte[] key = f.generateSecret(spec).getEncoded();
		return new SecretKeySpec(key, ENCRYPTION_ALGORITHM);
	}

	private String jsonEncodeObject(final Object object) throws JsonProcessingException {
		final ObjectMapper jsonEncoder = new ObjectMapper();
		return jsonEncoder.writeValueAsString(object);
	}

	private String generateRandomPhone() {
		return String.format("(%s)%s-%s", RandomStringUtils.randomNumeric(3), RandomStringUtils.randomNumeric(3),
				RandomStringUtils.randomNumeric(4));
	}

	private String generateRandomEncryptionKey() throws NoSuchAlgorithmException {
		final KeyGenerator keyGen = KeyGenerator.getInstance(ENCRYPTION_ALGORITHM);

		keyGen.init(256);

		final SecretKey secretKey = keyGen.generateKey();

		return new String(secretKey.getEncoded());
	}

	private UserData buildUserDataPayload(final User user) {
		final UserData userData = new UserData(user);

		userData.setFolders(new HashMap<>());

		return userData;
	}

	/**
	 * Insert a user into the database
	 * 
	 * @param user user to insert
	 * @throws SQLException an SQL error occurred
	 */
	protected static User getUserCredentialsFromDatabase(final User user) throws SQLException {
		final Connection connection = dataSource.getConnection();

		final PreparedStatement query = connection.prepareStatement("SELECT * FROM UserCredentials WHERE uuid = ?");

		query.setObject(1, user.getUuid());

		final ResultSet results = query.executeQuery();

		if (!results.next()) {
			return null;
		}

		final User userFromDatabase = new User();
		userFromDatabase.setUuid((UUID) results.getObject("uuid"));
		userFromDatabase.setEmail(results.getString("email"));
		userFromDatabase.setPassword(results.getString("password"));

		return userFromDatabase;
	}

	protected static String getEncryptedUserDataFromDatabase(final User user) throws SQLException {
		final Connection connection = dataSource.getConnection();

		final PreparedStatement query = connection.prepareStatement("SELECT * FROM EncryptedData WHERE userId = ?");

		query.setObject(1, user.getUuid());

		final ResultSet results = query.executeQuery();

		if (!results.next()) {
			return null;
		}

		final String encryptedData = results.getString("data");

		return encryptedData;
	}
}
