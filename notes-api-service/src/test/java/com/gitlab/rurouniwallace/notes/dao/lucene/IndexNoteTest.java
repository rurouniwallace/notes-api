package com.gitlab.rurouniwallace.notes.dao.lucene;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.gitlab.rurouniwallace.notes.dao.LuceneDao;
import com.gitlab.rurouniwallace.notes.exceptions.DataAccessException;
import com.gitlab.rurouniwallace.notes.models.Note;
import com.gitlab.rurouniwallace.notes.models.User;

public class IndexNoteTest {

	private static User user;

	private static LuceneDao dao;

	private static Directory index;

	private static Analyzer analyzer;

	private static Note note;

	private static IndexWriter writer;

	@BeforeAll
	public static void setUpBeforeClass() throws IOException {
		user = new User(UUID.randomUUID(), "searchNotes@example.com", "test1234", "(716)555-5555", "Test", "User");

		analyzer = new StandardAnalyzer();
		index = new RAMDirectory();

		final IndexWriterConfig config = new IndexWriterConfig(analyzer);
		writer = new IndexWriter(index, config);

		dao = new LuceneDao(index, analyzer, writer);

		note = new Note(UUID.randomUUID(), UUID.randomUUID(), user.getUuid(), "My Note",
				"This is my note. There are many like it but this one is mine.");
	}

	/**
	 * Test indexing a note, verifying that it was successfully inserted into the
	 * index.
	 * 
	 * @throws DataAccessException if an exception occurred within the DAO
	 * @throws IOException if an exception occurs checking the index
	 */
	@ParameterizedTest
	@MethodSource("provideValues")
	public void indexNote_NoteInsertedIntoIndex(final String field, final String value)
			throws DataAccessException, IOException {

		dao.indexNote(user.getUuid(), note);

		final IndexReader reader = DirectoryReader.open(index);
		final IndexSearcher searcher = new IndexSearcher(reader);

		final Query query = new TermQuery(new Term(field, value));

		final TopDocs docs = searcher.search(query, 1);

		final ScoreDoc[] hits = docs.scoreDocs;

		final int docId = hits[0].doc;

		final Document d = searcher.doc(docId);

		assertEquals(note, readNoteFromDocument(d));
	}

	/**
	 * Read a note instance from a Lucene document
	 * 
	 * @param document Lucene document
	 * @return the note read from the document
	 */
	private Note readNoteFromDocument(final Document document) {
		final Note note = new Note();

		if (document.get("uuid") != null) {
			note.setUuid(UUID.fromString(document.get("uuid")));
		}

		if (document.get("folderId") != null) {
			note.setFolder(UUID.fromString(document.get("folderId")));
		}

		if (document.get("userId") != null) {
			note.setUser(UUID.fromString(document.get("userId")));
		}

		note.setTitle(document.get("title"));
		note.setBody(document.get("body"));

		if (document.get("lastUpdated") != null) {
			note.setLastUpdated(new Date(Long.parseLong(document.get("lastUpdated"))));
		}

		if (document.get("created") != null) {
			note.setCreated(new Date(Long.parseLong(document.get("created"))));
		}

		return note;
	}

	private static Stream<Arguments> provideValues() {
		final List<Arguments> arguments = new ArrayList<>();

		arguments.add(Arguments.of("uuid", note.getUuid().toString()));
		arguments.add(Arguments.of("folderId", note.getFolder().toString()));
		arguments.add(Arguments.of("userId", user.getUuid().toString()));
		arguments.add(Arguments.of("title", "note"));
		arguments.add(Arguments.of("body", "my"));

		return arguments.stream();
	}

	@AfterAll
	public static void tearDownAfterClass() throws IOException {
		index.close();
	}
}
