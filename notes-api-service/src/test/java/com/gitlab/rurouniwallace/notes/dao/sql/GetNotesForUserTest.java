package com.gitlab.rurouniwallace.notes.dao.sql;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;

import com.gitlab.rurouniwallace.notes.dao.SqlDao;
import com.gitlab.rurouniwallace.notes.exceptions.DataAccessException;
import com.gitlab.rurouniwallace.notes.exceptions.EntityNotFoundException;
import com.gitlab.rurouniwallace.notes.models.Folder;
import com.gitlab.rurouniwallace.notes.models.Note;
import com.gitlab.rurouniwallace.notes.models.User;

public class GetNotesForUserTest extends BaseSqlDaoTest {

	@Test
	public void getNotesForUser_RetrieveAllNotesSuccessful_ReturnsNotes() throws SQLException, DataAccessException {
		final SqlDao dao = new SqlDao(dataSource, securityConfig);

		final UUID userUuid = UUID.randomUUID();
		final User userToCreate = new User(userUuid, "testuser13@example.com");
		insertUserIntoDatabase(userToCreate);

		final Note noteToCreate1 = new Note(UUID.randomUUID(), null, userUuid, "Cool Note",
				"To be or not to be, that is the question", new Date(), new Date());
		insertNoteIntoDatabase(noteToCreate1);

		final Note noteToCreate2 = new Note(UUID.randomUUID(), null, userUuid, "Lorem Ipsum",
				"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean condimentum nec quam ut vulputate. In massa.",
				new Date(), new Date());
		insertNoteIntoDatabase(noteToCreate2);

		final Note noteToCreate3 = new Note(UUID.randomUUID(), null, userUuid, "Pledge",
				"I pledge allegiance to my Flag and the Republic for which it stands, one nation, indivisible, with liberty and justice for all.",
				new Date(), new Date());
		insertNoteIntoDatabase(noteToCreate3);

		// throw in an extraneous user's note to verify that the DAO only returns notes
		// owned by the specified user
		final UUID extraneousUserUuid = UUID.randomUUID();
		final User extraneousUser = new User(extraneousUserUuid, "testuser14@example.com");
		insertUserIntoDatabase(extraneousUser);

		final Note noteToCreate4 = new Note(UUID.randomUUID(), null, extraneousUserUuid, "Poem",
				"Roses are red, violets are blue.", new Date(), new Date());
		insertNoteIntoDatabase(noteToCreate4);

		final List<Note> notesInserted = Arrays.asList(noteToCreate1, noteToCreate2, noteToCreate3);

		final List<Note> notesFromDao = dao.getNotesForUser(userUuid, Optional.empty());

		assertEquals(notesInserted, notesFromDao);
	}

	@Test
	public void getNotesForUser_RetrieveAllNotesInFolder_ReturnsNotesInFolder()
			throws SQLException, DataAccessException {
		final SqlDao dao = new SqlDao(dataSource, securityConfig);

		final UUID userUuid = UUID.randomUUID();
		final User userToCreate = new User(userUuid, "testuser15@example.com");
		insertUserIntoDatabase(userToCreate);

		final UUID folderUuid = UUID.randomUUID();
		final Folder folderToCreate = new Folder(folderUuid, userUuid, "My Folder");
		insertFolderIntoDatabase(folderToCreate);

		final Note noteToCreate1 = new Note(UUID.randomUUID(), folderUuid, userUuid, "Cool Note",
				"To be or not to be, that is the question", new Date(), new Date());
		insertNoteIntoDatabase(noteToCreate1);

		final Note noteToCreate2 = new Note(UUID.randomUUID(), folderUuid, userUuid, "Lorem Ipsum",
				"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean condimentum nec quam ut vulputate. In massa.",
				new Date(), new Date());
		insertNoteIntoDatabase(noteToCreate2);

		final Note noteToCreate3 = new Note(UUID.randomUUID(), folderUuid, userUuid, "Pledge",
				"I pledge allegiance to my Flag and the Republic for which it stands, one nation, indivisible, with liberty and justice for all.",
				new Date(), new Date());
		insertNoteIntoDatabase(noteToCreate3);

		final List<Note> notesInserted = Arrays.asList(noteToCreate1, noteToCreate2, noteToCreate3);

		// throw in an extraneous user and folder
		final UUID extraneousUserUuid = UUID.randomUUID();
		final User extraneousUser = new User(extraneousUserUuid, "testuser16@example.com");
		insertUserIntoDatabase(extraneousUser);

		final UUID extraneousFolderUuid = UUID.randomUUID();
		final Folder extraneousFolderToCreate = new Folder(extraneousFolderUuid, extraneousUserUuid, "His Folder");
		insertFolderIntoDatabase(extraneousFolderToCreate);

		// throw in an extraneous folder, this time belonging to the user under test
		final UUID extraneousFolderUuid2 = UUID.randomUUID();
		final Folder extraneousFolderToCreate2 = new Folder(extraneousFolderUuid2, userUuid, "My Other Folder");
		insertFolderIntoDatabase(extraneousFolderToCreate2);

		final List<Note> notesFromDao = dao.getNotesForUser(userUuid, Optional.of(folderUuid));

		assertEquals(notesInserted, notesFromDao);
	}

	@Test
	public void getNotesForUser_UserHasNoNotes_ReturnsEmptyList() throws SQLException, DataAccessException {
		final SqlDao dao = new SqlDao(dataSource, securityConfig);

		final UUID userUuid = UUID.randomUUID();
		final User userToCreate = new User(userUuid, "testuser17@example.com");
		insertUserIntoDatabase(userToCreate);

		// throw in an extraneous user's note
		final UUID extraneousUserUuid = UUID.randomUUID();
		final User extraneousUser = new User(extraneousUserUuid, "testuser18@example.com");
		insertUserIntoDatabase(extraneousUser);

		final Note extraneousNote = new Note(UUID.randomUUID(), null, extraneousUserUuid, "Poem",
				"Roses are red, violets are blue.", new Date(), new Date());
		insertNoteIntoDatabase(extraneousNote);

		final List<Note> notesFromDao = dao.getNotesForUser(userUuid, Optional.empty());

		assertEquals(0, notesFromDao.size(), "Non-zero number of notes returned from DAO, expected 0");
	}

	@Test
	public void getNotesForUser_UserDoesNotExist_ThrowException() throws SQLException {
		final SqlDao dao = new SqlDao(dataSource, securityConfig);

		// throw in an extraneous user and note
		final UUID extraneousUserUuid = UUID.randomUUID();
		final User extraneousUser = new User(extraneousUserUuid, "testuser19@example.com");
		insertUserIntoDatabase(extraneousUser);

		final Note extraneousNote = new Note(UUID.randomUUID(), null, extraneousUserUuid, "Poem",
				"Roses are red, violets are blue.", new Date(), new Date());
		insertNoteIntoDatabase(extraneousNote);

		assertThrows(EntityNotFoundException.class, () -> {
			dao.getNotesForUser(UUID.randomUUID(), Optional.empty());
		});
	}

	@Test
	public void getNotesForUser_FolderDoesNotExist_ThrowException() throws SQLException {
		final SqlDao dao = new SqlDao(dataSource, securityConfig);

		// our user under test
		final UUID userUuid = UUID.randomUUID();
		final User userToCreate = new User(userUuid, "testuser20@example.com");
		insertUserIntoDatabase(userToCreate);

		// create a folder for this user
		final UUID folderUuid = UUID.randomUUID();
		final Folder folderToCreate = new Folder(folderUuid, userUuid, "My Folder");
		insertFolderIntoDatabase(folderToCreate);

		final Note noteToCreate = new Note(UUID.randomUUID(), folderUuid, userUuid, "Poem",
				"Roses are red, violets are blue.", new Date(), new Date());
		insertNoteIntoDatabase(noteToCreate);

		assertThrows(EntityNotFoundException.class, () -> {
			dao.getNotesForUser(userUuid, Optional.of(UUID.randomUUID()));
		});
	}
}
