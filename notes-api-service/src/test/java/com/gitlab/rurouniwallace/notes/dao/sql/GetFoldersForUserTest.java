package com.gitlab.rurouniwallace.notes.dao.sql;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.gitlab.rurouniwallace.notes.dao.SqlDao;
import com.gitlab.rurouniwallace.notes.exceptions.DataAccessException;
import com.gitlab.rurouniwallace.notes.exceptions.EntityNotFoundException;
import com.gitlab.rurouniwallace.notes.models.Folder;
import com.gitlab.rurouniwallace.notes.models.User;

public class GetFoldersForUserTest extends BaseSqlDaoTest {

	private SqlDao dao;

	@BeforeEach
	public void setUp() {
		dao = new SqlDao(dataSource, securityConfig);
	}

	@Test
	public void getFoldersForUser_FolderRetrievalSuccessful_ReturnsFolders() throws SQLException, DataAccessException {
		final UUID userUuid = UUID.randomUUID();
		final User user = new User(userUuid, "getfoldersforuser@example.com");
		insertUserIntoDatabase(user);

		final Folder folderToAdd1 = new Folder(UUID.randomUUID(), userUuid, "Folder 1");
		insertFolderIntoDatabase(folderToAdd1);

		final Folder folderToAdd2 = new Folder(UUID.randomUUID(), userUuid, "Folder 2");
		insertFolderIntoDatabase(folderToAdd2);

		final Folder folderToAdd3 = new Folder(UUID.randomUUID(), userUuid, "Folder 3");
		insertFolderIntoDatabase(folderToAdd3);

		// throw in an extraneous user and folder
		final UUID extraneousUserUuid = UUID.randomUUID();
		final User extraneousUser = new User(extraneousUserUuid, "getfoldersforuser2@example.com");
		insertUserIntoDatabase(extraneousUser);

		final Folder extraneousFolder = new Folder(UUID.randomUUID(), extraneousUserUuid, "Extraneous Folder");
		insertFolderIntoDatabase(extraneousFolder);

		final List<Folder> expectedResult = Arrays.asList(folderToAdd1, folderToAdd2, folderToAdd3);

		final List<Folder> result = dao.getFoldersForUser(userUuid);

		assertEquals(expectedResult, result);
	}

	@Test
	public void getFoldersForUser_UserDoesNotExist_ThrowException() throws SQLException {
		final UUID userUuid = UUID.randomUUID();
		final User user = new User(userUuid, "getfoldersforuser3@example.com");
		insertUserIntoDatabase(user);

		final Folder folderToAdd1 = new Folder(UUID.randomUUID(), userUuid, "Folder 1");
		insertFolderIntoDatabase(folderToAdd1);

		final Folder folderToAdd2 = new Folder(UUID.randomUUID(), userUuid, "Folder 2");
		insertFolderIntoDatabase(folderToAdd2);

		final Folder folderToAdd3 = new Folder(UUID.randomUUID(), userUuid, "Folder 3");
		insertFolderIntoDatabase(folderToAdd3);

		assertThrows(EntityNotFoundException.class, () -> {
			dao.getFoldersForUser(UUID.randomUUID());
		});
	}
}
