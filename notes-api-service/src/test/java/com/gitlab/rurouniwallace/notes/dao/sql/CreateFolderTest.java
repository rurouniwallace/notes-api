package com.gitlab.rurouniwallace.notes.dao.sql;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.sql.SQLException;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.gitlab.rurouniwallace.notes.dao.SqlDao;
import com.gitlab.rurouniwallace.notes.exceptions.DataAccessException;
import com.gitlab.rurouniwallace.notes.exceptions.EntityNotFoundException;
import com.gitlab.rurouniwallace.notes.models.Folder;
import com.gitlab.rurouniwallace.notes.models.User;

/**
 * Test cases for creating a folder
 */
public class CreateFolderTest extends BaseSqlDaoTest {

	/**
	 * DAO under test
	 */
	private SqlDao dao;

	/**
	 * Initialize test parameters
	 */
	@BeforeEach
	public void setUp() {
		super.setUp();
		dao = new SqlDao(dataSource, securityConfig);
	}

	/**
	 * Upon successfully creating a folder, verify it was inserted into a database.
	 * 
	 * @throws DataAccessException if a data access error occurred in the DAO
	 * @throws SQLException if an SQL exception occurred accessing the database
	 */
	@Test
	public void createFolder_CreateFolderSuccessful_FolderCreatedSuccessfully()
			throws DataAccessException, SQLException {
		final UUID userUuid = UUID.randomUUID();
		final User user = new User(userUuid, "createfolder@example.com");
		insertUserIntoDatabase(user);

		final Folder folderToCreate = new Folder(null, null, "My Folder");
		final Folder createdFolder = dao.createFolder(userUuid, folderToCreate);

		final Folder folderFromDatabase = getFolderFromDatabase(createdFolder.getUuid());
		folderToCreate.setUser(userUuid);
		folderFromDatabase.setUuid(null);

		assertEquals(folderToCreate, folderFromDatabase);
	}

	/**
	 * Upon successfully creating a folder, verify it's returned from the DAO method
	 * 
	 * @throws DataAccessException if a data access error occurred in the DAO
	 * @throws SQLException if an SQL exception occurred accessing the database
	 */
	@Test
	public void createFolder_CreateFolderSuccessful_FolderReturnedFromDao() throws DataAccessException, SQLException {
		final UUID userUuid = UUID.randomUUID();
		final User user = new User(userUuid, "createfolder2@example.com");
		insertUserIntoDatabase(user);

		final Folder folderToCreate = new Folder(null, null, "My Folder");
		final Folder folderFromDao = dao.createFolder(userUuid, folderToCreate);

		folderToCreate.setUser(userUuid);
		folderFromDao.setUuid(null);

		assertEquals(folderToCreate, folderFromDao);
	}

	/**
	 * Verify that an exception is thrown if the specified user does not exist in
	 * the database
	 * 
	 * @throws SQLException if an SQL exception occurred accessing the database
	 * @throws DataAccessException if a data access error occurred in the DAO
	 */
	@Test
	public void createFolder_UserIdDoesNotExist_ThrowException() throws SQLException, DataAccessException {
		final UUID userUuid = UUID.randomUUID();
		final User user = new User(userUuid, "createfolder3@example.com");
		insertUserIntoDatabase(user);

		final Folder folderToCreate = new Folder(null, null, "My Folder");

		assertThrows(EntityNotFoundException.class, () -> {
			dao.createFolder(UUID.randomUUID(), folderToCreate);
		});
	}
}
