package com.gitlab.rurouniwallace.notes.dao.sql;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.sql.SQLException;
import java.util.Date;
import java.util.UUID;

import org.junit.jupiter.api.Test;

import com.gitlab.rurouniwallace.notes.dao.SqlDao;
import com.gitlab.rurouniwallace.notes.exceptions.DataAccessException;
import com.gitlab.rurouniwallace.notes.exceptions.EntityNotFoundException;
import com.gitlab.rurouniwallace.notes.models.Folder;
import com.gitlab.rurouniwallace.notes.models.Note;
import com.gitlab.rurouniwallace.notes.models.User;

/**
 * Test cases for creating a note
 */
public class CreateNoteTest extends BaseSqlDaoTest {

	/**
	 * Check that when a note is created by the DAO, it's successfully inserted into
	 * the database
	 * 
	 * @throws DataAccessException if a data access error occurred in the DAO
	 * @throws SQLException if an SQL exception occurred accessing the database
	 */
	@Test
	public void createNote_NoteCreatedSuccessfully_CreatedNoteMatchesOriginal()
			throws SQLException, DataAccessException {
		final SqlDao dao = new SqlDao(dataSource, securityConfig);

		// the note needs to be associated with a user. We could use one of the user's
		// created in
		// a previous test case, but we want to keep our test cases
		// independent/self-contained
		final UUID userUuid = UUID.randomUUID();
		final User userToCreate = new User(userUuid, "createNote1@example.com");

		final Note noteToCreate = new Note(UUID.randomUUID(), null, null, "Cool Note",
				"To be or not to be, that is the question", new Date(), new Date());

		insertUserIntoDatabase(userToCreate);

		final Note noteFromDao = dao.createNote(userUuid, noteToCreate);

		final Note noteFromDatabase = getNoteFromDatabase(noteFromDao.getUuid());

		noteToCreate.setUser(userUuid);

		assertEquals(noteToCreate, noteFromDatabase);
	}

	/**
	 * Check that when a note is created, it's returned from the DAO method
	 * 
	 * @throws SQLException if a data access error occurred in the DAO
	 * @throws DataAccessException if an SQL exception occurred accessing the
	 * database
	 */
	@Test
	public void createNote_NoteCreatedSuccessfully_ReturnedNoteMatchesOriginal()
			throws SQLException, DataAccessException {
		final SqlDao dao = new SqlDao(dataSource, securityConfig);

		// the note needs to be associated with a user. We could use one of the user's
		// created in
		// a previous test case, but we want to keep our test cases
		// independent/self-contained
		final UUID userUuid = UUID.randomUUID();
		final User userToCreate = new User(userUuid, "createNote2@example.com");

		final Note noteToCreate = new Note(UUID.randomUUID(), null, null, "Cool Note",
				"To be or not to be, that is the question", new Date(), new Date());

		insertUserIntoDatabase(userToCreate);

		final Note noteFromDao = dao.createNote(userUuid, noteToCreate);

		noteToCreate.setUser(userUuid);

		assertEquals(noteToCreate, noteFromDao);
	}

	/**
	 * Check that when a note is created in a folder, the note is successfully
	 * inserted into the databadse
	 * 
	 * @throws SQLException if an SQL exception occurred
	 * @throws DataAccessException if a data access exception occurred
	 */
	@Test
	public void createNote_PutNoteIntoFolder_NoteInsertedSuccessfully() throws SQLException, DataAccessException {
		final SqlDao dao = new SqlDao(dataSource, securityConfig);

		final UUID userUuid = UUID.randomUUID();
		final User userToCreate = new User(userUuid, "createNote3@example.com");

		insertUserIntoDatabase(userToCreate);

		final UUID folderUuid = UUID.randomUUID();
		final Folder folderToCreate = new Folder(folderUuid, userUuid, "My Folder");

		insertFolderIntoDatabase(folderToCreate);

		final Note noteToCreate = new Note(UUID.randomUUID(), folderUuid, null, "Cool Note",
				"To be or not to be, that is the question", new Date(), new Date());

		final Note noteFromDao = dao.createNote(userUuid, noteToCreate);

		noteToCreate.setUser(userUuid);

		assertEquals(noteToCreate, noteFromDao);
	}

	/**
	 * Check that when we attempt to put a note into a folder that belongs to a
	 * different user, an "entity not found" exception is thrown
	 * 
	 * @throws SQLException if an SQL exception occurred
	 */
	@Test
	public void createNote_PutNoteIntoFolderForDifferentUser_ExceptionThrown() throws SQLException {
		final SqlDao dao = new SqlDao(dataSource, securityConfig);

		final UUID ourUserUuid = UUID.randomUUID();
		final User ourUser = new User(ourUserUuid, "createNote4@example.com");

		final UUID otherUserUuid = UUID.randomUUID();
		final User otherUser = new User(otherUserUuid, "testuser10@example.com");

		insertUserIntoDatabase(ourUser);
		insertUserIntoDatabase(otherUser);

		final UUID folderUuid = UUID.randomUUID();
		final Folder folderToCreate = new Folder(folderUuid, ourUserUuid, "My Folder");

		insertFolderIntoDatabase(folderToCreate);

		final Note noteToCreate = new Note(UUID.randomUUID(), folderUuid, null, "Cool Note",
				"To be or not to be, that is the question", new Date(), new Date());

		assertThrows(EntityNotFoundException.class, () -> {
			dao.createNote(otherUserUuid, noteToCreate);
		});
	}

	/**
	 * Check that when the folder we're attempting to put the note into doesn't
	 * exist, an "entity not found" exception is thrown
	 * 
	 * @throws SQLException if an SQL exception occurred
	 * @throws DataAccessException if a data access exception occurred
	 */
	@Test
	public void createNote_PutNoteIntoFolderThatDoesNotExist_ExceptionThrown()
			throws SQLException, DataAccessException {
		final SqlDao dao = new SqlDao(dataSource, securityConfig);

		final UUID userUuid = UUID.randomUUID();
		final User userToCreate = new User(userUuid, "createNote5@example.com");

		insertUserIntoDatabase(userToCreate);

		final Note noteToCreate = new Note(UUID.randomUUID(), UUID.randomUUID(), null, "Cool Note",
				"To be or not to be, that is the question", new Date(), new Date());

		assertThrows(EntityNotFoundException.class, () -> {
			dao.createNote(userUuid, noteToCreate);
		});
	}

	/**
	 * Check that when the user we're creating the note for doesn't exist, an
	 * "entity not found" exception is thrown
	 * 
	 * @throws SQLException if an SQL exception occurred
	 * @throws DataAccessException if a data access exception occurred
	 */
	@Test
	public void createNote_AssignNoteToUserThatDoesNotExist_ExceptionThrown() throws SQLException, DataAccessException {
		final SqlDao dao = new SqlDao(dataSource, securityConfig);

		final UUID userUuid = UUID.randomUUID();
		final User userToCreate = new User(userUuid, "createNote6@example.com");
		insertUserIntoDatabase(userToCreate);

		final UUID folderUuid = UUID.randomUUID();
		final Folder folderToCreate = new Folder(folderUuid, userUuid, "My Folder");
		insertFolderIntoDatabase(folderToCreate);

		final Note noteToCreate = new Note(UUID.randomUUID(), folderUuid, null, "Cool Note",
				"To be or not to be, that is the question", new Date(), new Date());

		assertThrows(EntityNotFoundException.class, () -> {
			dao.createNote(UUID.randomUUID(), noteToCreate);
		});
	}

	@Test
	public void createNote_NoteUuidNotAsserted_ThrowException() throws SQLException {
		final SqlDao dao = new SqlDao(dataSource, securityConfig);

		final UUID userUuid = UUID.randomUUID();
		final User userToCreate = new User(userUuid, "createNote7@example.com");
		insertUserIntoDatabase(userToCreate);

		final UUID folderUuid = UUID.randomUUID();
		final Folder folderToCreate = new Folder(folderUuid, userUuid, "My Folder");
		insertFolderIntoDatabase(folderToCreate);

		final Note noteToCreate = new Note(null, folderUuid, null, "Cool Note",
				"To be or not to be, that is the question", new Date(), new Date());

		assertThrows(IllegalArgumentException.class, () -> {
			dao.createNote(UUID.randomUUID(), noteToCreate);
		});
	}
}
