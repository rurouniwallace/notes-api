package com.gitlab.rurouniwallace.notes.dao.sql;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.gitlab.rurouniwallace.notes.dao.SqlDao;
import com.gitlab.rurouniwallace.notes.exceptions.DataAccessException;
import com.gitlab.rurouniwallace.notes.exceptions.EntityNotFoundException;
import com.gitlab.rurouniwallace.notes.models.Note;
import com.gitlab.rurouniwallace.notes.models.User;

import liquibase.exception.LiquibaseException;

public class UpdateNoteTest extends BaseSqlDaoTest {

	private static final UUID userUuid = UUID.randomUUID();

	private SqlDao dao;

	@BeforeAll
	public static void setUpBeforeClass() throws SQLException, LiquibaseException {
		BaseSqlDaoTest.setUpBeforeClass();

		final User user = new User(userUuid, "updatenote@example.com");
		insertUserIntoDatabase(user);
	}

	@BeforeEach
	public void setUp() {
		dao = new SqlDao(dataSource, securityConfig);
	}

	@ParameterizedTest
	@MethodSource("provideNoteUpdates")
	public void updateNote_updateIndividualField_FieldSuccessfullyUpdated(final String fieldToUpdate,
			final Object updatedValue) throws SQLException, DataAccessException {
		final UUID noteUuid = UUID.randomUUID();
		final Note note = new Note(noteUuid, null, userUuid, "My Note",
				"This is my note. There are many like it but this one is mine", new Date(), new Date());
		insertNoteIntoDatabase(note);

		final Note updatePayload = createUpdatePayload(fieldToUpdate, updatedValue);

		dao.updateNote(userUuid, noteUuid, updatePayload);

		final Note noteFromDatabase = getNoteFromDatabase(noteUuid);

		final Note expectedNote = updateField(new Note(note), fieldToUpdate, updatedValue);

		assertEquals(expectedNote, noteFromDatabase);
	}

	@Test
	public void updateNote_AttemptToUpdateNoteUuid_ThrowException() throws SQLException {
		final UUID noteUuid = UUID.randomUUID();
		final Note note = new Note(noteUuid, null, userUuid, "My Note",
				"This is my note. There are many like it but this one is mine", new Date(), new Date());
		insertNoteIntoDatabase(note);

		final Note updatePayload = createUpdatePayload("uuid", UUID.randomUUID());

		assertThrows(DataAccessException.class, () -> {
			dao.updateNote(userUuid, noteUuid, updatePayload);
		});
	}

	@Test
	public void updateNote_PutNoteIntoFolderThatDoesNotExist_ThrowException() throws SQLException {
		final UUID noteUuid = UUID.randomUUID();
		final Note note = new Note(noteUuid, null, userUuid, "My Note",
				"This is my note. There are many like it but this one is mine", new Date(), new Date());
		insertNoteIntoDatabase(note);

		final Note updatePayload = createUpdatePayload("folderId", UUID.randomUUID());

		assertThrows(EntityNotFoundException.class, () -> {
			dao.updateNote(userUuid, noteUuid, updatePayload);
		});
	}

	@Test
	public void updateNote_AttemptToUpdateUser_ThrowException() throws SQLException {
		final UUID noteUuid = UUID.randomUUID();
		final Note note = new Note(noteUuid, null, userUuid, "My Note",
				"This is my note. There are many like it but this one is mine", new Date(), new Date());
		insertNoteIntoDatabase(note);

		final Note updatePayload = createUpdatePayload("userId", UUID.randomUUID());

		assertThrows(DataAccessException.class, () -> {
			dao.updateNote(userUuid, noteUuid, updatePayload);
		});
	}

	@Test
	public void updateNote_NoteIdNotFound_ThrowException() throws SQLException {
		final UUID noteUuid = UUID.randomUUID();
		final Note note = new Note(noteUuid, null, userUuid, "My Note",
				"This is my note. There are many like it but this one is mine", new Date(), new Date());
		insertNoteIntoDatabase(note);

		final Note updatePayload = createUpdatePayload("title", "New Title");

		assertThrows(EntityNotFoundException.class, () -> {
			dao.updateNote(userUuid, UUID.randomUUID(), updatePayload);
		});
	}

	@Test
	public void updateNote_UserIdNotFound_ThrowException() throws SQLException {
		final UUID noteUuid = UUID.randomUUID();
		final Note note = new Note(noteUuid, null, userUuid, "My Note",
				"This is my note. There are many like it but this one is mine", new Date(), new Date());
		insertNoteIntoDatabase(note);

		final Note updatePayload = createUpdatePayload("title", "New Title");

		assertThrows(EntityNotFoundException.class, () -> {
			dao.updateNote(UUID.randomUUID(), noteUuid, updatePayload);
		});
	}

	@Test
	public void updateNote_AttemptToUpdateAnotherUsersNot_ThrowException() throws SQLException {
		final UUID anotherUserUuid = UUID.randomUUID();
		final User anotherUser = new User(anotherUserUuid, "updateuser2@example.com");
		insertUserIntoDatabase(anotherUser);

		final UUID noteUuid = UUID.randomUUID();
		final Note note = new Note(noteUuid, null, userUuid, "My Note",
				"This is my note. There are many like it but this one is mine", new Date(), new Date());
		insertNoteIntoDatabase(note);

		final Note updatePayload = createUpdatePayload("title", "New Title");

		assertThrows(EntityNotFoundException.class, () -> {
			dao.updateNote(anotherUserUuid, noteUuid, updatePayload);
		});
	}

	private Note createUpdatePayload(final String fieldToUpdate, final Object updatedValue) {
		return updateField(new Note(), fieldToUpdate, updatedValue);
	}

	private Note updateField(final Note updatePayload, final String fieldToUpdate, final Object updatedValue) {
		switch (fieldToUpdate) {
		case "uuid":
			updatePayload.setUuid((UUID) updatedValue);
			break;
		case "folderId":
			updatePayload.setFolder((UUID) updatedValue);
			break;
		case "userId":
			updatePayload.setUser((UUID) updatedValue);
			break;
		case "title":
			updatePayload.setTitle((String) updatedValue);
			break;
		case "body":
			updatePayload.setBody((String) updatedValue);
			break;
		case "created":
			updatePayload.setCreated((Date) updatedValue);
			break;
		case "lastUpdated":
			updatePayload.setLastUpdated((Date) updatedValue);
			break;
		}
		return updatePayload;
	}

	private static Stream<Arguments> provideNoteUpdates() {
		final List<Arguments> arguments = new ArrayList<>();

		arguments.add(Arguments.of("title", "Still My Note"));
		arguments
				.add(Arguments.of("body", "This is still my note. There are many like it, but this one is still mine"));
		arguments.add(Arguments.of("created", new Date(1614901610276L)));
		arguments.add(Arguments.of("lastUpdated", new Date(1614901637821L)));

		return arguments.stream();
	}
}
