package com.gitlab.rurouniwallace.notes.dao.sql;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.gitlab.rurouniwallace.notes.dao.SqlDao;
import com.gitlab.rurouniwallace.notes.exceptions.DataAccessException;
import com.gitlab.rurouniwallace.notes.exceptions.EntityNotFoundException;
import com.gitlab.rurouniwallace.notes.models.Folder;
import com.gitlab.rurouniwallace.notes.models.User;

import liquibase.exception.LiquibaseException;

public class UpdateFolderTest extends BaseSqlDaoTest {

	private static final UUID userUuid = UUID.randomUUID();

	private SqlDao dao;

	@BeforeAll
	public static void setUpBeforeClass() throws SQLException, LiquibaseException {
		BaseSqlDaoTest.setUpBeforeClass();

		final User user = new User(userUuid, "updatefolder@example.com");
		insertUserIntoDatabase(user);
	}

	@BeforeEach
	public void setUp() {
		dao = new SqlDao(dataSource, securityConfig);
	}

	@ParameterizedTest
	@MethodSource("provideFolderUpdates")
	public void updateFolder_UpdateIndividualField_FieldSuccessfullyUpdated(final String fieldToUpdate,
			final Object updatedValue) throws SQLException, DataAccessException {
		final UUID folderUuid = UUID.randomUUID();
		final Folder folder = new Folder(folderUuid, userUuid, "My Folder");
		insertFolderIntoDatabase(folder);

		final Folder updatePayload = createUpdatePayload(fieldToUpdate, updatedValue);

		dao.updateFolder(userUuid, folderUuid, updatePayload);

		final Folder folderFromDatabase = getFolderFromDatabase(folderUuid);

		final Folder expectedFolder = updateField(new Folder(folder), fieldToUpdate, updatedValue);

		assertEquals(expectedFolder, folderFromDatabase);
	}

	@Test
	public void updateFolder_AttemptToUpdateFolderId_ThrowException() throws SQLException {
		final UUID folderUuid = UUID.randomUUID();
		final Folder folder = new Folder(folderUuid, userUuid, "My Folder");
		insertFolderIntoDatabase(folder);

		final Folder updatePayload = createUpdatePayload("uuid", UUID.randomUUID());

		assertThrows(DataAccessException.class, () -> {
			dao.updateFolder(folderUuid, userUuid, updatePayload);
		});
	}

	@Test
	public void updateFolder_AttemptToUpdateUserId_ThrowException() throws SQLException {
		final UUID folderUuid = UUID.randomUUID();
		final Folder folder = new Folder(folderUuid, userUuid, "My Folder");
		insertFolderIntoDatabase(folder);

		final Folder updatePayload = createUpdatePayload("userId", UUID.randomUUID());

		assertThrows(DataAccessException.class, () -> {
			dao.updateFolder(folderUuid, userUuid, updatePayload);
		});
	}

	@Test
	public void updateFolder_FolderIdNotFound_ThrowException() throws SQLException {
		final UUID folderUuid = UUID.randomUUID();
		final Folder folder = new Folder(folderUuid, userUuid, "My Folder");
		insertFolderIntoDatabase(folder);

		final Folder updatePayload = createUpdatePayload("name", "My Other Folder");

		assertThrows(EntityNotFoundException.class, () -> {
			dao.updateFolder(UUID.randomUUID(), userUuid, updatePayload);
		});
	}

	@Test
	public void updateFolder_UserIdNotFound_ThrowException() throws SQLException {
		final UUID folderUuid = UUID.randomUUID();
		final Folder folder = new Folder(folderUuid, userUuid, "My Folder");
		insertFolderIntoDatabase(folder);

		final Folder updatePayload = createUpdatePayload("name", "My Other Folder");

		assertThrows(EntityNotFoundException.class, () -> {
			dao.updateFolder(folderUuid, UUID.randomUUID(), updatePayload);
		});
	}

	private Folder createUpdatePayload(final String fieldToUpdate, final Object updatedValue) {
		return updateField(new Folder(), fieldToUpdate, updatedValue);
	}

	private Folder updateField(final Folder folder, final String fieldToUpdate, final Object updatedValue) {
		switch (fieldToUpdate) {
		case "uuid":
			folder.setUuid((UUID) updatedValue);
			break;
		case "userId":
			folder.setUser((UUID) updatedValue);
			break;
		case "name":
			folder.setName((String) updatedValue);
			break;
		}
		return folder;
	}

	private static Stream<Arguments> provideFolderUpdates() {
		final List<Arguments> arguments = new ArrayList<>();

		arguments.add(Arguments.of("name", "Still My Folder"));

		return arguments.stream();
	}
}
