echo "Starting server...\n";
java -jar /opt/wiremock-jre8-standalone-2.33.2.jar --port 8090 --root-dir notes-api-test/wiremock > /dev/null &
notes-api-service/gradlew run --args="server ../notes-api-test/config-test.yml" > /dev/null &

service_running=false;
while [ $service_running = false ]
do
	nohup curl http://localhost:8080/health > /dev/null;

	if [ $? = 0 ]
	then
		service_running=true;
	fi
done
echo "Server started!\n";
